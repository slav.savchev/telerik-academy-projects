# RidePal – Playlist Generator
Here you will find information regarding the Web Application RidePal.

This is the link for our Trello: https://bit.ly/2HAcE4R

## Project description

Our goal is to design and implement a Restful Web Application for serve the needs of music-lovers alike.

## Functional details and General Information

**RidePal Playlist Generator** enables users to generate playlists for specific travel duration periods based on their preferred genres.
A **playlist** consists of a list of individual **tracks** (songs). Each track has an **artist**, **title**, **album**, **duration** (playtime length) and **rank** (a numeric value).
Tracks can be played (full duration) when they have been added to a playlist or browsed so that the `[USER]` can see whether the auto-generated playlist suits his taste.

Each **playlist** has a `[USER]` given title, associated tags (e.g. musical genres), a 
list of **tracks** with the associated track details, total playtime (the sum of the playtimes of all 
**tracks** in that **playlist**) and rank (the average of the ranks of all **tracks** in that playlist).

## Main Use Case

A `[USER]` travelling from point A to point B wants to have something to listen to during the duration of the travel. 
The `[USER]` wants to generate a track list based on his musical tastes (the user selects genres from a predefined list). 
An algorithm, which uses external service as track sample data, generates the playlist.

**RidePal Playlist Generator** calculates the travel duration time between the starting and destination locations and combines 
tracks chosen randomly in the specified genres, until the playing time roughly matches the travel time duration.
The generated playlist is saved under this `[USER]`’s profile and they can start listening to it. Furthermore, the application 
offers the option to browse playlists created by other users and allows filtering by total duration and genre tags. By default
playlists are sorted by average rank ascending. 

The application offers certain configuration over the playlist generation algorithm
and the possibility to play full-length tracks in the generated playlist.

# Front-End:

## PUBLIC PART

The public part of your project is visible without authentication to `[GUESTs]`. This includes 
the application start page, the `[USER]` login and `[USER]` registration forms, as well as the list of 
all `[USER]` generated playlists. People that are not authenticated cannot see any `[USER]` specific details, 
neither they can interact with the website. They can only browse the playlists and see the tracks list and details of them.

*  Public page contains 3 Editor-Picked Playlists with top tracks from each of the 3 available genres
*  Playlists can be clicked to show the list of tracks
*  Playlists show average rank (average of its tracks) and total playtime without being opened
*  Playlists are sorted by rank and can be filtered by name, genre and duration

## PRIVATE PART (Users Only)

The private part is accessible only to `[USERs]` who have successfully authenticated (registered users).

The private part of the web application provides the users with the ability to generate 
new playlists, control the generation algorithm as well as edit or delete their own existing playlists.

## ADMIN PART

An `[ADMIN]` can administer all major information objects in the system.
On top of the regular `[USER]` capabilities, the `[ADMIN]` has the following capabilities:

*  Administrators can edit/delete users and other administrators
*  Administrators can edit/delete over the playlists
*  Administrators can manually trigger Genre synchronization

## REST API

Here you will find documentation/information regarding the free-to-access `[REST]` 
endpoints of RidePal.

This is a link to our **SWAGGER** documentation: https://bit.ly/31XOc78

## 3rd PARTY API INTEGRATION

RidePal makes use of several 3rd party APIs to provide the best user experience.

### Microsoft Bing Maps

Bing Maps is a web mapping service provided as a part of Microsoft's Bing suite 
of search engines and powered by the Bing Maps for Enterprise framework.

### Deezer

Deezer is a French online music streaming service. It allows users to listen to music 
content from record labels, including Universal Music Group, Sony Music and Warner 
Music Group on various devices online or offline.

### NewsAPI

NewsApi allows users to get the latest headlines from a number of online news sources/blogs.













