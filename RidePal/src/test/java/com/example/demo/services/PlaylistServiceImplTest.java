package com.example.demo.services;

import com.example.demo.models.Artist;
import com.example.demo.models.DTOs.DisplayPlaylistDTO;
import com.example.demo.models.DTOs.PlaylistDTO;
import com.example.demo.models.Genre;
import com.example.demo.models.Playlist;
import com.example.demo.models.Track;
import com.example.demo.repositories.contracts.PlaylistRepository;
import com.example.demo.services.contracts.GenreService;
import com.example.demo.services.contracts.PlaylistService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.parameters.P;

import java.util.*;

@RunWith(MockitoJUnitRunner.class)
public class PlaylistServiceImplTest {

    @Mock
    PlaylistRepository playlistRepository;

    @Mock
    GenreService genreService;

    @InjectMocks
    PlaylistServiceImpl playlistService;



    @Test(expected = NullPointerException.class)
    public void updatePlaylist_Should_UpdatePlaylist(){
        //Arrange
        Playlist playlist = MockFactory.mockPlaylist();
        DisplayPlaylistDTO displayPlaylistDTO = MockFactory.mockDisplayPlaylistDTO();
        Genre genre = MockFactory.mockGenre1();

        Mockito.when(playlistRepository.updatePlaylist(playlist)).thenReturn(playlist);
        Mockito.when(playlistRepository.getById(1)).thenReturn(playlist);
//        Mockito.when(genreService.getGenreByName(genre.getName())).thenReturn(genre);

        //Act
        DisplayPlaylistDTO returned = playlistService.editPlaylist(displayPlaylistDTO);

        //Assert

        Assert.assertEquals(playlist.getTitle(),returned.getTitle());


    }

    @Test
    public void getAllPlaylists_Should_Get_All_Playlists(){
        //Arrange
        List<Playlist> playlistList = MockFactory.mockPlaylistList();

        Mockito.when(playlistRepository.getAllPlaylists()).thenReturn(playlistList);

        //Act
        List<Playlist> returned = playlistService.getAllPlaylists();
        //Assert
        Assert.assertEquals(playlistList.size(),returned.size());
    }

    @Test
    public void getAllPlaylistsMap_Should_Get_All_Playlists(){
        //Arrange
        List<Playlist> playlistList = MockFactory.mockPlaylistList();
        Map<Integer,String> map = new HashMap<>();

        Mockito.when(playlistRepository.getAllPlaylists(map)).thenReturn(playlistList);

        //Act
        List<Playlist> returned = playlistService.getAllPlaylists(map);
        //Assert
        Assert.assertEquals(playlistList.size(),returned.size());
    }


    @Test
    public void getByID_Should_Get_Playlist_ByID(){
        //Arrange
        Playlist playlist = MockFactory.mockPlaylist();

        Mockito.when(playlistRepository.getById(1)).thenReturn(playlist);

        //Act
        Playlist playlist1 = playlistService.getById(1);

        //Assert
        Assert.assertEquals(playlist.getTitle(),playlist1.getTitle());
    }

    @Test(expected = ArithmeticException.class)
    public void createPlaylist_Should_CreatePlaylist(){
        //Arrange
        Playlist playlist = MockFactory.mockPlaylist();
        PlaylistDTO playlistDTO = new PlaylistDTO();
        playlistDTO.setDuration(10);
        playlistDTO.setSameArtist(true);
        playlistDTO.setTopTracks(true);
        playlistDTO.setTitle("Testing");
        playlistDTO.setUserID("slav");
        playlistDTO.setGenrePref(new HashMap<>());


        //Act
        Playlist playlist1 = playlistService.createPlaylist(playlistDTO);

        //Assert
        Assert.assertEquals(playlist,playlist1);
    }

    @Test
    public void createPlaylistList_Should_CreatePlaylist(){
        //Arrange
        List<Playlist> playlistList = MockFactory.mockPlaylistList();

        Playlist playlist = MockFactory.mockPlaylist();
        PlaylistDTO playlistDTO = new PlaylistDTO();
        playlistDTO.setDuration(10);
        playlistDTO.setSameArtist(true);
        playlistDTO.setTopTracks(true);
        playlistDTO.setTitle("Testing");
        playlistDTO.setUserID("slav");
        playlistDTO.setGenrePref(new HashMap<>());

        Mockito.when(playlistRepository.createPlaylist(playlistList)).thenReturn(playlistList);

        //Act
        List<Playlist> playlistList1 = playlistService.createPlaylist(playlistList);

        //Assert
        Assert.assertEquals(playlistList1.size(),playlistList.size());
    }

    @Test
    public void changePlaylistStatus_Should_ChangeStatus(){
        //Arrange
        Playlist playlist = MockFactory.mockPlaylist();

        Mockito.when(playlistRepository.getById(1)).thenReturn(playlist);
        Mockito.when(playlistRepository.updatePlaylist(playlist)).thenReturn(playlist);

        //Act
        Playlist playlist1 = playlistService.changePlaylistStatus(1,true);

        //Assert
        Assert.assertEquals(playlist.getTitle(),playlist1.getTitle());
    }

    @Test
    public void getAllPlaylistsAdmin_Should_Get_All_Playlists(){
        //Arrange
        List<Playlist> playlistList = MockFactory.mockPlaylistList();

        Mockito.when(playlistRepository.getAllPlaylistsAdmin()).thenReturn(playlistList);

        //Act
        List<Playlist> returned = playlistService.getAllPlaylistsAdmin();

        //Assert
        Assert.assertEquals(playlistList.size(),returned.size());
    }

    @Test
    public void getMinAndMaxDuration_Should_ReturnDuration(){
        //Arrange
        int[] arr = {1,2};

        Mockito.when(playlistRepository.getMinAndMaxDuration()).thenReturn(arr);

        //Act

        int[] arr2 = playlistService.getMinAndMaxDuration();

        //Assert

        Assert.assertEquals(arr.length,arr2.length);
    }

    @Test
    public void getPlaylistsByUserID_Should_Reutrn_Playlists(){
        //Arrange
        List<Playlist> playlistList = MockFactory.mockPlaylistList();

        Mockito.when(playlistRepository.getPlaylistsByUserID("slav")).thenReturn(playlistList);

        //Act
        List<Playlist> playlistList1 = playlistService.getPlaylistsByUserID("slav");

        //Assert
        Assert.assertEquals(playlistList.size(),playlistList1.size());
    }

    @Test
    public void checkArtistExistsInPlaylist_Should_Return_Bool(){
        Artist artist = MockFactory.mockArtist1();
        Playlist playlist = MockFactory.mockPlaylist();
        Set<Track> trackSet = new HashSet<>();
        trackSet.add(MockFactory.mockTrack1());
        playlist.setTracks(trackSet);

        playlistService.checkArtistExistsInPlaylist(artist,playlist);
    }

}
