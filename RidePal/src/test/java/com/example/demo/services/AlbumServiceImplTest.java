package com.example.demo.services;

import com.example.demo.models.Album;
import com.example.demo.models.Genre;
import com.example.demo.repositories.contracts.AlbumRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class AlbumServiceImplTest {

    @Mock
    AlbumRepository albumRepository;

    @InjectMocks
    AlbumServiceImpl albumService;

    @Test
    public void createAlbum_Should_CreateAlbum_When_AlbumDoesNotExist() {
        // Arrange
        Album album = MockFactory.mockAlbum1();

        Mockito.when(albumRepository.checkAlbumExists(album.getTitle())).thenReturn(Boolean.FALSE);

        // Act
        albumService.createAlbum(album);

        // Assert
        Mockito.verify(albumRepository, Mockito.times(1)).createAlbum(album);

    }

    @Test
    public void createAlbum_ShouldNot_CreateAlbum_When_AlbumExists() {
        // Arrange
        Album album = MockFactory.mockAlbum1();

        Mockito.when(albumRepository.checkAlbumExists(album.getTitle())).thenReturn(Boolean.TRUE);

        // Act
        albumService.createAlbum(album);

        // Assert
        Mockito.verify(albumRepository, Mockito.times(0)).createAlbum(album);

    }


    @Test
    public void createAlbumList_Should_ReturnAlbumList_When_AlbumListIsCreated() {
        // Arrange
        List<Album> albumList = MockFactory.mockAlbumList();

        Mockito.when(albumRepository.createAlbum(albumList)).thenReturn(albumList);

        // Act
        List<Album> returnedAlbumList = albumService.createAlbumList(albumList);

        // Assert
        Assert.assertEquals(albumList.get(0), returnedAlbumList.get(0));
        Assert.assertEquals(albumList.get(1), returnedAlbumList.get(1));

    }

    @Test
    public void getAlbumById_Should_ReturnAlbum_When_AlbumExists() {

        // Arrange
        Album album = MockFactory.mockAlbum1();

        Mockito.when(albumRepository.getById(ArgumentMatchers.anyInt())).thenReturn(album);

        // Act
        Album returnedAlbum = albumService.getById(1);

        // Assert
        Assert.assertEquals(returnedAlbum.getId(), album.getId());
    }


    @Test
    public void checkAlbumExists_Should_True_WhenAlbumExists() {
        // Arrange
        Album album = MockFactory.mockAlbum1();
        Mockito.when(albumRepository.checkAlbumExists(album.getTitle())).thenReturn(true);

        // Act
        boolean returnedBoolean = albumService.checkAlbumExists(album.getTitle());

        // Assert
        Assert.assertTrue(returnedBoolean);

    }

    @Test
    public void getAllAlbums_Should_ReturnAllAlbums_When_Prompted() {

        // Arrange
        List<Album> albumList = MockFactory.mockAlbumList();

        Mockito.when(albumRepository.getAllAlbums()).thenReturn(albumList);

        // Act
        List<Album> returnedAlbumList = albumService.getAllAlbums();

        // Assert
        Assert.assertEquals(returnedAlbumList.get(0), albumList.get(0));
        Assert.assertEquals(returnedAlbumList.get(1), albumList.get(1));

    }

    @Test
    public void getByName_Should_ReturnAlbum_When_AlbumExists() {

        // Arrange
        Album album = MockFactory.mockAlbum1();

        Mockito.when(albumRepository.getAlbumByName(album.getTitle())).thenReturn(album);

        // Act
        Album returnedAlbum = albumService.getAlbumByName("Title 1");

        // Assert
        Assert.assertEquals(returnedAlbum.getTitle(), album.getTitle());
    }


}
