package com.example.demo.services;

import com.example.demo.models.*;
import com.example.demo.models.DTOs.DisplayPlaylistDTO;
import com.example.demo.models.DTOs.GenreDTO;
import com.example.demo.models.DTOs.UserDTO;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MockFactory {


    public static Genre mockGenre1() {
        Genre genre1 = new Genre();
        genre1.setId(1);
        genre1.setName("genre 1");
        genre1.setActive(true);
        return genre1;
    }

    public static Genre mockGenre2() {
        Genre genre2 = new Genre();
        genre2.setId(2);
        genre2.setName("genre 2");
        genre2.setActive(false);
        return genre2;
    }

    public static Genre mockGenre3() {
        Genre genre3 = new Genre();
        genre3.setId(3);
        genre3.setName("genre 3");
        genre3.setActive(false);
        return genre3;
    }


    public static GenreDTO mockGenreDTO1() {
        GenreDTO genre1 = new GenreDTO();
        genre1.setId(1);
        genre1.setName("genre 1");
        genre1.setActive(true);
        return genre1;
    }

    public static GenreDTO mockGenreDTO2() {
        GenreDTO genre2 = new GenreDTO();
        genre2.setId(2);
        genre2.setName("genre 2");
        genre2.setActive(true);
        return genre2;
    }


    public static List<Genre> mockGenreList() {
        List<Genre> genreList = new ArrayList<>();
        genreList.add(mockGenre1());
        genreList.add(mockGenre2());
        genreList.add(mockGenre3());
        return genreList;
    }


    public static List<GenreDTO> mockGenreListDTO() {
        List<GenreDTO> genreListDTO = new ArrayList<>();
        genreListDTO.add(mockGenreDTO1());
        genreListDTO.add(mockGenreDTO2());
        return genreListDTO;
    }

    public static Album mockAlbum1() {
        Album album1 = new Album();
        album1.setId(1);
        album1.setTitle("Title 1");
        album1.setTracklist("Tracklist 1");
        return album1;
    }

    public static Album mockAlbum2() {
        Album album2 = new Album();
        album2.setId(2);
        album2.setTitle("Title 2");
        album2.setTracklist("Tracklist 2");
        return album2;
    }

    public static List<Album> mockAlbumList() {
        List<Album> albumList = new ArrayList<>();
        albumList.add(mockAlbum1());
        albumList.add(mockAlbum2());
        return albumList;
    }

    public static Artist mockArtist1() {
        Artist artist1 = new Artist();
        artist1.setId(1);
        artist1.setName("artist 1");
        artist1.setTracklist("Tracklist 1");
        return artist1;
    }

    public static Artist mockArtist2() {
        Artist artist2 = new Artist();
        artist2.setId(2);
        artist2.setName("artist 2");
        artist2.setTracklist("Tracklist 2");
        return artist2;
    }

    public static List<Artist> mockArtistList() {
        List<Artist> artistList = new ArrayList<>();
        artistList.add(mockArtist1());
        artistList.add(mockArtist2());
        return artistList;
    }

    public static Track mockTrack1() {
        Track track1 = new Track();
        track1.setId(1);
        track1.setTitle("track 1");
        track1.setDuration(500);
        track1.setLink("Link 1");
        track1.setArtist(mockArtist1());
        track1.setAlbum(mockAlbum1());
        track1.setGenre(mockGenre1().getName());
        track1.setPreview("www.fake.com");
        track1.setLink("www.fakelink.com");
        return track1;
    }

    public static Track mockTrack2() {
        Track track2 = new Track();
        track2.setId(2);
        track2.setTitle("track 2");
        track2.setDuration(600);
        track2.setLink("Link 2");

        return track2;
    }

    public static List<Track> mockTrackList() {
        List<Track> trackList = new ArrayList<>();
        trackList.add(mockTrack1());
        trackList.add(mockTrack2());
        return trackList;
    }

    public static User mockUser(){
        User user = new User();
        user.setUsername("Slav");
        user.setEmail("slav.savchev@abv.bg");
        user.setEnabled(true);
        user.setFirstName("Slav");
        user.setLastName("Savchev");
        user.setPassword("1234");

        return user;
    }

    public static UserDTO mockUserDTO(){
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername("Slav");
        userDTO.setAdmin(true);
        userDTO.setEnabled(true);
        userDTO.setEmail("slav.s@abv.bg");
        userDTO.setFirstName("Slav");
        userDTO.setPassword("slav");
        userDTO.setLastName("Savchev");
        userDTO.setPasswordConfirmation("slav");


        return userDTO;
    }

    public static List<UserDTO> mockUserDTOList(){
        List<UserDTO> list = new ArrayList<>();
        UserDTO user1 = new UserDTO();
       user1.setUsername("Slav");
       user1.setAdmin(true);
       user1.setEnabled(true);
       user1.setEmail("slav.s@abv.bg");
       user1.setFirstName("Slav");
       user1.setPassword("slav");
       user1.setLastName("Savchev");
       user1.setPasswordConfirmation("slav");
       list.add(user1);

       UserDTO userDTO2 = new UserDTO();
        userDTO2.setUsername("Slav");
        userDTO2.setAdmin(true);
        userDTO2.setEnabled(true);
        userDTO2.setEmail("slav.s@abv.bg");
        userDTO2.setFirstName("Slav");
        userDTO2.setPassword("slav");
        userDTO2.setLastName("Savchev");
        userDTO2.setPasswordConfirmation("slav");
        list.add(userDTO2);

        return list;
    }

    public static List<User> mockUserList(){
        List<User> list = new ArrayList<>();
        User user1 = new User();
        user1.setUsername("Slav");
        user1.setEnabled(true);
        user1.setEmail("slav.s@abv.bg");
        user1.setFirstName("Slav");
        user1.setPassword("slav");
        user1.setLastName("Savchev");
        list.add(user1);

        User user12 = new User();
        user12.setUsername("Slav");
        user12.setEnabled(true);
        user12.setEmail("slav.s@abv.bg");
        user12.setFirstName("Slav");
        user12.setPassword("slav");
        user12.setLastName("Savchev");
        list.add(user12);

        return list;
    }

    public static Playlist mockPlaylist(){
        Playlist playlist = new Playlist();
        playlist.setTitle("Playlist 1");
        playlist.setId(1);
        playlist.setRank(1);
        Set set = new HashSet();
        set.add(mockGenre1());
        playlist.setGenres(set);
        playlist.setActive(true);
        playlist.setDeezerID(43434);
        playlist.setUserID("Slav");
        playlist.setPlaytime(33);
        Set set1 = new HashSet();
        set1.add(mockTrack1());
        playlist.setTracks(set1);
        return playlist;
    }

    public static List<Playlist> mockPlaylistList(){
        List<Playlist> playlistList = new ArrayList<>();
        playlistList.add(mockPlaylist());

        return playlistList;
    }

    public static DisplayPlaylistDTO mockDisplayPlaylistDTO(){
        DisplayPlaylistDTO displayPlaylistDTO = new DisplayPlaylistDTO();
        Playlist playlist = mockPlaylist();
        displayPlaylistDTO.setRank(playlist.getRank());
        displayPlaylistDTO.setGenres("Rock, ");
        displayPlaylistDTO.setUserID("Slav");
        displayPlaylistDTO.setMinutes("30");
        displayPlaylistDTO.setTitle("TestOne");
        displayPlaylistDTO.setId(1);

        return displayPlaylistDTO;
    }
}

