package com.example.demo.services;


import com.example.demo.models.DTOs.UserDTO;
import com.example.demo.models.Role;
import com.example.demo.models.User;
import com.example.demo.repositories.contracts.RoleRepository;
import com.example.demo.repositories.contracts.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {


    @Mock
    UserRepository userRepository;

    @Mock
    RoleRepository roleRepository;

    @InjectMocks
    UserServiceImpl userService;



    @Test
    public void createUser_Should_ReturnUser_When_Called(){

        // Arrange
        User user = MockFactory.mockUser();

        Mockito.when(userRepository.createUser(user)).thenReturn(user);

        // Act
        User returned = userService.createUser(user);

        // Assert
        Assert.assertEquals(user.getFirstName(),returned.getFirstName());
    }

    @Test
    public void getByName_Should_ReturnUserDTO_When_Called(){

        //Arrange
        UserDTO userDTO = MockFactory.mockUserDTO();

        Mockito.when(userRepository.getByName(userDTO.getUsername())).thenReturn(MockFactory.mockUser());

        //Act
        UserDTO returned = userService.getByName(userDTO.getUsername());

        //Assert
        Assert.assertEquals(userDTO.getUsername(),returned.getUsername());
    }

    @Test
    public void getAllUsersDtos_Should_Return_UsersDTOs_When_Called(){

        //Arrange
        List<UserDTO> list = MockFactory.mockUserDTOList();

        Mockito.when(userRepository.getAllUsers()).thenReturn(MockFactory.mockUserList());

        //Act
        List<UserDTO> returned = userService.getAllUsersDtos();

        //Assert
        Assert.assertEquals(returned.size(),list.size());
    }


    @Test(expected = NullPointerException.class)
    public void editUserProfile_Should_Return_UserDTO_When_Called(){

        //Arrange
        UserDTO userDTO = MockFactory.mockUserDTO();

        Mockito.when(userRepository.getByName(userDTO.getUsername())).thenReturn(MockFactory.mockUser());

        //Act
        UserDTO returned = userService.editUserProfile(userDTO);

        //Assert
        Assert.assertEquals(returned.getUsername(),userDTO.getUsername());
    }

    @Test
    public void changePassword_Should_ChangePassword_When_Called(){

        //Arrange
        UserDTO userDTO = MockFactory.mockUserDTO();
        String username = "Slav1";
        String password = "Slav1";


        //Assert
        Mockito.verify(userRepository, Mockito.times(0)).updateUserDetails(MockFactory.mockUser());

    }

    @Test(expected = NullPointerException.class)
    public void makeAdmin_Should_SetAdminRole_When_Called(){

        //Arrange
        User user = MockFactory.mockUser();
        UserDTO userDTO = MockFactory.mockUserDTO();
        Role role = new Role();
        role.setRole("ADMIN");
        role.setRoleID(1);

        Mockito.when(userRepository.getByName(user.getUsername())).thenReturn(user);

        //Act
        UserDTO userDTO1 = userService.makeAdmin(userDTO);

        //Assert

        Assert.assertEquals(userDTO.getUsername(),userDTO1.getUsername());
    }

    @Test(expected = NullPointerException.class)
    public void removeAdmin_Should_RemoveAdminRole_When_Called(){

        //Arrange
        User user = MockFactory.mockUser();
        UserDTO userDTO = MockFactory.mockUserDTO();
        Role role = new Role();
        role.setRole("ADMIN");
        role.setRoleID(1);

        Mockito.when(userRepository.getByName(user.getUsername())).thenReturn(user);

        //Act
        UserDTO userDTO1 = userService.removeAdmin(userDTO);

        //Assert

        Assert.assertEquals(userDTO.getUsername(),userDTO1.getUsername());
    }


    @Test
    public void changeUserStatus_Should_ChangeStatus_When_Called(){

        //Arrange
        User user = MockFactory.mockUser();
        String id = "Slav";
        boolean b = false;

        Mockito.when(userRepository.getByName(id)).thenReturn(user);
        Mockito.when(userRepository.updateUserDetails(user)).thenReturn(user);

        //Act

        User user1 = userService.changeUserStatus(id,b);

        //Assert
        Assert.assertEquals(user.getFirstName(),user1.getFirstName());

    }
}
