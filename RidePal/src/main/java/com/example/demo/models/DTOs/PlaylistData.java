package com.example.demo.models.DTOs;

public class PlaylistData {

    private String genreName;

    private int percentage;


    public PlaylistData(){
    }

    public PlaylistData(String genreName){
        this.genreName=genreName;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

}
