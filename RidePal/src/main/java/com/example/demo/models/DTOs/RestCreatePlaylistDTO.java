package com.example.demo.models.DTOs;

import java.util.HashMap;
import java.util.Map;

public class RestCreatePlaylistDTO {

    private String title;

    private int duration;

    private Map<String,Integer> genreRestMap = new HashMap<>();

    private String userID = "EXTERNAL";

    private boolean sameArtist;

    private boolean topTracks;

    public RestCreatePlaylistDTO(){}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Map<String, Integer> getGenreRestMap() {
        return genreRestMap;
    }

    public void setGenreRestMap(Map<String, Integer> genreRestMap) {
        this.genreRestMap = genreRestMap;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public boolean isSameArtist() {
        return sameArtist;
    }

    public void setSameArtist(boolean sameArtist) {
        this.sameArtist = sameArtist;
    }

    public boolean isTopTracks() {
        return topTracks;
    }

    public void setTopTracks(boolean topTracks) {
        this.topTracks = topTracks;
    }
}
