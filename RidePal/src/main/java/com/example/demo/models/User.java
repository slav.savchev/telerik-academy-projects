package com.example.demo.models;


import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users")
@SecondaryTable(name = "user_details")
public class User {
    
    @Id
    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(table = "user_details", name = "Email")
    private String email;

    @Column(table = "user_details", name = "FirstName")
    private String firstName;

    @Column(table = "user_details", name = "LastName")
    private String lastName;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "authorities",
            joinColumns = {@JoinColumn(name = "username")},
            inverseJoinColumns = {@JoinColumn(name = "authority")})
    Set<Role> roles = new HashSet<>();

    @Column(table = "users", name = "enabled")
    private boolean enabled=true;

    public User() {
    }

    public User(String username, String email, String firstName, String lastName) {
        this.username = username;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public Set<Role> addRole(Role role ){
        if(roles == null){
            roles = new HashSet<>();
        }
        roles.add(role);

        return roles;
    }

    public Set<Role> removeRole(Role role) {
        if(roles == null){
            roles = new HashSet<>();
        }
        roles.remove(role);
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
