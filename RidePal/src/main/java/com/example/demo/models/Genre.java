package com.example.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@SQLDelete(sql = "update genres SET active = false where GenreID = ?", check = ResultCheckStyle.COUNT)
@Table(name = "genres")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Genre {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "GenreID")
    private int id;

    @JsonProperty("name")
    @Column(name = "GenreName")
    private String name;

    @ManyToMany(mappedBy = "genres")
    private Set<Playlist> playlists = new HashSet<>();

    @JsonProperty("id")
    @Column(name="DeezerGenreId")
    private int deezerGenreId;

    @Column(name="Active")
    private boolean active;

    public Genre() {
    }

    public Genre(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    @JsonIgnore
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Playlist> getPlaylists() {
        return playlists;
    }

    public void setPlaylists(Set<Playlist> playlists) {
        this.playlists = playlists;
    }

    public int getDeezerGenreId() {
        return deezerGenreId;
    }

    public void setDeezerGenreId(int deezerGenreId) {
        this.deezerGenreId = deezerGenreId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        Genre other = (Genre)obj;
        if(!name.equals(other.getName())){
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
