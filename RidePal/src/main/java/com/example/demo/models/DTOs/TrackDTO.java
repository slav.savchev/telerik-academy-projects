package com.example.demo.models.DTOs;

public class TrackDTO {

    private String title;

    private String preview;

    private String link;

    private int duration;

    private String artistName;

    private String albumName;

    private int rank;

    public TrackDTO() {
    }

    public TrackDTO(String title, String preview, String link, int duration, String artistName, String albumName, int rank) {
        this.title = title;
        this.preview = preview;
        this.link = link;
        this.duration = duration;
        this.artistName = artistName;
        this.albumName = albumName;
        this.rank = rank;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}

