package com.example.demo.models.DTOs;

import com.example.demo.models.Track;

import javax.validation.constraints.Size;
import java.util.*;

import static com.example.demo.utils.Constants.MESSAGE_TITLE_SIZE;

public class PlaylistDTO {


    @Size(min = 1, max = 50, message = MESSAGE_TITLE_SIZE)
    private String title;

    private Map<String,Integer> genrePref = new HashMap<>();

    private boolean topTracks;

    private boolean sameArtist;

    private String userID;

    private int duration;

    public PlaylistDTO() {
    }

    public Map<String, Integer> getGenrePref() {
        return genrePref;
    }

    public void setGenrePref(Map<String, Integer> genrePref) {
        this.genrePref = genrePref;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isTopTracks() {
        return topTracks;
    }

    public void setTopTracks(boolean topTracks) {
        this.topTracks = topTracks;
    }

    public boolean isSameArtist() {
        return sameArtist;
    }

    public void setSameArtist(boolean sameArtist) {
        this.sameArtist = sameArtist;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }


}

