package com.example.demo.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Objects;

@Entity
@SQLDelete(sql = "update tracks SET active = false where TrackID = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "Active <> false")
@Table(name = "tracks")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Track {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TrackID")
    private int id;

    @Column(name = "Title")
    private String title;

    @Column(name = "PreviewURL")
    private String preview;

    @Column(name = "Link")
    private String link;

    @Column(name = "Duration")
    private int duration;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "ArtistID")
    private Artist artist;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "AlbumID")
    private Album album;

    @Column(name = "Rank")
    private int rank;

    @Column(name = "Genre")
    private String genre;

    @JsonProperty(value = "id")
    @Column(name = "DeezerID")
    private long deezerID;

    public Track(){
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getId() {
        return id;
    }

    @JsonIgnore
    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public long getDeezerID() {
        return deezerID;
    }

    public void setDeezerID(long deezerID) {
        this.deezerID = deezerID;
    }

    @Override
    public boolean equals(Object obj) {
        Track other = (Track)obj;
        if(!link.equals(other.link)){
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(link);
    }
}
