package com.example.demo.models.DTOs;

import java.util.List;

public class PlaylistDataForm {

    private List<PlaylistData> playlistDataList;

    private String title;

    private int duration;

    private boolean sameArtist;

    private boolean topTracks;

    public PlaylistDataForm(List<PlaylistData> playlistDataList) {
        this.playlistDataList = playlistDataList;
    }

    public List<PlaylistData> getPlaylistDataList() {
        return playlistDataList;
    }

    public void setPlaylistDataList(List<PlaylistData> playlistDataList) {
        this.playlistDataList = playlistDataList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public boolean isSameArtist() {
        return sameArtist;
    }

    public void setSameArtist(boolean sameArtist) {
        this.sameArtist = sameArtist;
    }

    public boolean isTopTracks() {
        return topTracks;
    }

    public void setTopTracks(boolean topTracks) {
        this.topTracks = topTracks;
    }
}
