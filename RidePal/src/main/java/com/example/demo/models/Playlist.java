package com.example.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
//@SQLDelete(sql = "update playlists SET active = false where PlaylistID = ?", check = ResultCheckStyle.COUNT)
//@Where(clause = "Active <> false")
@Table(name = "playlists")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Playlist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PlaylistID")
    private int id;

    @Column(name = "PlaylistTitle")
    private String title;

    @Column (name = "Playtime")
    private int playtime;

    @Column (name = "Rank")
    private int rank;

    @Column (name = "UserID")
    private String userID;

    @Column (name = "DeezerID")
    private long deezerID;

    @Column(name="Active")
    private boolean active=true;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable( name = "playlists_tracks",
            joinColumns = { @JoinColumn(name = "playlist_id") },
            inverseJoinColumns = { @JoinColumn(name = "track_id") }
    )
    private Set<Track> tracks = new HashSet<>();


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable( name = "playlists_genres",
            joinColumns = { @JoinColumn(name = "playlist_id") },
            inverseJoinColumns = { @JoinColumn(name = "genre_id") }
    )
    private Set<Genre> genres = new HashSet<>();

    public Playlist(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPlaytime() {
        return playtime;
    }

    public void setPlaytime(int playtime) {
        this.playtime = playtime;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public Set<Track> getTracks() {
        return tracks;
    }

    public void setTracks(Set<Track> tracks) {
        this.tracks = tracks;
    }

    public Set<Genre> getGenres() {
        return genres;
    }

    public void setGenres(Set<Genre> genres) {
        this.genres = genres;
    }

    public long getDeezerID() {
        return deezerID;
    }

    public void setDeezerID(long deezerID) {
        this.deezerID = deezerID;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
