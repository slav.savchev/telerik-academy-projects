package com.example.demo.models;

import com.example.demo.models.DTOs.*;

import java.util.*;
import java.util.stream.Collectors;

public class DtoMapper {

    public static UserDTO toDto(User user){

        UserDTO userDTO = new UserDTO(user.getUsername(), user.getEmail(), user.getFirstName(), user.getLastName());
        userDTO.setEnabled(user.isEnabled());
        boolean isAdmin = user.getRoles().stream()
                .map(Role::getRole)
                .collect(Collectors.toList())
                .contains("ROLE_ADMIN");
        userDTO.setAdmin(isAdmin);
        return userDTO;

    }

    public static User fromDto(UserDTO userDto){

        return new User(userDto.getUsername(), userDto.getEmail(), userDto.getFirstName(), userDto.getLastName());
    }


    public static PlaylistDTO toPlaylistDTO(PlaylistDataForm form, String userID){

        List<PlaylistData> selectedGenres = form.getPlaylistDataList().stream()
                .filter(genre-> genre.getPercentage()>0)
                .collect(Collectors.toList());

        PlaylistDTO playlistDTO = new PlaylistDTO();
        playlistDTO.setTitle(form.getTitle());
        playlistDTO.setDuration(form.getDuration());
        playlistDTO.setUserID(userID);
        playlistDTO.setSameArtist(form.isSameArtist());
        playlistDTO.setTopTracks(form.isTopTracks());

        Map<String,Integer> genreMap = new HashMap<>();

        for (int i = 0; i <selectedGenres.size() ; i++) {
            genreMap.put(selectedGenres.get(i).getGenreName(),normalizePercentages(selectedGenres,selectedGenres.get(i).getPercentage()));
        }

        playlistDTO.setGenrePref(genreMap);

        return playlistDTO;

    }

    public static List<DisplayPlaylistDTO> toDisplayPlaylistDTO(List<Playlist> playlists){

        List<DisplayPlaylistDTO> displayPlaylistDTOList = new ArrayList<>();

        for (int i = 0; i <playlists.size() ; i++) {

            DisplayPlaylistDTO displayPlaylistDTO = new DisplayPlaylistDTO();
            displayPlaylistDTO.setId(playlists.get(i).getId());
            displayPlaylistDTO.setTitle(playlists.get(i).getTitle());
            displayPlaylistDTO.setUserID(playlists.get(i).getUserID());
            displayPlaylistDTO.setRank(playlists.get(i).getRank());
            StringBuffer stringBuffer = new StringBuffer("");
            for (Genre elem:
                 playlists.get(i).getGenres()) {

                stringBuffer.append(elem.getName() + "  ");
            }
            displayPlaylistDTO.setGenres(String.valueOf(stringBuffer));

            int secs = playlists.get(i).getPlaytime();
            int mins = (int) (secs * 0.016666667);
            displayPlaylistDTO.setMinutes(mins + " MINUTES");

            displayPlaylistDTOList.add(displayPlaylistDTO);
        }

        return displayPlaylistDTOList;
    }

    public static GenreDTO toDto(Genre genre){
        return new GenreDTO(genre.getId(), genre.getName(), genre.getDeezerGenreId(), genre.isActive());
    }
    public static DisplayPlaylistDTO toDisplayPlaylistDTO(Playlist playlist){
        DisplayPlaylistDTO displayPlaylistDTO = new DisplayPlaylistDTO();

        displayPlaylistDTO.setId(playlist.getId());
        displayPlaylistDTO.setTitle(playlist.getTitle());
        displayPlaylistDTO.setUserID(playlist.getUserID());
        displayPlaylistDTO.setRank(playlist.getRank());
        StringBuffer stringBuffer = new StringBuffer("");
        for (Genre elem:
                playlist.getGenres()) {

            stringBuffer.append(elem.getName() + "  ");
        }
        displayPlaylistDTO.setGenres(String.valueOf(stringBuffer));

        int secs = playlist.getPlaytime();
        int mins = (int) (secs * 0.016666667);
        displayPlaylistDTO.setMinutes(mins + " MINUTES");


        return displayPlaylistDTO;

    }


    private static int normalizePercentages(List<PlaylistData> selectedGenres, int currentPercentage){
        int totalPercentage=0;
        for (int i = 0; i <selectedGenres.size(); i++) {
            totalPercentage += selectedGenres.get(i).getPercentage();
        }
        double coeficient = (double)100/totalPercentage;
        double result = currentPercentage*coeficient;
        return (int)result;
    }

    private static int normalizePercentages(int totalPercentage, int currentPercentage){

        double coeficient = (double)100/totalPercentage;
        double result = currentPercentage*coeficient;
        return (int)result;
    }



    public static PlaylistDTO RestToPlaylistDTO(RestCreatePlaylistDTO restCreatePlaylistDTO){
        if(restCreatePlaylistDTO.getGenreRestMap().size()==0){
            throw new IllegalArgumentException("Please add genres.");
        }
        if(restCreatePlaylistDTO.getDuration() <= 4){
            throw new IllegalArgumentException("Duration should be more than 4 minutes.");
        }

        PlaylistDTO playlistDTO = new PlaylistDTO();
        playlistDTO.setTitle(restCreatePlaylistDTO.getTitle());
        playlistDTO.setDuration(restCreatePlaylistDTO.getDuration()*60);
        playlistDTO.setUserID("EXTERNAL");

        playlistDTO.setSameArtist(restCreatePlaylistDTO.isSameArtist());
        playlistDTO.setTopTracks(restCreatePlaylistDTO.isTopTracks());

        Set<String> keySet = restCreatePlaylistDTO.getGenreRestMap().keySet();
        int totalPercentage = 0;
        for (String elem:
                keySet) {
            int value = restCreatePlaylistDTO.getGenreRestMap().get(elem);
            totalPercentage+=value;
            playlistDTO.getGenrePref().put(elem,value);
        }

        Map<String,Integer> map = playlistDTO.getGenrePref();
        Set<String> keySet2 = map.keySet();
        for (String elem:
             keySet2) {
            map.put(elem,normalizePercentages(totalPercentage,map.get(elem)));
        }

        return playlistDTO;
    }
}
