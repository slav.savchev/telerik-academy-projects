package com.example.demo.controllers.restControllers;


import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.DTOs.DisplayPlaylistDTO;
import com.example.demo.models.DTOs.PlaylistDTO;
import com.example.demo.models.DTOs.RestCreatePlaylistDTO;
import com.example.demo.models.DtoMapper;
import com.example.demo.models.Genre;
import com.example.demo.models.Playlist;
import com.example.demo.services.contracts.GenreService;
import com.example.demo.services.contracts.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/v1")
public class PlaylistRestController {

    private PlaylistService playlistService;
    private GenreService genreService;

    @Autowired
    public PlaylistRestController(PlaylistService playlistService, GenreService genreService){
        this.playlistService = playlistService;
        this.genreService = genreService;
    }

    @GetMapping("/playlist/{id}")
    public DisplayPlaylistDTO getPlaylistByID(@PathVariable int id){
        try{

            return DtoMapper.toDisplayPlaylistDTO(playlistService.getById(id));

        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @GetMapping("/playlist/all")
    public List<DisplayPlaylistDTO> getAllPlaylists(){

        return DtoMapper.toDisplayPlaylistDTO(playlistService.getAllPlaylists());
    }


    @PutMapping("/playlist/{id}")
    public DisplayPlaylistDTO updatePlaylist(@PathVariable int id, @RequestBody @Valid DisplayPlaylistDTO displayPlaylistDTO){
        try{

            return playlistService.editPlaylist(displayPlaylistDTO);

        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/playlist/{id}")
    public void deletePlaylist(@PathVariable int id){
        try {

            playlistService.changePlaylistStatus(id,false);

        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @PostMapping("/playlist/create")
    public Playlist createPlaylist(@RequestBody RestCreatePlaylistDTO restCreatePlaylistDTO){
        try{
            PlaylistDTO playlistDTO = DtoMapper.RestToPlaylistDTO(restCreatePlaylistDTO);

            return playlistService.createPlaylist(playlistDTO);

        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED);
        }
    }
}
