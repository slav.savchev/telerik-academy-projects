package com.example.demo.controllers.restControllers;

import com.example.demo.external.api.model.GeoLocation;
import com.example.demo.utils.BingMapsHelper;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1")
public class RestRouteController {

    @PostMapping(value = "/geolocation" ,produces = "application/json")
    public GeoLocation calculateRoute(@RequestBody String json){
        String bingKey = "&key=" + System.getenv("BingAPI_key");
        String urlOne = "http://dev.virtualearth.net/REST/V1/Routes?wp.0=";
        String urlTwo = "&wp.1=";

        JSONObject jsonObject = new JSONObject(json);

        String startPoint = jsonObject.getString("startLocation");
        startPoint = startPoint.replace(" ","%");
        String endPoint = jsonObject.getString("endLocation");
        endPoint = endPoint.replace(" ","%");
        GeoLocation geoLocation = new GeoLocation();

        String url = urlOne.concat(startPoint).concat(urlTwo).concat(endPoint).concat(bingKey);
        try{
            geoLocation = BingMapsHelper.calculateTravelDuration(url);

        } catch (IOException e){
            e.getMessage();
        }

        return geoLocation;
    }
}
