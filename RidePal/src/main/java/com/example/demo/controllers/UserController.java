package com.example.demo.controllers;

import com.example.demo.models.DTOs.UserDTO;
import com.example.demo.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
public class UserController {
    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }


    //for admin
    @GetMapping("/profile/{username}")
    public String showEditUserForm(Model model, @PathVariable String username) {

        model.addAttribute("user", userService.getByName(username));

        return "profile";
    }

    @GetMapping("/user/profile")
    public String showProfile(Model model, Principal principal) {
        model.addAttribute("user", userService.getByName(principal.getName()));
        return "profile";
    }

    @PostMapping("/user/profile")
    public String editProfile(Model model,
                              @Valid @ModelAttribute("user") UserDTO userDto, Principal principal, BindingResult bindingResult) {

        if(bindingResult.hasErrors()){
            model.addAttribute("user", userService.getByName(principal.getName()));
            return "profile";
        }

        userDto.setUsername(principal.getName());
        model.addAttribute("user", userService.editUserProfile(userDto));
        return "profile";
    }

    //for admin
    @PostMapping("/profile/{username}")
    public String editUserProfile(Model model, @PathVariable String username,
                                  @Valid @ModelAttribute("user") UserDTO userDto) {
        model.addAttribute("user", userService.editUserProfile(userDto));
        return "profile";
    }

    @GetMapping("/profile/{username}/change-password")
    public String showPasswordForm(@PathVariable String username, Model model, Principal principal) {
        model.addAttribute("user", userService.getByName(username));
        return "change-password";
    }

    @PostMapping("/profile/{username}/change-password")
    public String changePassword(@PathVariable String username,
                                 @ModelAttribute("user") UserDTO user,
                                 Model model) {
        if (!user.getPassword().equals(user.getPasswordConfirmation())) {
            model.addAttribute("user", userService.getByName(username));
            model.addAttribute("message", "Password doesn't match!");
            return "change-password";
        }
        userService.changePassword(username, user.getPassword());
        model.addAttribute("user", userService.getByName(username));
        model.addAttribute("message", "Password changed successfully!");
        return "change-password";
    }

    @GetMapping("/profile/{username}/make-admin")
    public String giveUserAdminRole( @PathVariable String username,
                                     @Valid @ModelAttribute("user") UserDTO userDto, Model model){

        UserDTO newUserDto = userService.makeAdmin(userDto);
        model.addAttribute("user", newUserDto);
        return "profile";
    }

    @GetMapping("/profile/{username}/remove-admin")
    public String removeUserAdminRole( @PathVariable String username,
                                     @Valid @ModelAttribute("user") UserDTO userDto, Model model){

        UserDTO newUserDto = userService.removeAdmin(userDto);
        model.addAttribute("user", newUserDto);
        return "profile";
    }

    @GetMapping("/admin/users")
    public String showAllUsers(Model model) {

        List<UserDTO> allUsers = userService.getAllUsersDtos();
        model.addAttribute("users", allUsers);
        return "users";

    }

    @GetMapping("/admin/user/{userID}/activate")
    public String activateUser(@PathVariable String userID){

        userService.changeUserStatus(userID,true);

        return "redirect:/admin/users";
    }

    @GetMapping("/admin/user/{userID}/deactivate")
    public String deactivateUser(@PathVariable String userID){

        userService.changeUserStatus(userID,false);

        return "redirect:/admin/users";
    }

}
