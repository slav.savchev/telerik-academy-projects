package com.example.demo.controllers;

import com.example.demo.models.DTOs.DisplayPlaylistDTO;
import com.example.demo.models.DtoMapper;
import com.example.demo.models.Playlist;
import com.example.demo.models.Track;
import com.example.demo.services.contracts.GenreService;
import com.example.demo.services.contracts.PlaylistService;
import com.example.demo.services.contracts.UserService;
import com.example.demo.utils.JSONutils;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.*;

@Controller
public class PlaylistController {

    private PlaylistService playlistService;
    private UserService userService;
    private GenreService genreService;

    @Autowired
    public PlaylistController(PlaylistService playlistService, UserService userService, GenreService genreService){
        this.playlistService = playlistService;
        this.userService = userService;
        this.genreService = genreService;
    }

    @GetMapping("/playlist/{id}")
    public String getPlaylist(@PathVariable int id, Model model, Principal principal){

        if(id == -1){
            Playlist playlist = new Playlist();
            playlist.setTitle("Rock Road Trip");
            playlist.setDeezerID(3126664682L);
            model.addAttribute("playlist",playlist);
        } else if(id == -2){
            Playlist playlist = new Playlist();
            playlist.setTitle("Rap Bangers");
            playlist.setDeezerID(1996494362L);
            model.addAttribute("playlist",playlist);
        } else if(id == -3){
            Playlist playlist = new Playlist();
            playlist.setTitle("Today's Best Pop");
            playlist.setDeezerID(1963962142L);
            model.addAttribute("playlist",playlist);
        } else {
            Playlist toBeDisplayed = playlistService.getById(id);

            model.addAttribute("playlist",toBeDisplayed);
        }

        return "playlist";
    }


    @GetMapping("/user/playlists")
    public String getPlaylists(Principal principal, Model model,@RequestParam (defaultValue = "0") int page ){



        List<DisplayPlaylistDTO> playlistList = DtoMapper.toDisplayPlaylistDTO(playlistService.getPlaylistsByUserID(principal.getName()));

        List<List<DisplayPlaylistDTO>> pageList;
        try{
            pageList = JSONutils.getPages(playlistList);
        } catch (IndexOutOfBoundsException e){
            pageList = new ArrayList<>();
        }

        if(pageList.size() !=0){
            model.addAttribute("numOfPages", pageList.size()-1);
        } else {
            model.addAttribute("numOfPages", pageList.size());
        }

        List<DisplayPlaylistDTO> requestedPage;

        if(pageList.size()!=0){
            requestedPage = pageList.get(page);
        } else {
            requestedPage = new ArrayList<>();
        }
        model.addAttribute("pages", requestedPage);
        model.addAttribute("playlistsList",playlistList);
        model.addAttribute("user", principal.getName());

        return "playlists";
    }

    @GetMapping("/user/playlists/{userID}")
    public String getUserPlaylists(@PathVariable String userID, Model model,@RequestParam (defaultValue = "0") int page ){



        List<DisplayPlaylistDTO> playlistList = DtoMapper.toDisplayPlaylistDTO(playlistService.getPlaylistsByUserID(userID));

        List<List<DisplayPlaylistDTO>> pageList;
        try{
            pageList = JSONutils.getPages(playlistList);
        } catch (IndexOutOfBoundsException e){
            pageList = new ArrayList<>();
        }

        if(pageList.size() !=0){
            model.addAttribute("numOfPages", pageList.size()-1);
        } else {
            model.addAttribute("numOfPages", pageList.size());
        }

        List<DisplayPlaylistDTO> requestedPage;

        if(pageList.size()!=0){
            requestedPage = pageList.get(page);
        } else {
            requestedPage = new ArrayList<>();
        }
        model.addAttribute("pages", requestedPage);
        model.addAttribute("playlistsList",playlistList);
        model.addAttribute("user", userID);

        return "playlists";
    }

    @GetMapping("/playlists/all")
    public String getAllPlaylists(Model model, @RequestParam (defaultValue = "DESC") String sort,
                                  @RequestParam (required = false) String duration,
                                  @RequestParam (required = false) String genre,
                                  @RequestParam (defaultValue = "0") int page){

        Map<Integer,String> flagMapMVC = new HashMap<>();
        flagMapMVC.put(0,sort);
        if(genre != null){
            flagMapMVC.put(1,genre);
        }
        if(duration != null){
            flagMapMVC.put(2,duration);
        }

        List<DisplayPlaylistDTO> playlistDTOList = DtoMapper.toDisplayPlaylistDTO(playlistService.getAllPlaylists(flagMapMVC));
        int[] minAndMax = playlistService.getMinAndMaxDuration();

        if(genre == null && duration == null){
            List<List<DisplayPlaylistDTO>> pageList;
            try{
                pageList = JSONutils.getPages(playlistDTOList);
            } catch (IndexOutOfBoundsException e){
                pageList = new ArrayList<>();
            }

            if(pageList.size() !=0){
                model.addAttribute("numOfPages", pageList.size()-1);
            } else {
                model.addAttribute("numOfPages", pageList.size());
            }

            List<DisplayPlaylistDTO> requestedPage;

            try{
                if(pageList.size()!=0){
                    requestedPage = pageList.get(page);
                } else {
                    requestedPage = new ArrayList<>();
                }

                model.addAttribute("pages", requestedPage);
                model.addAttribute("user","All");
                model.addAttribute("allGenres",genreService.getActiveGenres());
                model.addAttribute("minDuration",minAndMax[0]);
                model.addAttribute("maxDuration",minAndMax[1]);
            } catch (IndexOutOfBoundsException e){
                e.printStackTrace();
            }
        } else {
            model.addAttribute("pages", playlistDTOList);
            model.addAttribute("user","All");
            model.addAttribute("allGenres",genreService.getActiveGenres());
            model.addAttribute("minDuration",minAndMax[0]);
            model.addAttribute("maxDuration",minAndMax[1]);
            model.addAttribute("numOfPages",0);
        }


        return "allPlaylists";
    }


    @GetMapping("/admin/playlists")
    public String editPlaylists(Principal principal, Model model){

        List<Playlist> playlistList = playlistService.getAllPlaylistsAdmin();
        model.addAttribute("playlists",playlistList);


        return "adminPlaylists";
    }

    @GetMapping("/admin/playlist/{playlistID}/activate")
    public String activatePlaylist(@PathVariable int playlistID){

        playlistService.changePlaylistStatus(playlistID,true);

        return "redirect:/admin/playlists";
    }

    @GetMapping("/admin/playlist/{playlistID}/deactivate")
    public String deactivatePlaylist(@PathVariable int playlistID){


        playlistService.changePlaylistStatus(playlistID,false);

        return "redirect:/admin/playlists";
    }

    @GetMapping("/playlist/{playlistID}/deactivate")
    public String deactivatePlaylistAsOwner(@PathVariable int playlistID, Principal principal){
        playlistService.changePlaylistStatus(playlistID,false);

        return "redirect:/user/playlists";
    }

    @GetMapping("/playlist/{id}/update")
    public String updatePlaylist(@PathVariable int id, Model model){

        DisplayPlaylistDTO displayPlaylistDTO = DtoMapper.toDisplayPlaylistDTO(playlistService.getById(id));

        model.addAttribute("playlist",displayPlaylistDTO);
        model.addAttribute("allGenres",genreService.getActiveGenres());


        return "editPlaylist";
    }

    @PostMapping("/playlist/{id}/update")
    public String updatePlaylistDetails(@PathVariable int id, Model model, @ModelAttribute DisplayPlaylistDTO playlist){

        model.addAttribute("playlist",playlistService.editPlaylist(playlist));
        model.addAttribute("allGenres",genreService.getActiveGenres());

        return "editPlaylist";
    }

    @PutMapping("playlist/deactivate/{playlistID}")
    public String deletePlaylist(@PathVariable int playlistID){

        playlistService.changePlaylistStatus(playlistID,false);

        return "redirect:/user/playlists";
    }

    @GetMapping("/playlist/{id}/tracks")
    public String getTrackList(@PathVariable int id, Model model){

        Set<Track> trackSet = playlistService.getById(id).getTracks();

        model.addAttribute("tracks",trackSet);

        return "playlistTracks";
    }
}
