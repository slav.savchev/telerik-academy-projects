package com.example.demo.controllers.restControllers;

import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.DTOs.UserDTO;
import com.example.demo.models.DtoMapper;
import com.example.demo.models.User;
import com.example.demo.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class UserRestController {

    private UserService userService;

    @Autowired
    public UserRestController(UserService userService){
        this.userService = userService;
    }

    @GetMapping("/user/{id}")
    public UserDTO getPlaylistByID(@PathVariable String id){
        try{

            return userService.getByName(id);

        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @GetMapping("/user/all")
    public List<UserDTO> getAllUser(){
        try{

            return userService.getAllUsersDtos();

        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED);
        }
    }

    @PutMapping("/user/{userID}")
    public UserDTO updateUser(@RequestBody UserDTO updated,@PathVariable String userID){
        try{
            updated.setUsername(userID);
            UserDTO userDTO1 = userService.getByName(userID);
            userDTO1.setUsername(updated.getUsername());
            userDTO1.setFirstName(updated.getFirstName());
            userDTO1.setLastName(updated.getLastName());
            userDTO1.setEmail(updated.getEmail());

            return userService.editUserProfile(userDTO1);

        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @DeleteMapping("/user/{userID}")
    public void deleteUser(@PathVariable String userID){
        try{

            userService.changeUserStatus(userID,false);

        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }
    
}
