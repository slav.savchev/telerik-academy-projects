package com.example.demo;

import com.example.demo.services.contracts.GenreService;
import com.example.demo.services.contracts.TrackService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@SpringBootApplication
@EnableSwagger2
public class DemoApplication {


    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    CommandLineRunner runner(GenreService genreService, TrackService trackService){
        return args -> {
//            genreService.syncGenres();
//            trackService.syncTracks();
        };
    }

    @Bean
    public Docket swaggerConfiguration() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.demo"))
                .build()
                .apiInfo(apiDetails());

    }

    private ApiInfo apiDetails() {
        return new ApiInfo(
                "Ride Pal API",
                "Ride Pal API",
                "1.0",
                "Free to use",
                new springfox.documentation.service.Contact
                        ("Julian Ivanov", "http://demo.example.com", "slav.savchev@abv.bg"),
                "API License",
                "http://demo.example.com",
                Collections.emptyList());


    }

}
