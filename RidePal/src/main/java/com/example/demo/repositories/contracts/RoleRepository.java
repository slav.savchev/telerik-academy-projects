package com.example.demo.repositories.contracts;

import com.example.demo.models.Role;

public interface RoleRepository {

    Role getRole(String role);
}
