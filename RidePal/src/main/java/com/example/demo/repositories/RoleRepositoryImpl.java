package com.example.demo.repositories;

import com.example.demo.models.Role;
import com.example.demo.repositories.contracts.RoleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RoleRepositoryImpl implements RoleRepository {


    private SessionFactory sessionFactory;

    @Autowired
    public RoleRepositoryImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Role getRole(String role) {
        try(Session session = sessionFactory.openSession()){
            Query<Role> roleQuery = session.createQuery("from Role where role = :role",Role.class);
            roleQuery.setParameter("role",role);
            return roleQuery.list().get(0);
        }
    }
}
