package com.example.demo.repositories;

import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.Genre;
import com.example.demo.repositories.contracts.GenreRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GenreRepositoryImpl implements GenreRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public GenreRepositoryImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Genre createGenre(Genre genre) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(genre);

            session.getTransaction().commit();

            return genre;
        }
    }

    public List<Genre> createGenres(List<Genre> genreList) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            for (Genre genre : genreList) {
                session.save(genre);
            }
            session.getTransaction().commit();

            return genreList;
        }
    }

    @Override
    public Genre getById(int id) {
        try(Session session = sessionFactory.openSession()){
            return session.get(Genre.class, id);
        }
    }

    @Override
    public List<Genre> getActiveGenres() {
        try(Session session = sessionFactory.openSession()){
            Query<Genre> genreQuery = session.createQuery("from Genre where active = true",Genre.class);
            return genreQuery.list();
        }
    }

    @Override
    public boolean checkGenreExists(String name) {
        try(Session session = sessionFactory.openSession()){
            Query<Genre> genreQuery = session.createQuery("from Genre where name = :name ",Genre.class);
            genreQuery.setParameter("name",name);
            return genreQuery.list().size() != 0;
        }
    }

    @Override
    public Genre getGenreByName(String genreName) {
        try(Session session = sessionFactory.openSession()){
            Query<Genre> genreQuery = session.createQuery("from Genre where name = :name",Genre.class);
            genreQuery.setParameter("name",genreName);
            return genreQuery.list().get(0);
        }
    }

    @Override
    public List<Genre> getAllGenres(){
        try(Session session = sessionFactory.openSession()){
            Query<Genre> genreQuery = session.createQuery("from Genre", Genre.class);
            return genreQuery.list();
        }
    }

    @Override
    public Genre updateGenre(Genre genre) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(genre);
            session.getTransaction().commit();
            return genre;
        }
    }

    @Override
    public int countTracksInGenre(String genre){
        try(Session session = sessionFactory.openSession()){
            Query<Long> genreQuery = session.createQuery(
                    "select count(*) from Track t where t.genre = :genre", Long.class);
            genreQuery.setParameter("genre",genre);
            return genreQuery.getFirstResult();

        }
    }
}
