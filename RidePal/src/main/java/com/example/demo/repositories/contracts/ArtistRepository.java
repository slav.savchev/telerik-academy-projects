package com.example.demo.repositories.contracts;

import com.example.demo.models.Artist;

import java.util.List;

public interface ArtistRepository {

    void createArtist(Artist artist);

    List<Artist> createArtist(List<Artist> artistList);

    Artist getById(int id);

    List<Artist> getAllArtists();

    boolean checkArtistExists(String artistName);

    Artist getArtistByName(String name);
}
