package com.example.demo.repositories;

import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.User;
import com.example.demo.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }


    @Override
    public User createUser(User user) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(user);

            session.getTransaction().commit();

            return user;
        }
    }

    @Override
    public User getByName(String username) {
        try(Session session = sessionFactory.openSession()){
            User user = session.get(User.class, username);
            if(user == null){
                throw new EntityNotFoundException(String.format("User with name '%s' does not exist.",username));
            }
            return user;
        }
    }

    @Override
    public List<User> getAllUsers() {
        try(Session session = sessionFactory.openSession()){
            Query<User> userQuery = session.createQuery("from User",User.class);

            return userQuery.list();
        }
    }

    @Override
    public User updateUserDetails(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
            return user;
        }
    }
}
