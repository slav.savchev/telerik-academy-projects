package com.example.demo.repositories.contracts;

import com.example.demo.models.Album;

import java.util.List;

public interface AlbumRepository {

    void createAlbum(Album album);

    List<Album> createAlbum(List<Album> albumList);

    Album getById(int id);

    List<Album> getAllAlbums();

    boolean checkAlbumExists(String albumName);

    Album getAlbumByName(String albumName);
}
