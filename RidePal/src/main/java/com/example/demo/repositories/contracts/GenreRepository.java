package com.example.demo.repositories.contracts;

import com.example.demo.models.Genre;

import java.util.List;

public interface GenreRepository {

    Genre createGenre(Genre genre);

    List<Genre> createGenres(List<Genre> genreList);

    Genre getById(int id);

    List<Genre> getActiveGenres();

    boolean checkGenreExists(String name);

    Genre getGenreByName(String genreName);

    List<Genre> getAllGenres();

    Genre updateGenre(Genre genre);

    int countTracksInGenre(String genre);
}
