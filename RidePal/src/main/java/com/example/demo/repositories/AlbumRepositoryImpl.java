package com.example.demo.repositories;

import com.example.demo.models.Album;
import com.example.demo.repositories.contracts.AlbumRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AlbumRepositoryImpl implements AlbumRepository {


    private SessionFactory sessionFactory;

    @Autowired
    public AlbumRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public void createAlbum(Album album) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(album);

            session.getTransaction().commit();
        }
    }

    @Override
    public List<Album> createAlbum(List<Album> albumList) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            for (int i = 0; i < albumList.size(); i++) {
                session.save(albumList.get(i));
            }
            session.getTransaction().commit();

            return albumList;
        }
    }

    @Override
    public Album getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Album.class, id);
        }
    }

    @Override
    public List<Album> getAllAlbums() {
        try (Session session = sessionFactory.openSession()) {
            Query<Album> albumQuery = session.createQuery("from Album", Album.class);

            return albumQuery.list();
        }
    }

    @Override
    public boolean checkAlbumExists(String albumName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Album> albumQuery = session.createQuery("from Album where title = :title ", Album.class);
            albumQuery.setParameter("title", albumName);
            if (albumQuery.list().size() != 0) {
                return true;
            } else {
                return false;
            }
        }

    }

    @Override
    public Album getAlbumByName(String albumName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Album> albumQuery = session.createQuery("from Album where title = :title", Album.class);
            albumQuery.setParameter("title", albumName);

            return albumQuery.list().get(0);
        }
    }
}
