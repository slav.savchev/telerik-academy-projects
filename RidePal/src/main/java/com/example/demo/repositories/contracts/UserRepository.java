package com.example.demo.repositories.contracts;

import com.example.demo.models.User;

import java.util.List;

public interface UserRepository {

    User createUser(User user);

    User getByName(String username);

    List<User> getAllUsers();

    User updateUserDetails(User user);
}
