package com.example.demo.repositories.contracts;

import com.example.demo.models.Track;

import java.util.List;

public interface TrackRepository {
    void createTrack(Track track, String genre);

    List<Track> createTrack(List<Track> trackList);

    Track getById(int id);

    List<Track> getAllTracks();

    boolean checkTrackExists(String trackName, String previewUrl);

    long countTracksPerGenre(String genre);

    List<Track> getTracksByGenre(String genre);

    List<Track> getTopTracksByGenre(String genre);

    List<Track> getTracksByGenreAndTime(String genre, int timeRemaining);
}
