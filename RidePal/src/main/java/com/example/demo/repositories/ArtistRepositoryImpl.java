package com.example.demo.repositories;

import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.Artist;
import com.example.demo.repositories.contracts.ArtistRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class ArtistRepositoryImpl implements ArtistRepository {


    private SessionFactory sessionFactory;

    @Autowired
    public ArtistRepositoryImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }


    @Override
    public void createArtist(Artist artist) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(artist);

            session.getTransaction().commit();


        }
    }

    @Override
    public List<Artist> createArtist(List<Artist> artistList) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            for (int i = 0; i < artistList.size(); i++) {
                session.save(artistList.get(i));
            }
            session.getTransaction().commit();

            return artistList;
        }
    }

    @Override
    public Artist getById(int id) {
        try(Session session = sessionFactory.openSession()){
            Artist artist = session.get(Artist.class, id);
            if(artist == null){
                throw new EntityNotFoundException(String.format("Artist with id '%d' does not exist.",id));
            }
            return artist;
        }
    }

    @Override
    public List<Artist> getAllArtists() {
        try(Session session = sessionFactory.openSession()){
            Query<Artist> artistQuery = session.createQuery("from Artist",Artist.class);

            return artistQuery.list();
        }
    }

    @Override
    public boolean checkArtistExists(String artistName) {
        try(Session session = sessionFactory.openSession()){
            Query<Artist> artistQuery = session.createQuery("from Artist where name = :name ",Artist.class);
            artistQuery.setParameter("name",artistName);
            return artistQuery.list().size() != 0;
        }
    }

    @Override
    public Artist getArtistByName(String name) {
        try(Session session = sessionFactory.openSession()){
            Query<Artist> artistQuery = session.createQuery("from Artist where name = :name",Artist.class);
            artistQuery.setParameter("name",name);

            return artistQuery.list().get(0);
        }
    }
}
