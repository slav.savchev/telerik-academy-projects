package com.example.demo.utils;

import com.example.demo.external.api.model.GeoLocation;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.IOException;


@Service
public class BingMapsHelper {

    public static GeoLocation calculateTravelDuration(String url) throws IOException {

        JSONObject travelResponse = JSONutils.readJsonFromUrl(url);

        JSONArray resourceSets = (JSONArray)travelResponse.get("resourceSets");

        JSONObject object = resourceSets.getJSONObject(0);

        JSONArray resources = (JSONArray)object.get("resources");

        JSONObject resource0 = resources.getJSONObject(0);

        Integer duration = resource0.getInt("travelDuration");

        JSONArray routeLegs = (JSONArray)resource0.get("routeLegs");

        JSONObject firstChoice = routeLegs.getJSONObject(0);

        JSONObject actualEnd = (JSONObject)firstChoice.get("actualEnd");

        JSONArray endCoordinates = (JSONArray)actualEnd.get("coordinates");

        String endLatitude = endCoordinates.get(0).toString();
        String endLongitude = endCoordinates.get(1).toString();

        JSONObject actualStart = (JSONObject)firstChoice.get("actualStart");

        JSONArray startCoordinates = (JSONArray)actualStart.get("coordinates");

        String startLatitude = startCoordinates.get(0).toString();
        String startLongitude = startCoordinates.get(1).toString();

        JSONObject endLocation = (JSONObject)firstChoice.get("endLocation");

        String endLocationName = endLocation.getString("name");

        JSONObject startLocation = (JSONObject)firstChoice.get("startLocation");

        String startLocationName = startLocation.getString("name");

        endLocationName = endLocationName.replace(",","");
        endLocationName = endLocationName.replace(" ","%");

        startLocationName = startLocationName.replace(",","");
        startLocationName = startLocationName.replace(" ","%");



        GeoLocation geoLocation = new GeoLocation();
        geoLocation.setTravelDuration(duration);
        geoLocation.setStartLocation(startLocationName);
        geoLocation.setEndLocation(endLocationName);
        geoLocation.setEndLatitude(endLatitude);
        geoLocation.setEndLongitude(endLongitude);
        geoLocation.setStartLatitude(startLatitude);
        geoLocation.setStartLongitude(startLongitude);

        return geoLocation;
    }


}
