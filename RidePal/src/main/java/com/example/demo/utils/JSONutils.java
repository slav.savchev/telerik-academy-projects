package com.example.demo.utils;

import com.example.demo.models.DTOs.DisplayPlaylistDTO;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class JSONutils {

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static String connectToUrl(String baseURL) {
        try{
            URL url = new URL(baseURL);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");


            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();

            return String.valueOf(content);

        } catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

    public static List<List<DisplayPlaylistDTO>> getPages(List<DisplayPlaylistDTO> allPlaylists) {
        int totalNumberOfPlaylists = allPlaylists.size();
        int playlistsPerPage = 4;
        int listsToBeCreated = (int)Math.ceil((double)totalNumberOfPlaylists/(double)playlistsPerPage);
        List<List<DisplayPlaylistDTO>> pageLists = new ArrayList<>();
        int counter =0;
        int stopFill = 0;

        for (int i = 0; i <listsToBeCreated ; i++) {
            List<DisplayPlaylistDTO> toBeFilled = new ArrayList<>();

            for (int j = 0; j <playlistsPerPage ; j++) {
                if(counter < totalNumberOfPlaylists){
                    toBeFilled.add(allPlaylists.get(counter));
                    counter++;
                } else {
                    stopFill = 1;
                    break;
                }
            }
            pageLists.add(toBeFilled);
            if(stopFill==1){
                break;
            }
        }

        return pageLists;
    }

}
