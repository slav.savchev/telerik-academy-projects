package com.example.demo.services.contracts;

import com.example.demo.models.Album;

import java.util.List;

public interface AlbumService {

    void createAlbum(Album album);

    List<Album> createAlbumList(List<Album> albumList);

    Album getById(int id);

    List<Album> getAllAlbums();

    boolean checkAlbumExists(String albumName);

    Album getAlbumByName(String albumName);
}
