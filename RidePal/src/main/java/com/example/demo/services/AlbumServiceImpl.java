package com.example.demo.services;

import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.Album;
import com.example.demo.repositories.contracts.AlbumRepository;
import com.example.demo.services.contracts.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AlbumServiceImpl implements AlbumService {

    private AlbumRepository repository;

    @Autowired
    public AlbumServiceImpl(AlbumRepository repository) {
        this.repository = repository;
    }

    @Override
    public void createAlbum(Album album) {
        if (!checkAlbumExists(album.getTitle())) {
            repository.createAlbum(album);
        }
    }

    @Override
    public List<Album> createAlbumList(List<Album> albumList) {
        return repository.createAlbum(albumList);
    }

    @Override
    public Album getById(int id) {
        Album album = repository.getById(id);
        if (album == null) {
            throw new EntityNotFoundException(String.format("Album with id '%d' does not exist.", id));
        }
        return album;
    }

    @Override
    public List<Album> getAllAlbums() {
        return repository.getAllAlbums();
    }

    @Override
    public boolean checkAlbumExists(String albumName) {
        return repository.checkAlbumExists(albumName);
    }

    @Override
    public Album getAlbumByName(String albumName) {
        return repository.getAlbumByName(albumName);
    }
}
