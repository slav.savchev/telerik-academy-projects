package com.example.demo.services.contracts;

import com.example.demo.models.Genre;
import com.example.demo.models.Track;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface TrackService {
    void createTrack(Track track, String genre);

    List<Track> createTrack(List<Track> trackList);

    Track getById(int id);

    List<Track> getAllTracks();

    void syncTracksForAllGenres() throws IOException;

    void syncTracksPerGenre(Genre genre) throws IOException;

    boolean checkTrackExists(String trackName, String previewUrl);

    List<Track> getTracksByGenre(String genre);

    List<Track> getTopTracksByGenre(String genre);

    long countTracksPerGenre(String genre);

    List<Track> getTracksByGenreAndTime(String genre, int timeRemaining);

    Map<String,String> getLatestNews();
}
