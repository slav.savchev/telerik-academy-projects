package com.example.demo.services;

import com.example.demo.models.Artist;
import com.example.demo.repositories.contracts.ArtistRepository;
import com.example.demo.services.contracts.ArtistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ArtistServiceImpl implements ArtistService {

    private ArtistRepository repository;


    @Autowired
    public ArtistServiceImpl(ArtistRepository repository){
        this.repository = repository;
    }


    @Override
    public void createArtist(Artist artist) {
        if(!checkArtistExists(artist.getName())){
            repository.createArtist(artist);
        }
    }

    @Override
    public List<Artist> createArtist(List<Artist> artistList) {
        return repository.createArtist(artistList);
    }

    @Override
    public Artist getById(int id) {
        return repository.getById(id);
    }

    @Override
    public List<Artist> getAllArtists() {
        return repository.getAllArtists();
    }

    @Override
    public boolean checkArtistExists(String artistName) {
        return repository.checkArtistExists(artistName);
    }

    @Override
    public Artist getArtistByName(String name) {
        return repository.getArtistByName(name);
    }
}
