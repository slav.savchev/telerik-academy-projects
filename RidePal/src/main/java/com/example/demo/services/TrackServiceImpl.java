package com.example.demo.services;

import com.example.demo.external.api.model.DeezerArtist;
import com.example.demo.models.Album;
import com.example.demo.models.Artist;
import com.example.demo.models.Genre;
import com.example.demo.models.Track;
import com.example.demo.repositories.contracts.TrackRepository;
import com.example.demo.services.contracts.AlbumService;
import com.example.demo.services.contracts.ArtistService;
import com.example.demo.services.contracts.GenreService;
import com.example.demo.services.contracts.TrackService;
import com.example.demo.utils.JSONutils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class TrackServiceImpl implements TrackService {

    public static final String NEWS_URL = "https://newsapi.org/v2/everything?" + System.getenv("newsAPI") +  "&sources=mtv-news";


    private TrackRepository trackRepository;
    private ArtistService artistService;
    private AlbumService albumService;
    private GenreService genreService;

    @Autowired
    public TrackServiceImpl(TrackRepository trackRepository, ArtistService artistService, AlbumService albumService, GenreService genreService){
        this.trackRepository = trackRepository;
        this.artistService = artistService;
        this.albumService = albumService;
        this.genreService = genreService;
    }


    @Override
    public void createTrack(Track track, String genre) {
        if(!checkTrackExists(track.getTitle(),track.getPreview())){

            if(track.getPreview().equals("")){
                return;
            }

            createTrackArtist(track);

            createTrackAlbum(track, genre);
            track.getAlbum().setArtist(track.getArtist());

            trackRepository.createTrack(track,genre);

        }
    }

     void createTrackAlbum(Track track, String genre) {
        if(!albumService.checkAlbumExists(track.getAlbum().getTitle())){
            Genre genreByName = genreService.getGenreByName(genre);
            Album album = track.getAlbum();
            album.addGenre(genreByName);
            albumService.createAlbum(track.getAlbum());
        } else {
            Album album = albumService.getAlbumByName(track.getAlbum().getTitle());
            track.setAlbum(album);
        }
    }

     void createTrackArtist(Track track) {
        if(!artistService.checkArtistExists(track.getArtist().getName())){
            artistService.createArtist(track.getArtist());

        } else {
            Artist artist = artistService.getArtistByName(track.getArtist().getName());
            track.setArtist(artist);
        }
    }

    @Override
    public List<Track> createTrack(List<Track> trackList) {
        return trackRepository.createTrack(trackList);
    }

    @Override
    public Track getById(int id) {
        return trackRepository.getById(id);
    }

    @Override
    public List<Track> getAllTracks() {
        return trackRepository.getAllTracks();
    }

    @Override
    public void syncTracksForAllGenres() throws IOException {

        List<Genre> allGenres = genreService.getActiveGenres();

        for (Genre genre: allGenres) {
            syncTracksPerGenre(genre);
        }

    }

    @Override
    public void syncTracksPerGenre(Genre genre) throws IOException {
        if(trackRepository.countTracksPerGenre(genre.getName()) == 0){
            JSONObject jsonObject = JSONutils.readJsonFromUrl(String.format("https://api.deezer.com/genre/%s/artists", genre.getDeezerGenreId()));

            JSONArray jsonArray = (JSONArray) jsonObject.get("data");

            ObjectMapper mapper = new ObjectMapper();
            List<String> trackListsFromArtists = getTrackListsFromArtists(jsonArray, mapper);

            createAllTracksPerArtist(genre, mapper, trackListsFromArtists);

        }
    }


    @Override
    public boolean checkTrackExists(String trackName, String previewUrl) {
        return trackRepository.checkTrackExists(trackName,previewUrl);
    }
    @Override
    public List<Track> getTracksByGenre(String genre) {
        return trackRepository.getTracksByGenre(genre);
    }

    @Override
    public List<Track> getTopTracksByGenre(String genre) {
        return trackRepository.getTopTracksByGenre(genre);
    }

    @Override
    public long countTracksPerGenre(String genre){
        return trackRepository.countTracksPerGenre(genre);
    }

    private List<String> getTrackListsFromArtists(JSONArray jsonArray, ObjectMapper mapper) throws JsonProcessingException {

        List<DeezerArtist> artists = mapper.readValue(jsonArray.toString(), new TypeReference<List<DeezerArtist>>() {});

        List<String> trackLists = artists.stream()
                .map(DeezerArtist::getTracklist)
                .collect(Collectors.toList());

        int limitBy = 1000 / trackLists.size() + 1;

        return  trackLists.stream()
                .map(el -> el.substring(0, el.length() - 2).concat(String.valueOf(limitBy)))
                .collect(Collectors.toList());
    }

    private void createAllTracksPerArtist(Genre genre, ObjectMapper mapper, List<String> trackListsFromArtists) throws IOException {
        for (String trackList : trackListsFromArtists) {
            List<Track> tracks = getTracksFromTracklist(trackList, mapper);
            for (Track track : tracks) {
                createTrack(track, genre.getName());
                try{
                    Thread.sleep(50);
                } catch (Exception e){
                    e.printStackTrace();
                }

            }
        }
    }

    private List<Track> getTracksFromTracklist(String url, ObjectMapper mapper)
            throws IOException {

        JSONObject jsonObject = JSONutils.readJsonFromUrl(url);
        JSONArray jsonArray = (JSONArray) jsonObject.get("data");
        return mapper.readValue(jsonArray.toString(), new TypeReference<List<Track>>() {
        });

    }

    @Override
    public List<Track> getTracksByGenreAndTime(String genre, int timeRemaining) {
        return trackRepository.getTracksByGenreAndTime(genre,timeRemaining);
    }

    @Override
    public Map<String,String> getLatestNews() {

      Map<String,String> map = new HashMap<>();

        try {

            JSONObject jsonObject = JSONutils.readJsonFromUrl(NEWS_URL);
            JSONArray newsArr = (JSONArray)jsonObject.get("articles");

            for (int i = 0; i <newsArr.length() ; i++) {
                JSONObject newsHeadline = newsArr.getJSONObject(i);
                String newsPiece = newsHeadline.getString("title");
                map.put(String.valueOf(i),newsPiece);
            }

            JSONObject toBeConverted = new JSONObject(map);

            String dir =  System.getProperty("user.dir");

            try(FileWriter file = new FileWriter(new File(dir + "\\src\\main\\resources\\static","newsJSON.json"))){
                file.write(toBeConverted.toString());
                file.flush();
            } catch (IOException e){
                e.getMessage();
            }

            return map;

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        return map;
    }


}
