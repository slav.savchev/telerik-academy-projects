package com.example.demo.services;

import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.DTOs.GenreDTO;
import com.example.demo.models.DtoMapper;
import com.example.demo.models.Genre;
import com.example.demo.repositories.contracts.GenreRepository;
import com.example.demo.services.contracts.GenreService;
import com.example.demo.utils.JSONutils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GenreServiceImpl implements GenreService {

    public static final String HTTPS_API_DEEZER_COM_GENRE = "https://api.deezer.com/genre";
    public static final String GENRE_WITH_ID_D_DOES_NOT_EXIST = "Genre with id '%d' does not exist.";
    public static final String GENRE_WITH_SUCH_NAME_DOES_NOT_EXIST = "Genre with such name does not exist.";
    private GenreRepository genreRepository;

    @Autowired
    public GenreServiceImpl(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }


    @Override
    public Genre createGenre(Genre genre) {
        return genreRepository.createGenre(genre);
    }

    @Override
    public List<Genre> createGenres(List<Genre> genreList) {
        return genreRepository.createGenres(genreList);
    }

    @Override
    public Genre getById(int id) {

        Genre genre = genreRepository.getById(id);
        if (genre == null) {
            throw new EntityNotFoundException(String.format(GENRE_WITH_ID_D_DOES_NOT_EXIST, id));
        }
        return genre;
    }

    @Override
    public List<Genre> getActiveGenres() {
        return genreRepository.getActiveGenres();
    }

    @Override
    public boolean checkGenreExists(String name) {
        return genreRepository.checkGenreExists(name);
    }

    @Override
    public List<GenreDTO> syncGenres() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        JSONObject jsonObject = JSONutils.readJsonFromUrl(HTTPS_API_DEEZER_COM_GENRE);
        JSONArray jsonArray = (JSONArray) jsonObject.get("data");
        List<Genre> deezerGenres = mapper.readValue(jsonArray.toString(), new TypeReference<List<Genre>>() {
        });

        List<Genre> allGenres = genreRepository.getAllGenres();
        List<String> storedGenreNames = allGenres.stream()
                .map(Genre::getName)
                .collect(Collectors.toList());

        List<Genre> newDeezerGenres = deezerGenres.stream()
                .filter(genre -> !storedGenreNames.contains(genre.getName()))
                .collect(Collectors.toList());

        for (Genre genre : newDeezerGenres) {
            createGenre(genre);
        }

        return newDeezerGenres.stream()
                .map(DtoMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<GenreDTO> getAllGenres() {
        return genreRepository.getAllGenres().stream().
                map(DtoMapper::toDto).
                collect(Collectors.toList());
    }

    @Override
    public Genre getGenreByName(String genreName) {
        Genre genre = genreRepository.getGenreByName(genreName);

        if (genre == null) {
            throw new EntityNotFoundException(GENRE_WITH_SUCH_NAME_DOES_NOT_EXIST);
        }
        return genre;
    }

    @Override
    public Genre changeGenreStatus(int genreId, boolean activityStatus) {

        Genre genre = genreRepository.getById(genreId);

        genre.setActive(activityStatus);
        Genre genreFromDb = genreRepository.updateGenre(genre);
        return genreFromDb;
    }

}
