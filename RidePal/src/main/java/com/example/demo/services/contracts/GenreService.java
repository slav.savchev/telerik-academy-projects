package com.example.demo.services.contracts;

import com.example.demo.models.DTOs.GenreDTO;
import com.example.demo.models.Genre;

import java.io.IOException;
import java.util.List;

public interface GenreService {

    Genre createGenre(Genre genre);

    List<Genre> createGenres(List<Genre> genreList);

    Genre getById(int id);

    List<Genre> getActiveGenres();

    boolean checkGenreExists(String name);

    List<GenreDTO> syncGenres() throws IOException;

    List<GenreDTO> getAllGenres();

    Genre getGenreByName(String genreName);

    Genre changeGenreStatus(int genreId, boolean activityStatus);
}
