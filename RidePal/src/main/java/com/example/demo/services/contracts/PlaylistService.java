package com.example.demo.services.contracts;

import com.example.demo.models.Artist;
import com.example.demo.models.DTOs.DisplayPlaylistDTO;
import com.example.demo.models.DTOs.PlaylistDTO;
import com.example.demo.models.Playlist;
import com.example.demo.models.Track;
import io.swagger.models.auth.In;

import java.util.List;
import java.util.Map;

public interface PlaylistService {

    Playlist createPlaylist(PlaylistDTO playlistDTO);

    List<Playlist> createPlaylist(List<Playlist> playlistList);

    Playlist getById(int id);

    List<Playlist> getAllPlaylists();

    List<Playlist> getAllPlaylists(Map<Integer,String> flagMapMVC);

    Playlist generatePlayList(PlaylistDTO playlistDTO, Integer duration);

    boolean checkArtistExistsInPlaylist(Artist artist, Playlist playlist);

    List<Playlist> getPlaylistsByUserID(String name);

    int[] getMinAndMaxDuration();

    List<Playlist> getAllPlaylistsAdmin();

    Playlist changePlaylistStatus(int playlistID, boolean b);

    DisplayPlaylistDTO editPlaylist(DisplayPlaylistDTO playlist);

}
