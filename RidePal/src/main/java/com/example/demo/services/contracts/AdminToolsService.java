package com.example.demo.services.contracts;

import java.io.IOException;

public interface AdminToolsService {

    void createGenres() throws IOException;
}
