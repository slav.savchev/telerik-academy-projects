package com.example.demo.services;

import com.example.demo.models.*;
import com.example.demo.models.DTOs.DisplayPlaylistDTO;
import com.example.demo.models.DTOs.PlaylistDTO;
import com.example.demo.repositories.contracts.PlaylistRepository;
import com.example.demo.services.contracts.GenreService;
import com.example.demo.services.contracts.PlaylistService;
import com.example.demo.services.contracts.TrackService;
import com.example.demo.utils.JSONutils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PlaylistServiceImpl implements PlaylistService {

    public static final String DEEZER_ACCESS_TOKEN = System.getenv("deezerAPI");
    public static final String BASE_DEEZER_URL = "https://api.deezer.com/user/3401491264/playlists";
    private PlaylistRepository repository;
    private GenreService genreService;
    private TrackService trackService;

    @Autowired
    public PlaylistServiceImpl(PlaylistRepository repository, GenreService genreService, TrackService trackService) {
        this.repository = repository;
        this.genreService = genreService;
        this.trackService = trackService;
    }


    @Override
    public Playlist createPlaylist(PlaylistDTO playlistDTO) {

        Playlist playlist = generatePlayList(playlistDTO, playlistDTO.getDuration());

        createPlaylistInDeezer(playlist);

        return repository.createPlaylist(playlist);
    }

    private void createPlaylistInDeezer(Playlist playlist) {

        String baseURL = BASE_DEEZER_URL;
        String deezerPlaylistName = "?title=" + playlist.getUserID() + "_" + playlist.getTitle();
        String accessToken = DEEZER_ACCESS_TOKEN;

        String contentSub = JSONutils.connectToUrl(baseURL + deezerPlaylistName + accessToken);
        String contentStr = contentSub.substring(6, contentSub.length() - 1);

        long deezerID = Long.parseLong(contentStr);
        playlist.setDeezerID(deezerID);

        putTracksIntoDeezerPlaylist(playlist);
    }


    private void putTracksIntoDeezerPlaylist(Playlist playlist) {

        String accessToken = DEEZER_ACCESS_TOKEN;

        StringBuffer stringBuffer = new StringBuffer();
        Set<Track> trackSet = playlist.getTracks();

        for (Track elem : trackSet) {
            stringBuffer.append(elem.getDeezerID()).append(",");
        }


        String tracks = String.valueOf(stringBuffer);
        String trackString = tracks.substring(0, tracks.length() - 1);

        String baseURL = "https://api.deezer.com/playlist/" + playlist.getDeezerID() + "/tracks?songs=" + trackString + accessToken;

        JSONutils.connectToUrl(baseURL);
    }


    @Override
    public Playlist generatePlayList(PlaylistDTO playlistDTO, Integer duration) {

        Map<String, Integer> sortedMap = getDescOrderKeys(playlistDTO.getGenrePref());

        boolean topTracks = playlistDTO.isTopTracks();
        boolean sameArtist = playlistDTO.isSameArtist();

        Playlist playlist = new Playlist();
        playlist.setTitle(playlistDTO.getTitle());
        playlist.setUserID(playlistDTO.getUserID());

        Set<Genre> genreSet = sortedMap.keySet().stream()
                .map(genre -> genreService.getGenreByName(genre))
                .collect(Collectors.toSet());


        playlist.setGenres(genreSet);

        Random rand = new Random();
        int timeRemaining = duration;


        for (Map.Entry<String, Integer> currentGenre : sortedMap.entrySet()) {

            int currentGenreTimeLimit = (duration * playlistDTO.getGenrePref().get(currentGenre.getKey())) / 100;
            int durationPerGenre = 0;

            if (topTracks) {

                List<Track> trackList = trackService.getTopTracksByGenre(currentGenre.getKey());

                int counter = 0;

                while (durationPerGenre <= currentGenreTimeLimit) {

                    Track topTrack = trackList.get(counter);
                    counter++;

                    if (!sameArtist) {
                        if (checkArtistExistsInPlaylist(topTrack.getArtist(), playlist)) {
                            continue;
                        }
                    }
                    playlist.setPlaytime(playlist.getPlaytime() + topTrack.getDuration());
                    playlist.getTracks().add(topTrack);


                    if (timeRemaining < -300) {
                        playlist.getTracks().remove(topTrack);
                        break;
                    }

                    timeRemaining -= topTrack.getDuration();
                    durationPerGenre += topTrack.getDuration();

                }

            } else {

                while (durationPerGenre <= currentGenreTimeLimit) {

                    Track randomTrack = pickTrack(currentGenre.getKey(), timeRemaining, rand);

                    if (randomTrack == null) {
                        break;
                    }
                    if (checkTrackExistsInPlaylist(randomTrack, playlist)) {
                        continue;
                    }
                    if (!sameArtist) {
                        if (checkArtistExistsInPlaylist(randomTrack.getArtist(), playlist)) {
                            continue;
                        }
                    }
                    playlist.setPlaytime(playlist.getPlaytime() + randomTrack.getDuration());
                    playlist.getTracks().add(randomTrack);
                    timeRemaining -= randomTrack.getDuration();
                    durationPerGenre += randomTrack.getDuration();

                }
            }

        }

        Object[] tracksArr = playlist.getTracks().toArray();
        int trackRankSum = 0;
        for (int i = 0; i < tracksArr.length; i++) {
            trackRankSum += ((Track) tracksArr[i]).getRank();
        }

        int playlistRank = trackRankSum / tracksArr.length;
        playlist.setRank(playlistRank);

        return playlist;
    }

    private Track pickTrack(String genre, int timeRemaining, Random random) {

        List<Track> trackList = trackService.getTracksByGenreAndTime(genre, timeRemaining);
        if (trackList.size() == 0) {
            return null;
        }

        return trackList.get(random.nextInt(trackList.size()));
    }

    private boolean checkTrackExistsInPlaylist(Track track, Playlist playlist) {
        return playlist.getTracks().contains(track);
    }

    @Override
    public boolean checkArtistExistsInPlaylist(Artist artist, Playlist playlist) {
        return playlist.getTracks().stream()
                .anyMatch(track -> track.getArtist().getName().equals(artist.getName()));
    }

    @Override
    public List<Playlist> getPlaylistsByUserID(String name) {
        return repository.getPlaylistsByUserID(name);
    }


    @Override
    public int[] getMinAndMaxDuration() {
        return repository.getMinAndMaxDuration();
    }

    @Override
    public List<Playlist> getAllPlaylistsAdmin() {
        return repository.getAllPlaylistsAdmin();
    }

    @Override
    public Playlist changePlaylistStatus(int playlistID, boolean b) {
        Playlist playlist = repository.getById(playlistID);

        playlist.setActive(b);
        Playlist playlistFromDB = repository.updatePlaylist(playlist);

        return playlistFromDB;

    }

    @Override
    public DisplayPlaylistDTO editPlaylist(DisplayPlaylistDTO playlist) {
        Playlist playlistFromDB = repository.getById(playlist.getId());
        playlistFromDB.setTitle(playlist.getTitle());
        playlistFromDB.setRank(playlist.getRank());
        Set<Genre> genreSet = new HashSet<>();
        String genres = playlist.getGenres();
        genres = genres.replace("undefined", "");
        if (genres.length() != 0) {
            String[] genreArr = genres.substring(0, genres.length() - 2).split(", ");
            for (int i = 0; i < genreArr.length; i++) {
                Genre genre = genreService.getGenreByName(genreArr[i]);
                genreSet.add(genre);
            }
        }
        playlistFromDB.setGenres(genreSet);

        return DtoMapper.toDisplayPlaylistDTO(repository.updatePlaylist(playlistFromDB));
    }

    @Override
    public List<Playlist> createPlaylist(List<Playlist> playlistList) {
        return repository.createPlaylist(playlistList);
    }

    @Override
    public Playlist getById(int id) {
        return repository.getById(id);
    }

    @Override
    public List<Playlist> getAllPlaylists() {
        return repository.getAllPlaylists();
    }

    @Override
    public List<Playlist> getAllPlaylists(Map<Integer, String> flagMapMVC) {
        return repository.getAllPlaylists(flagMapMVC);
    }

    private Map<String, Integer> getDescOrderKeys(Map<String, Integer> genreMap) {

        Map<String, Integer> sortedMap = genreMap.entrySet().stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                        LinkedHashMap::new));

        return sortedMap;
    }

}
