
var slider1 = document.getElementById("myRange_" + 0);
var slider2 = document.getElementById("myRange_" + 1);
var slider3 = document.getElementById("myRange_" + 2);
var slider4 = document.getElementById("myRange_" + 3);

var output1 = document.getElementById("sliderValue_" + 0);
var output2 = document.getElementById("sliderValue_" + 1);
var output3 = document.getElementById("sliderValue_" + 2);
var output4 = document.getElementById("sliderValue_" + 3);


output1.innerHTML = slider1.value;
slider1.oninput = function () {
    output1.innerHTML = this.value;
};

output2.innerHTML = slider2.value;
slider2.oninput = function () {
    output2.innerHTML = this.value;
};

output3.innerHTML = slider3.value;
slider3.oninput = function () {
    output3.innerHTML = this.value;
};

output4.innerHTML = slider4.value;
slider4.oninput = function () {
    output4.innerHTML = this.value;
};

