$(function () {

    $.ajax({
        type: 'GET',
        url: '/api/v1/syncNews',
        success: function () {
            $('#news').eocjsNewsticker({
                type: 'ajax',
                source: 'newsJSON.json',
                divider: ' * * * ',
                speed:50
            });
        }
    });

});

