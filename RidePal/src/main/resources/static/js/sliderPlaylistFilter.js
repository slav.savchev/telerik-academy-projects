var slider = document.getElementById("myRangeFilter");
var output = document.getElementById("myRangeFilterValue");
output.innerHTML = slider.value;

slider.oninput = function () {
    output.innerHTML = this.value;
};

