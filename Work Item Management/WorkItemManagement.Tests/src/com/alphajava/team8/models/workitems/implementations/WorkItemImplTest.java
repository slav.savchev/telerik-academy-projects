package com.alphajava.team8.models.workitems.implementations;

import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.implementations.PersonImpl;
import com.alphajava.team8.models.workitems.contracts.Bug;
import com.alphajava.team8.models.workitems.contracts.Feedback;
import com.alphajava.team8.models.workitems.contracts.Story;
import com.alphajava.team8.models.workitems.enums.PriorityType;
import com.alphajava.team8.models.workitems.enums.SeverityType;
import com.alphajava.team8.models.workitems.enums.SizeType;
import com.alphajava.team8.models.workitems.enums.StatusType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class WorkItemImplTest {

    private Person random;
    private Database database;

    @Before
    public void before() {
        database = new Database();
        random = new PersonImpl("Random");
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_titleIsNull(){
        //Act
        Feedback feedback = new FeedbackImpl(null, StatusType.SCHEDULED, 1, "Default description");
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_titleLengthLessThanMinimum(){
        //Act
        Feedback feedback = new FeedbackImpl("item", StatusType.SCHEDULED, 1, "Default description");
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_titleLengthMoreThanMaximum(){
        //Act
        Feedback feedback = new FeedbackImpl(new String(new char[101]).replace('\0','a'), StatusType.SCHEDULED, 1, "Default description");
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_descriptionIsNull(){
        //Act
        Feedback feedback = new FeedbackImpl("LongTitleForWorkItem", StatusType.SCHEDULED, 1, null);
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_descriptionLengthLessThanMinimum(){
        //Act
        Feedback feedback = new FeedbackImpl("LongTitleForWorkItem", StatusType.SCHEDULED, 1, "describe");
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_descriptionLengthMoreThanMaximum(){
        //Act
        Feedback feedback = new FeedbackImpl("LongTitleForWorkItem", StatusType.SCHEDULED, 1, new String(new char [501]).replace('\0','a'));
    }

    @Test
    public void history_should_containActivity_when_activityAdded() {

        //Arrange
        Feedback feedback = new FeedbackImpl("LongTitleForWorkItem", StatusType.SCHEDULED, 1, "Default description");

        //Act
        feedback.addActivityToHistory("Testing activity");

        //Assert
        Assert.assertTrue(feedback.getHistory().contains("Testing activity"));
    }

   @Test
    public void comments_should_containComment_when_commentAdded() {
        //Arrange
        Feedback feedback = new FeedbackImpl("LongTitleForWorkItem", StatusType.SCHEDULED, 1, "Default description");

        //Act
        feedback.addComment("Testing comments", random);

        //Assert
        Assert.assertTrue(feedback.getComments().contains("Testing comments - author: Random"));

    }

    @Test (expected = IllegalArgumentException.class)
    public void bugConstructor_should_throwError_when_wrongStatusType() {
        //Arrange
        Bug bug;

        //Act
        for (StatusType status : StatusType.values()) {
            if (!status.equals(StatusType.ACTIVE) && !status.equals(StatusType.FIXED)) {
                bug = new BugImpl("LongBugName", status, "Default description", PriorityType.MEDIUM, SeverityType.MAJOR, random);
            }
        }
    }

    @Test (expected = IllegalArgumentException.class)
    public void storyConstructor_should_throwError_when_wrongStatusType() {
        //Arrange
        Story story;

        //Act
        for (StatusType status : StatusType.values()) {
            if (!status.equals(StatusType.NOTDONE) && !status.equals(StatusType.DONE) && !status.equals(StatusType.INPROGRESS)) {
                story = new StoryImpl("LongStoryName", status, PriorityType.MEDIUM, SizeType.MEDIUM, random, "Default description");
            }
        }
    }

    @Test (expected = IllegalArgumentException.class)
    public void feedbackConstructor_should_throwError_when_wrongStatusType() {
        //Arrange
        Feedback feedback;

        //Act
        for (StatusType status : StatusType.values()) {
            if (!status.equals(StatusType.NEW) && !status.equals(StatusType.DONE) && !status.equals(StatusType.SCHEDULED) && !status.equals(StatusType.UNSCHEDULED)) {
                feedback = new FeedbackImpl("LongFeedbackName", status, 3, "Default description");
            }
        }
    }

}