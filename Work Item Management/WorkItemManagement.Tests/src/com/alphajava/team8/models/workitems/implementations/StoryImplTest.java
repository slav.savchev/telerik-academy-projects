package com.alphajava.team8.models.workitems.implementations;

import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.implementations.PersonImpl;
import com.alphajava.team8.models.workitems.contracts.Bug;
import com.alphajava.team8.models.workitems.contracts.Story;
import com.alphajava.team8.models.workitems.enums.PriorityType;
import com.alphajava.team8.models.workitems.enums.SizeType;
import com.alphajava.team8.models.workitems.enums.StatusType;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class StoryImplTest {

    private Person random;
    private Database database;

    @Before
    public void before() {
        database = new Database();
        random = new PersonImpl("Random");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_assigneeIsNull(){
        //Act
        Story story = new StoryImpl("LongStoryName", StatusType.INPROGRESS, PriorityType.MEDIUM, SizeType.MEDIUM, null, "Default description");
    }

    @Test
    public void toString_should_returnProperString() {
        //Arrange
        Story story = new StoryImpl("LongStoryName", StatusType.INPROGRESS, PriorityType.MEDIUM, SizeType.MEDIUM, random, "Default description");
        String correctOutput = String.format(
                "WorkItem ID: %d -----\n  WorkItem type: Story, Title: LongStoryName\n  Status: InProgress, Priority: Medium, Size: Medium, Assignee: Random\n  Description:\n  Default description", story.getId());

        //Act
        String output = story.toString();

        //Assert
        assertEquals(output, correctOutput);
    }

}