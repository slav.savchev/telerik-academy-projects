package com.alphajava.team8.models.workitems.implementations;

import com.alphajava.team8.models.workitems.contracts.Feedback;
import com.alphajava.team8.models.workitems.enums.StatusType;
import org.junit.Test;

import static org.junit.Assert.*;

public class FeedbackImplTest {
    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_ratingLessThanMinimum(){
        //Act
        Feedback feedback = new FeedbackImpl("LongFeedbackName", StatusType.SCHEDULED, 0, "Default description");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_ratingMoreThanMaximum(){
        //Act
        Feedback feedback = new FeedbackImpl("LongFeedbackName", StatusType.SCHEDULED, 6, "Default description");
    }

    @Test
    public void toString_should_returnProperString() {
        //Arrange
        Feedback feedback = new FeedbackImpl("LongFeedbackName", StatusType.SCHEDULED, 3, "Default description");

        String correctOutput = String.format(
                "WorkItem ID: %d -----\n  WorkItem type: Feedback, Title: LongFeedbackName\n  Status: Scheduled, Rating: 3\n  Description:\n  Default description", feedback.getId());


        //Act
        String output = feedback.toString();

        //Assert
        assertEquals(output, correctOutput);
    }
}