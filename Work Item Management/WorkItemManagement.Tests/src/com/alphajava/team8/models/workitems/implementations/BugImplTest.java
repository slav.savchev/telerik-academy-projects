package com.alphajava.team8.models.workitems.implementations;

import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.implementations.PersonImpl;
import com.alphajava.team8.models.workitems.contracts.Bug;
import com.alphajava.team8.models.workitems.enums.PriorityType;
import com.alphajava.team8.models.workitems.enums.SeverityType;
import com.alphajava.team8.models.workitems.enums.StatusType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BugImplTest {

    private Person random;
    private Database database;

    @Before
    public void before() {
        database = new Database();
        random = new PersonImpl("Random");
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_assigneeIsNull(){
        //Act
        Bug bug = new BugImpl("LongBugName", StatusType.ACTIVE, "Default description", PriorityType.MEDIUM, SeverityType.MAJOR, null);
    }

    @Test
    public void stepsToReproduce_should_containStep_when_stepAdded(){
        //Arrange
        Bug bug = new BugImpl("LongBugName", StatusType.ACTIVE, "Default description", PriorityType.MEDIUM, SeverityType.MAJOR, random);

        //Act
        bug.addStepToReproduce("Adding step.");

        //Assert
        Assert.assertTrue(bug.getStepsToReproduce().contains("Adding step."));
    }

    @Test
    public void toString_should_returnProperString() {
        //Arrange
        Bug bug = new BugImpl("LongBugName", StatusType.ACTIVE, "Default description", PriorityType.MEDIUM, SeverityType.MAJOR, random);
        String correctOutput = String.format(
                "WorkItem ID: %d -----\n  WorkItem type: Bug, Title: LongBugName\n  Status: Active, Priority: Medium, Severity: Major, Assignee: Random\n  Description:\n  Default description", bug.getId());

        //Act
        String output = bug.toString();

        //Assert
        assertEquals(output, correctOutput);
    }

}