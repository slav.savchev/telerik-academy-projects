package com.alphajava.team8.models.teams.implementations;

import com.alphajava.team8.commands.assign.AssignWorkItemCommand;
import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.factories.WorkItemManagementFactoryImpl;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Board;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.workitems.contracts.WorkItem;
import com.alphajava.team8.models.workitems.enums.PriorityType;
import com.alphajava.team8.models.workitems.enums.SeverityType;
import com.alphajava.team8.models.workitems.enums.StatusType;
import com.alphajava.team8.models.workitems.implementations.BugImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BoardImplTest {
    private Board board;
    private WorkItemManagementFactory factory;
    private Database database;

    @Before
    public void before(){
        this.factory = new WorkItemManagementFactoryImpl();
        this.database = new Database();
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_boardLengthLessThanMinimum(){

        //Act
         board = new BoardImpl("boar");
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_boardLengthMoreThanMaximum(){

        //Act
         board = new BoardImpl("BoardMaxLength");
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_boardLengthIsNull(){

        //Act
         board = new BoardImpl(null);
    }

    @Test
    public void history_should_contain_activity_when_activityIsAdded(){

        //Arrange
        board = new BoardImpl("Board");

        //Act
        board.addActivityToHistory("TestActivity");

        //Assert
        Assert.assertTrue(board.getActivityHistory().contains("TestActivity"));
    }

    @Test
    public void workItemList_should_contain_when_itemIsAdded(){

        //Arrange
        board = new BoardImpl("Board");
        Person person = new PersonImpl("Svetoslav");
        WorkItem item = new BugImpl("NewBugForPersonSvetoslav", StatusType.ACTIVE,"NewDescriptionForBug", PriorityType.HIGH, SeverityType.CRITICAL,person);

        //Act
        board.addWorkItem(item);

        //Assert
        Assert.assertTrue(board.getWorkItems().contains(item));
    }



}