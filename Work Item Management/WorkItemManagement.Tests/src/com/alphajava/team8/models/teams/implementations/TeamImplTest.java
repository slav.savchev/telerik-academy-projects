package com.alphajava.team8.models.teams.implementations;

import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.factories.WorkItemManagementFactoryImpl;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Team;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TeamImplTest {
    private Team team;
    private WorkItemManagementFactory factory;
    private Database database;

    @Before
    public void before(){
        factory = new WorkItemManagementFactoryImpl();
        database = new Database();
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_teamNameNotUnique(){

        //Act
        team = new TeamImpl("team8");
        database.addTeam(team);
        team = new TeamImpl("team8");
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_teamNameIsNull(){

        //Act
        team = new TeamImpl(null);
    }


}