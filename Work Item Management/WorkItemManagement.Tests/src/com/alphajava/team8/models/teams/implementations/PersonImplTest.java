package com.alphajava.team8.models.teams.implementations;

import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.factories.WorkItemManagementFactoryImpl;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.workitems.contracts.WorkItem;
import com.alphajava.team8.models.workitems.enums.PriorityType;
import com.alphajava.team8.models.workitems.enums.SeverityType;
import com.alphajava.team8.models.workitems.enums.StatusType;
import com.alphajava.team8.models.workitems.implementations.BugImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PersonImplTest {
    private Person person;
    private WorkItemManagementFactory factory;
    private Database database;

    @Before
    public void before(){
        this.factory = new WorkItemManagementFactoryImpl();
        this.database = new Database();
    }


    @Test (expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_personLengthIsLessThanMinimum(){

        //Act
        person = new PersonImpl("Slav");
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_personLengthIsMoreThanMaximum(){

        //Act
        person = new PersonImpl("SvetoslavSvetoslavovSavchev");
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructor_should_throw_when_personNameIsNull(){

        //Act
        person = new PersonImpl(null);
    }


    @Test
    public void history_should_contain_activity_when_activityIsAdded(){

        //Arrange
        person = new PersonImpl("Svetoslav");

        //Act
        person.addToActivityHistory("TestToBeAddedToActivityHistory");

        //Assert
        Assert.assertTrue(person.getActivityHistory().contains("TestToBeAddedToActivityHistory"));
    }

    @Test
    public void workItemList_should_contain_item_when_itemIsAssigned(){

        //Arrange
        person = new PersonImpl("Svetoslav");
        WorkItem item = new BugImpl("NewBugForPersonSvetoslav", StatusType.ACTIVE,"NewDescriptionForBug", PriorityType.HIGH, SeverityType.CRITICAL,person);

        //Act
        person.assignItem(item);

        //Assert
        Assert.assertTrue(person.getWorkItems().contains(item));
    }

    @Test
    public void workItemList_should_not_contain_item_when_itemIsUnassigned(){

        //Arrange
        person = new PersonImpl("Svetoslav");
        WorkItem item = new BugImpl("NewBugForPersonSvetoslav", StatusType.ACTIVE,"NewDescriptionForBug", PriorityType.HIGH, SeverityType.CRITICAL,person);

        //Act
        person.assignItem(item);
        person.unAssignItem(item);

        //Assert
        Assert.assertFalse(person.getWorkItems().contains(item));
    }
}