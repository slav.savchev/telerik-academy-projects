package com.alphajava.team8.commands.creation;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.factories.WorkItemManagementFactoryImpl;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.implementations.PersonImpl;
import com.alphajava.team8.models.workitems.contracts.Bug;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CreateBugCommandTest {
    private Command testCommand;
    private WorkItemManagementFactory factory;
    private Database database;
    private Person assignee;
    private Bug bug;

    @Before
    public void before() {
        factory = new WorkItemManagementFactoryImpl();
        database = new Database();
        testCommand = new CreateBugCommand(factory, database);
        assignee = new PersonImpl("DefaultPerson");
        database.addPerson(assignee);
    }

    @Test (expected = IllegalArgumentException.class)
    public void command_should_throw_when_lessParameters() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("LongBugName");
        testList.add("Active");
        testList.add("Critical");
        testList.add("DefaultPerson");

        //Act
        testCommand.execute(testList);

    }

    @Test (expected = IllegalArgumentException.class)
    public void command_should_throw_when_moreParameters() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("LongBugName");
        testList.add("Active");
        testList.add("Critical");
        testList.add("High");
        testList.add("Extra parameter.");
        testList.add("DefaultPerson");

        //Act
        testCommand.execute(testList);

    }


}