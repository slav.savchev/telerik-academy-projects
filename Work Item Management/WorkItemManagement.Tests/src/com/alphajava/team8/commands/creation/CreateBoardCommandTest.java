package com.alphajava.team8.commands.creation;

import com.alphajava.team8.commands.add.AddCommentCommand;
import com.alphajava.team8.commands.add.AddPersonCommand;
import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.factories.WorkItemManagementFactoryImpl;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.contracts.Team;
import com.alphajava.team8.models.teams.implementations.PersonImpl;
import com.alphajava.team8.models.teams.implementations.TeamImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CreateBoardCommandTest {
    private Database database;
    private WorkItemManagementFactory factory;
    private List<String> testList;

    @Before
    public void before(){
        this.database = new Database();
        this.factory = new WorkItemManagementFactoryImpl();
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throw_when_argumentCountIsInvalid(){

        //Arrange
        testList = new ArrayList<>();
        testList.add("firstArg");
        testList.add("secondArg");
        testList.add("thirdArg");

        CreateBoardCommand testObj = new CreateBoardCommand(factory);

        //Act
        testObj.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throw_when_parametersAreIncorrect(){

        //Arrange
        Person person = new PersonImpl("Svetoslav");
        database.addPerson(person);

        Team team = new TeamImpl("Team8");
        database.addTeam(team);

        testList = new ArrayList<>();
        testList.add(null);
        testList.add(team.getTeamName());

        CreateBoardCommand testObj = new CreateBoardCommand(factory);

        //Act
        testObj.execute(testList);
    }

    @Test
    public void board_should_existInTeam_when_created(){

        //Arrange
        Person person = new PersonImpl("Svetoslav");
        database.addPerson(person);

        Team team = new TeamImpl("Team8");
        database.addTeam(team);

        testList = new ArrayList<>();
        testList.add("NewBoard");
        testList.add(team.getTeamName());

        CreateBoardCommand testObj = new CreateBoardCommand(factory);

        //Act
        testObj.execute(testList);

        //Assert
        Assert.assertTrue(team.getTeamBoards()
                .stream()
                .map(board -> board.getBoardName())
                .anyMatch(boardName -> boardName.equals("NewBoard")));
    }

    @Test
    public void history_should_record_activity_when_boardIsCreated(){
        //Arrange
        Person person = new PersonImpl("Svetoslav");
        database.addPerson(person);

        Team team = new TeamImpl("Team8");
        database.addTeam(team);

        testList = new ArrayList<>();
        testList.add("NewBoard");
        testList.add(team.getTeamName());

        CreateBoardCommand testObj = new CreateBoardCommand(factory);

        //Act
        testObj.execute(testList);

        //Assert
        Assert.assertTrue(Database.getBoard("Team8","NewBoard").getActivityHistory().size() == 1);
    }
}