package com.alphajava.team8.commands.creation;

import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.factories.WorkItemManagementFactoryImpl;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.contracts.Team;
import com.alphajava.team8.models.teams.implementations.PersonImpl;
import com.alphajava.team8.models.teams.implementations.TeamImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CreateTeamCommandTest {
    private Database database;
    private WorkItemManagementFactory factory;
    private List<String> testList;

    @Before
    public void before(){
        this.database = new Database();
        this.factory = new WorkItemManagementFactoryImpl();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throw_when_argumentCountIsInvalid(){

        //Arrange
        testList = new ArrayList<>();
        testList.add("firstArg");
        testList.add("secondArg");

        CreateTeamCommand testObj = new CreateTeamCommand(factory,database);

        //Act
        testObj.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throw_when_parametersAreIncorrect(){

        //Arrange

        testList = new ArrayList<>();
        testList.add(null);

        CreateTeamCommand testObj = new CreateTeamCommand(factory,database);

        //Act
        testObj.execute(testList);
    }

    @Test
    public void team_should_exist_when_created(){

        //Arrange

        testList = new ArrayList<>();
        testList.add("Team08");

        CreateTeamCommand testObj = new CreateTeamCommand(factory,database);

        //Act
        testObj.execute(testList);

        //Assert
        Assert.assertTrue(Database.teamExists("Team08"));
    }


    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throw_when_teamNameTaken(){

        //Arrange

        Team team = new TeamImpl("TestTeam");
        database.addTeam(team);

        testList = new ArrayList<>();
        testList.add(team.getTeamName());

        CreateTeamCommand testObj = new CreateTeamCommand(factory,database);

        //Act
        testObj.execute(testList);
    }

}