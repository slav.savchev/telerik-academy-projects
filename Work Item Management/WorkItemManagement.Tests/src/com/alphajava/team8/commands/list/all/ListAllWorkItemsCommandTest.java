package com.alphajava.team8.commands.list.all;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.implementations.PersonImpl;
import com.alphajava.team8.models.workitems.contracts.Feedback;
import com.alphajava.team8.models.workitems.contracts.Story;
import com.alphajava.team8.models.workitems.enums.PriorityType;
import com.alphajava.team8.models.workitems.enums.SizeType;
import com.alphajava.team8.models.workitems.enums.StatusType;
import com.alphajava.team8.models.workitems.implementations.FeedbackImpl;
import com.alphajava.team8.models.workitems.implementations.StoryImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ListAllWorkItemsCommandTest {
    private Command testCommand;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private Person random;
    private Database database;

    @Before
    public void before() {
        testCommand = new ListAllWorkItemsCommand();
        System.setOut(new PrintStream(outContent));
        database = new Database();
        random = new PersonImpl("Random");
        database.addPerson(random);
    }

    @Test (expected = IllegalArgumentException.class)
    public void command_should_throw_when_lessParameters() {
        //Arrange
        List<String> testList = new ArrayList<>();

        //Act
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void command_should_throw_when_moreParameters() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-all");
        testList.add("extraParameter");

        //Act
        testCommand.execute(testList);
    }

    @Test
    public void command_should_print_when_correctParameters() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-all");

        Feedback feedback = new FeedbackImpl("LongFeedbackName", StatusType.SCHEDULED, 3, "Default description");
        Story story = new StoryImpl("LongStoryName", StatusType.INPROGRESS, PriorityType.MEDIUM, SizeType.MEDIUM, random, "Default description");
        database.addWorkItem(feedback);
        database.addWorkItem(story);


        String expectedOutput = feedback.toString().concat(String.format("%n") + story.toString());

        //Act
        testCommand.execute(testList);

        //Assert
        Assert.assertEquals(expectedOutput, outContent.toString().trim());
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

}