package com.alphajava.team8.commands.list.filter;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.commands.list.all.ListAllWorkItemsCommand;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.implementations.PersonImpl;
import com.alphajava.team8.models.workitems.contracts.Bug;
import com.alphajava.team8.models.workitems.contracts.Feedback;
import com.alphajava.team8.models.workitems.contracts.Story;
import com.alphajava.team8.models.workitems.enums.PriorityType;
import com.alphajava.team8.models.workitems.enums.SeverityType;
import com.alphajava.team8.models.workitems.enums.SizeType;
import com.alphajava.team8.models.workitems.enums.StatusType;
import com.alphajava.team8.models.workitems.implementations.BugImpl;
import com.alphajava.team8.models.workitems.implementations.FeedbackImpl;
import com.alphajava.team8.models.workitems.implementations.StoryImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class ListWorkItemsByAssigneeCommandTest {
    private Command testCommand;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private Person person1;
    private Person person2;
    private Database database;

    @Before
    public void before() {
        testCommand = new ListWorkItemsByAssigneeCommand();
        System.setOut(new PrintStream(outContent));
        database = new Database();
        person1 = new PersonImpl("FirstPerson");
        person2 = new PersonImpl("SecondPerson");
        database.addPerson(person1);
        database.addPerson(person2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void command_should_throw_when_lessParameters() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-filter");

        //Act
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void command_should_throw_when_moreParameters() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-filter");
        testList.add("FirstPerson");
        testList.add("extraParameter");

        //Act
        testCommand.execute(testList);
    }

    @Test
    public void command_should_print_when_correctParameters() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-filter");
        testList.add("FirstPerson");

        Feedback feedback1 = new FeedbackImpl("LongFeedbackName", StatusType.SCHEDULED, 3, "Default description");
        Story story1 = new StoryImpl("LongStoryName", StatusType.INPROGRESS, PriorityType.MEDIUM, SizeType.MEDIUM, person1, "Default description");
        Bug bug1 = new BugImpl("LongBugName", StatusType.ACTIVE, "Default description", PriorityType.MEDIUM, SeverityType.MAJOR, person2);
        Story story2 = new StoryImpl("LongStoryName2", StatusType.NOTDONE, PriorityType.HIGH, SizeType.LARGE, person2, "Default description");
        Feedback feedback2 = new FeedbackImpl("LongFeedbackName2", StatusType.DONE, 4, "Default description");
        Bug bug2 = new BugImpl("LongBugName2", StatusType.FIXED, "Default description", PriorityType.LOW, SeverityType.MINOR, person1);

        database.addWorkItem(feedback1);
        database.addWorkItem(bug1);
        database.addWorkItem(story1);
        database.addWorkItem(feedback2);
        database.addWorkItem(story2);
        database.addWorkItem(bug2);


        String expectedOutput = bug2.toString().concat(String.format("%n") + story1.toString());

        //Act
        testCommand.execute(testList);

        //Assert
        Assert.assertEquals(expectedOutput, outContent.toString().trim());
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }
}