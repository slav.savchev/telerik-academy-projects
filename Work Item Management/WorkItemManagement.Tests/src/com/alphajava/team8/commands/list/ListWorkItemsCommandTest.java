package com.alphajava.team8.commands.list;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.commands.list.all.ListAllWorkItemsCommand;
import com.alphajava.team8.commands.list.filter.*;
import com.alphajava.team8.commands.list.sort.*;
import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.factories.WorkItemManagementFactoryImpl;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.implementations.PersonImpl;
import com.alphajava.team8.models.workitems.contracts.Bug;
import com.alphajava.team8.models.workitems.enums.PriorityType;
import com.alphajava.team8.models.workitems.enums.SeverityType;
import com.alphajava.team8.models.workitems.enums.StatusType;
import com.alphajava.team8.models.workitems.implementations.BugImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ListWorkItemsCommandTest {
    private Command testCommand;
    private WorkItemManagementFactory factory;
    private Database database;
    private Person person;

    @Before
    public void before() {
        factory = new WorkItemManagementFactoryImpl();
        database = new Database();
        testCommand = new ListWorkItemsCommand(factory);
        person = new PersonImpl("FirstPerson");
        database.addPerson(person);
    }

    @Test(expected = IllegalArgumentException.class)
    public void command_should_throw_when_lessParameters() {
        //Arrange
        List<String> testList = new ArrayList<>();

        //Act
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void command_should_throw_when_moreParameters() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-sort");
        testList.add("active");
        testList.add("person");
        testList.add("ExtraParameter");

        //Act
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void command_should_throw_when_invalidOption() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("invalidOption");

        //Act
        testCommand.execute(testList);
    }

    @Test
    public void command_should_call_ListAllWorkItemsCommand_when_optionIsAll() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-all");

        Command shouldBeCalled = new ListAllWorkItemsCommand();
        String expectedResult = shouldBeCalled.execute(testList);

        //Act
        String commandResult = testCommand.execute(testList);

        //Assert
        Assert.assertEquals(expectedResult, commandResult);
    }

    @Test
    public void command_should_call_ListAllBugsCommand_when_optionIsFilterBugs() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-filter");
        testList.add("bugs");

        Command shouldBeCalled = new ListAllBugsCommand();
        String expectedResult = shouldBeCalled.execute(testList);

        //Act
        String commandResult = testCommand.execute(testList);

        //Assert
        Assert.assertEquals(expectedResult, commandResult);
    }

    @Test
    public void command_should_call_ListAllFeedbackCommand_when_optionIsFilterFeedback() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-filter");
        testList.add("feedback");

        Command shouldBeCalled = new ListAllFeedbackCommand();
        String expectedResult = shouldBeCalled.execute(testList);

        //Act
        String commandResult = testCommand.execute(testList);

        //Assert
        Assert.assertEquals(expectedResult, commandResult);
    }

    @Test
    public void command_should_call_ListAllStoriesCommand_when_optionIsFilterStories() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-filter");
        testList.add("stories");

        Command shouldBeCalled = new ListAllStoriesCommand();
        String expectedResult = shouldBeCalled.execute(testList);

        //Act
        String commandResult = testCommand.execute(testList);

        //Assert
        Assert.assertEquals(expectedResult, commandResult);
    }

    @Test
    public void command_should_call_ListWorkItemsByAssigneeCommand_when_optionIsFilterPersonName() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-filter");
        testList.add("FirstPerson");

        Command shouldBeCalled = new ListWorkItemsByAssigneeCommand();
        String expectedResult = shouldBeCalled.execute(testList);

        //Act
        String commandResult = testCommand.execute(testList);

        //Assert
        Assert.assertEquals(expectedResult, commandResult);
    }

    @Test
    public void command_should_call_ListWorkItemsByStatusCommand_when_optionIsFilterStatusType() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-filter");
        testList.add("Active");

        Bug bug = new BugImpl("LongBugName", StatusType.ACTIVE, "Default description", PriorityType.MEDIUM, SeverityType.MAJOR, person);
        database.addWorkItem(bug);

        Command shouldBeCalled = new ListWorkItemsByStatusCommand();
        String expectedResult = shouldBeCalled.execute(testList);

        //Act
        String commandResult = testCommand.execute(testList);

        //Assert
        Assert.assertEquals(expectedResult, commandResult);
    }

    @Test
    public void command_should_call_ListWorkItemsByStatusAndAssigneeCommand_when_optionIsFilterStatusTypeAndPersonName() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-filter");
        testList.add("Active");
        testList.add("FirstPerson");

        Bug bug = new BugImpl("LongBugName", StatusType.ACTIVE, "Default description", PriorityType.MEDIUM, SeverityType.MAJOR, person);
        database.addWorkItem(bug);

        Command shouldBeCalled = new ListWorkItemsByStatusAndAssigneeCommand();
        String expectedResult = shouldBeCalled.execute(testList);

        //Act
        String commandResult = testCommand.execute(testList);

        //Assert
        Assert.assertEquals(expectedResult, commandResult);
    }

    @Test (expected = IllegalArgumentException.class)
    public void command_should_throw_when_invalidStatusType() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-filter");
        testList.add("InvalidStatus");

        Bug bug = new BugImpl("LongBugName", StatusType.ACTIVE, "Default description", PriorityType.MEDIUM, SeverityType.MAJOR, person);
        database.addWorkItem(bug);

        //Act
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void command_should_throw_when_personDoesNotExist() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-filter");
        testList.add("NonexistentPerson");

        Bug bug = new BugImpl("LongBugName", StatusType.ACTIVE, "Default description", PriorityType.MEDIUM, SeverityType.MAJOR, person);
        database.addWorkItem(bug);

        //Act
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void command_should_throw_when_validStatusTypeButPersonDoesNotExist() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-filter");
        testList.add("Active");
        testList.add("NonexistentPerson");

        Bug bug = new BugImpl("LongBugName", StatusType.ACTIVE, "Default description", PriorityType.MEDIUM, SeverityType.MAJOR, person);
        database.addWorkItem(bug);

        //Act
        testCommand.execute(testList);
    }

    @Test
    public void command_should_call_ListWorkItemsSortedByPriorityCommand_when_optionIsSortPriority() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-sort");
        testList.add("priority");

        Command shouldBeCalled = new ListWorkItemsSortedByPriorityCommand();
        String expectedResult = shouldBeCalled.execute(testList);

        //Act
        String commandResult = testCommand.execute(testList);

        //Assert
        Assert.assertEquals(expectedResult, commandResult);
    }

    @Test
    public void command_should_call_ListWorkItemsSortedByRatingCommand_when_optionIsSortRating() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-sort");
        testList.add("rating");

        Command shouldBeCalled = new ListWorkItemsSortedByRatingCommand();
        String expectedResult = shouldBeCalled.execute(testList);

        //Act
        String commandResult = testCommand.execute(testList);

        //Assert
        Assert.assertEquals(expectedResult, commandResult);
    }

    @Test
    public void command_should_call_ListWorkItemsSortedBySeverityCommand_when_optionIsSortSeverity() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-sort");
        testList.add("severity");

        Command shouldBeCalled = new ListWorkItemsSortedBySeverityCommand();
        String expectedResult = shouldBeCalled.execute(testList);

        //Act
        String commandResult = testCommand.execute(testList);

        //Assert
        Assert.assertEquals(expectedResult, commandResult);
    }

    @Test
    public void command_should_call_ListWorkItemsSortedBySizeCommand_when_optionIsSortSize() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-sort");
        testList.add("size");

        Command shouldBeCalled = new ListWorkItemsSortedBySizeCommand();
        String expectedResult = shouldBeCalled.execute(testList);

        //Act
        String commandResult = testCommand.execute(testList);

        //Assert
        Assert.assertEquals(expectedResult, commandResult);
    }

    @Test
    public void command_should_call_ListWorkItemsSortedByTitleCommand_when_optionIsSortTitle() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-sort");
        testList.add("title");

        Command shouldBeCalled = new ListWorkItemsSortedByTitleCommand();
        String expectedResult = shouldBeCalled.execute(testList);

        //Act
        String commandResult = testCommand.execute(testList);

        //Assert
        Assert.assertEquals(expectedResult, commandResult);
    }

    @Test (expected = IllegalArgumentException.class)
    public void command_should_throw_when_invalidSortOption() {
        //Arrange
        List<String> testList = new ArrayList<>();
        testList.add("-sort");
        testList.add("InvalidSortOption");

        //Act
        testCommand.execute(testList);
    }
}