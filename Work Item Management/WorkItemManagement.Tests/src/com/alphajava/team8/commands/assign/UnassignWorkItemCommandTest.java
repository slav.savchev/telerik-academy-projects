package com.alphajava.team8.commands.assign;

import com.alphajava.team8.commands.add.AddPersonCommand;
import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.factories.WorkItemManagementFactoryImpl;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.contracts.Team;
import com.alphajava.team8.models.teams.implementations.PersonImpl;
import com.alphajava.team8.models.teams.implementations.TeamImpl;
import com.alphajava.team8.models.workitems.contracts.WorkItem;
import com.alphajava.team8.models.workitems.enums.PriorityType;
import com.alphajava.team8.models.workitems.enums.SeverityType;
import com.alphajava.team8.models.workitems.enums.StatusType;
import com.alphajava.team8.models.workitems.implementations.BugImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UnassignWorkItemCommandTest {
    private Database database;
    private WorkItemManagementFactory factory;
    private List<String> testList;

    @Before
    public void before(){
        database = new Database();
        factory = new WorkItemManagementFactoryImpl();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throw_when_argumentCountIsInvalid(){

        //Arrange
        testList = new ArrayList<>();
        testList.add("firstArg");
        testList.add("secondArg");
        testList.add("thirdArg");

        UnassignWorkItemCommand testObj = new UnassignWorkItemCommand();

        //Act
        testObj.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throw_when_parametersAreIncorrect(){

        //Arrange
        Person person = new PersonImpl("Svetoslav");
        database.addPerson(person);

        testList = new ArrayList<>();
        testList.add("TestParameter");
        testList.add(person.getName());

        UnassignWorkItemCommand testObj = new UnassignWorkItemCommand();

        //Act
        testObj.execute(testList);
    }

    @Test
    public void person_should_contain_workItem_when_assigned(){

        //Arrange
        Person person = new PersonImpl("Svetoslav");
        Person person1 = new PersonImpl("Emilio");
        database.addPerson(person);
        database.addPerson(person1);

        Team team = new TeamImpl("Team8");
        database.addTeam(team);

        WorkItem item = new BugImpl("NewBugForPersonSvetoslav", StatusType.ACTIVE,"NewDescriptionForBug", PriorityType.HIGH, SeverityType.CRITICAL,person);
        database.addWorkItem(item);

        testList = new ArrayList<>();
        testList.add(String.valueOf(item.getId()));
        testList.add(person1.getName());

        List<String> testList2 = new ArrayList<>();
        testList2.add(person1.getName());
        testList2.add(team.getTeamName());

        AssignWorkItemCommand testObj = new AssignWorkItemCommand();
        AddPersonCommand testObj2 = new AddPersonCommand();
        UnassignWorkItemCommand testObj3 = new UnassignWorkItemCommand();

        //Act
        testObj2.execute(testList2);
        testObj.execute(testList);
        testObj3.execute(testList);

        //Assert
        Assert.assertFalse(person1.getWorkItems().contains(item));
    }

    @Test
    public void history_should_record_activity_when_itemIsUnassigned(){

        //Arrange
        Person person = new PersonImpl("Svetoslav");
        database.addPerson(person);

        Team team = new TeamImpl("Team8");
        database.addTeam(team);

        WorkItem item = new BugImpl("NewBugForPersonSvetoslav", StatusType.ACTIVE,"NewDescriptionForBug", PriorityType.HIGH, SeverityType.CRITICAL,person);
        database.addWorkItem(item);

        testList = new ArrayList<>();
        testList.add(String.valueOf(item.getId()));
        testList.add(person.getName());

        List<String> testList2 = new ArrayList<>();
        testList2.add(person.getName());
        testList2.add(team.getTeamName());

        UnassignWorkItemCommand testObj = new UnassignWorkItemCommand();
        AddPersonCommand testObj2 = new AddPersonCommand();

        //Act
        testObj2.execute(testList2);
        testObj.execute(testList);

        //Assert
        Assert.assertTrue(person.getActivityHistory().size()==2 && item.getHistory().size() == 1);

    }
}