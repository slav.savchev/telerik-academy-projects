package com.alphajava.team8.commands.change.bug;

import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.factories.WorkItemManagementFactoryImpl;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.implementations.PersonImpl;
import com.alphajava.team8.models.workitems.contracts.Bug;
import com.alphajava.team8.models.workitems.contracts.WorkItem;
import com.alphajava.team8.models.workitems.enums.PriorityType;
import com.alphajava.team8.models.workitems.enums.SeverityType;
import com.alphajava.team8.models.workitems.enums.StatusType;
import com.alphajava.team8.models.workitems.implementations.BugImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ChangeBugStatusCommandTest {
    private Database database;
    private WorkItemManagementFactory factory;
    private List<String> testList;

    @Before
    public void before(){
        database = new Database();
        factory = new WorkItemManagementFactoryImpl();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throw_when_argumentCountIsInvalid(){

        //Arrange
        testList = new ArrayList<>();
        testList.add("firstArg");
        testList.add("secondArg");
        testList.add("thirdArg");

        ChangeBugStatusCommand testObj = new ChangeBugStatusCommand();

        //Act
        testObj.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throw_when_parametersAreIncorrect(){

        //Arrange
        testList = new ArrayList<>();
        testList.add("TestParameter");
        testList.add(StatusType.ACTIVE.toString());

        ChangeBugStatusCommand testObj = new ChangeBugStatusCommand();

        //Act
        testObj.execute(testList);
    }

    @Test
    public void status_should_change_when_changed(){

        //Arrange
        Person person = new PersonImpl("Svetoslav");
        database.addPerson(person);

        WorkItem item = new BugImpl("NewBugForPersonSvetoslav", StatusType.ACTIVE,"NewDescriptionForBug", PriorityType.HIGH, SeverityType.CRITICAL,person);
        database.addWorkItem(item);

        testList = new ArrayList<>();
        testList.add(String.valueOf(item.getId()));
        testList.add(StatusType.FIXED.toString().toUpperCase());

        ChangeBugStatusCommand testObj = new ChangeBugStatusCommand();

        //Act
        testObj.execute(testList);

        //Assert
        Assert.assertTrue(item.getStatus().equals(StatusType.FIXED));
    }

    @Test
    public void history_should_record_activity_when_statusIsChanged(){

        //Arrange
        Person person = new PersonImpl("Svetoslav");
        database.addPerson(person);

        WorkItem item = new BugImpl("NewBugForPersonSvetoslav", StatusType.ACTIVE,"NewDescriptionForBug", PriorityType.HIGH, SeverityType.CRITICAL,person);
        database.addWorkItem(item);

        testList = new ArrayList<>();
        testList.add(String.valueOf(item.getId()));
        testList.add(StatusType.FIXED.toString().toUpperCase());

        ChangeBugStatusCommand testObj = new ChangeBugStatusCommand();

        //Act
        testObj.execute(testList);

        //Assert
        Assert.assertTrue(item.getHistory().size() == 1);
    }

    @Test (expected = IllegalArgumentException.class)
    public void factory_should_throw_when_statusIncorrect(){

        //Arrange
        Person person = new PersonImpl("Svetoslav");
        database.addPerson(person);

        WorkItem item = new BugImpl("NewBugForPersonSvetoslav", StatusType.ACTIVE,"NewDescriptionForBug", PriorityType.HIGH, SeverityType.CRITICAL,person);
        database.addWorkItem(item);

        testList = new ArrayList<>();
        testList.add(String.valueOf(item.getId()));
        testList.add("Bla");

        ChangeBugStatusCommand testObj = new ChangeBugStatusCommand();

        //Act
        testObj.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void factory_should_throw_when_statusFromAnotherItem(){

        //Arrange
        Person person = new PersonImpl("Svetoslav");
        database.addPerson(person);

        WorkItem item = new BugImpl("NewBugForPersonSvetoslav", StatusType.ACTIVE,"NewDescriptionForBug", PriorityType.HIGH, SeverityType.CRITICAL,person);
        database.addWorkItem(item);

        testList = new ArrayList<>();
        testList.add(String.valueOf(item.getId()));
        testList.add(StatusType.DONE.toString().toUpperCase());

        ChangeBugStatusCommand testObj = new ChangeBugStatusCommand();

        //Act
        testObj.execute(testList);
    }
}