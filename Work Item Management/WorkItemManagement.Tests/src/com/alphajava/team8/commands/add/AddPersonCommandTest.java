package com.alphajava.team8.commands.add;

import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.factories.WorkItemManagementFactoryImpl;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.contracts.Team;
import com.alphajava.team8.models.teams.implementations.PersonImpl;
import com.alphajava.team8.models.teams.implementations.TeamImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AddPersonCommandTest {
    private Database database;
    private WorkItemManagementFactory factory;
    private List<String> testList;

    @Before
    public void before(){
        this.database = new Database();
        this.factory = new WorkItemManagementFactoryImpl();
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throw_when_argumentCountIsInvalid(){

        //Arrange
        testList = new ArrayList<>();
        testList.add("firstArg");
        testList.add("secondArg");
        testList.add("thirdArg");

        AddCommentCommand testObj = new AddCommentCommand();

        //Act
        testObj.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throw_when_parametersAreIncorrect(){

        //Arrange
        Person person = new PersonImpl("Svetoslav");
        database.addPerson(person);

        Team team = new TeamImpl("Team8");
        database.addTeam(team);

        testList = new ArrayList<>();
        testList.add(null);
        testList.add(team.getTeamName());

        AddPersonCommand testObj = new AddPersonCommand();

        //Act
        testObj.execute(testList);
    }

    @Test
    public void person_should_appearInTeam_when_added(){

        //Arrange
        Person person = new PersonImpl("Svetoslav");
        database.addPerson(person);

        Team team = new TeamImpl("Team08");
        database.addTeam(team);

        testList = new ArrayList<>();
        testList.add(person.getName());
        testList.add(team.getTeamName());

        AddPersonCommand testObj = new AddPersonCommand();

        //Act
        testObj.execute(testList);

        //Assert
        Assert.assertTrue(team.getTeamMembers().contains(person));
    }

    @Test
    public void history_should_record_activity_when_personIsAdded(){

        //Arrange
        Person person = new PersonImpl("Svetoslav");
        database.addPerson(person);

        Team team = new TeamImpl("Team08");
        database.addTeam(team);

        testList = new ArrayList<>();
        testList.add(person.getName());
        testList.add(team.getTeamName());

        AddPersonCommand testObj = new AddPersonCommand();

        //Act
        testObj.execute(testList);

        //Assert
        Assert.assertTrue(person.getActivityHistory().size() == 1);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throw_when_personAlreadyAdded(){

        //Arrange
        Person person = new PersonImpl("Svetoslav");
        database.addPerson(person);

        Team team = new TeamImpl("Team8");
        database.addTeam(team);

        team.addPersonToTeam(person);

        testList = new ArrayList<>();
        testList.add(person.getName());
        testList.add(team.getTeamName());

        AddPersonCommand testObj = new AddPersonCommand();

        //Act
        testObj.execute(testList);
    }
}