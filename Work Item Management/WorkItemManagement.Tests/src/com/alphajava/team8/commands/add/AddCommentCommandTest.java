package com.alphajava.team8.commands.add;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.factories.WorkItemManagementFactoryImpl;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.implementations.PersonImpl;
import com.alphajava.team8.models.workitems.contracts.Bug;
import com.alphajava.team8.models.workitems.enums.PriorityType;
import com.alphajava.team8.models.workitems.enums.SeverityType;
import com.alphajava.team8.models.workitems.enums.StatusType;
import com.alphajava.team8.models.workitems.implementations.BugImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.*;

public class AddCommentCommandTest {
    private Command testCommand;
    private Database database;
    private WorkItemManagementFactory factory;
    private List<String> testList;
    private Bug bug;
    private Person person;

    @Before
    public void before(){
        this.database = new Database();
        this.factory = new WorkItemManagementFactoryImpl();
        this.testCommand = new AddCommentCommand();
        this.person = new PersonImpl("Svetosalv");
        this.bug = new BugImpl("NewBugForPersonSvetoslav", StatusType.ACTIVE,"NewDescriptionForBug", PriorityType.HIGH, SeverityType.CRITICAL,person);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throw_when_argumentCountIsInvalid(){

        //Arrange
        testList = new ArrayList<>();
        testList.add("firstArg");
        testList.add("secondArg");
        testList.add("thirdArg");


        //Act
        testCommand.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throw_when_parametersAreIncorrect(){

        //Arrange
        Person person = new PersonImpl("Svetoslav");
        database.addPerson(person);

        testList = new ArrayList<>();
        testList.add("TestParameter");
        testList.add(person.getName());


        //Act
        testCommand.execute(testList);
    }

    //TODO find a way to test AddCommentCommand

}