package com.alphajava.team8.commands.show.all;

import com.alphajava.team8.commands.show.activity.ShowTeamActivityCommand;
import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.factories.WorkItemManagementFactoryImpl;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.contracts.Team;
import com.alphajava.team8.models.teams.implementations.PersonImpl;
import com.alphajava.team8.models.teams.implementations.TeamImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ShowAllPeopleCommandTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    private Database database;
    private WorkItemManagementFactory factory;
    private List<String> testList;

    @Before
    public void before(){
        this.database = new Database();
        this.factory = new WorkItemManagementFactoryImpl();
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throw_when_argumentCountIsInvalid(){

        //Arrange
        testList = new ArrayList<>();
        testList.add("firstArg");

        ShowAllPeopleCommand testObj = new ShowAllPeopleCommand();

        //Act
        testObj.execute(testList);
    }

    @Test
    public void execute_should_print_allPeople_when_prompted(){

        //Arrange
        Person person = new PersonImpl("Svetoslav");
        database.addPerson(person);

        testList = new ArrayList<>();

        ShowAllPeopleCommand testObj = new ShowAllPeopleCommand();

        //Act
        testObj.execute(testList);

        //Assert
        Assert.assertEquals(String.format("List of all users:%n  1.Svetoslav%n"),outContent.toString());
    }


}