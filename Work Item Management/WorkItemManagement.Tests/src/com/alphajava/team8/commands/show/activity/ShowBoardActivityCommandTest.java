package com.alphajava.team8.commands.show.activity;

import com.alphajava.team8.commands.creation.CreateBoardCommand;
import com.alphajava.team8.commands.creation.CreatePersonCommand;
import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.factories.WorkItemManagementFactoryImpl;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Board;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.contracts.Team;
import com.alphajava.team8.models.teams.implementations.BoardImpl;
import com.alphajava.team8.models.teams.implementations.PersonImpl;
import com.alphajava.team8.models.teams.implementations.TeamImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ShowBoardActivityCommandTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    private Database database;
    private WorkItemManagementFactory factory;
    private List<String> testList;

    @Before
    public void before(){
        this.database = new Database();
        this.factory = new WorkItemManagementFactoryImpl();
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throw_when_argumentCountIsInvalid(){

        //Arrange
        testList = new ArrayList<>();
        testList.add("firstArg");
        testList.add("secondArg");
        testList.add("thirdArg");

        ShowBoardActivityCommand testObj = new ShowBoardActivityCommand();

        //Act
        testObj.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throw_when_parametersAreIncorrect(){

        //Arrange

        Team team = new TeamImpl("Team8");
        database.addTeam(team);

        CreateBoardCommand createBoard = new CreateBoardCommand(factory);
        List<String> listForBoard = new ArrayList<>();
        listForBoard.add("NewTeamBoard");
        listForBoard.add("Team8");
        createBoard.execute(listForBoard);

        testList = new ArrayList<>();
        testList.add("NewTeamBoard");
        testList.add("Team8");

        ShowBoardActivityCommand testObj = new ShowBoardActivityCommand();

        //Act
        testObj.execute(testList);
    }

    @Test
    public void board_should_print_activity_when_prompted(){
        //Arrange

        Team team = new TeamImpl("Team8");
        database.addTeam(team);

        CreateBoardCommand createBoard = new CreateBoardCommand(factory);
        List<String> listForBoard = new ArrayList<>();
        listForBoard.add("NewBoard");
        listForBoard.add("Team8");
        createBoard.execute(listForBoard);

        testList = new ArrayList<>();
        testList.add("NewBoard");
        testList.add("Team8");

        ShowBoardActivityCommand testObj = new ShowBoardActivityCommand();

        //Act
        testObj.execute(testList);

        //Assert
        Assert.assertEquals(String.format("Activity history of board NewBoard in team Team8 is:%n  - Board with name NewBoard was added to team with name Team8%n" +
                "===========================================%n" +
                "Activity history of work items in board NewBoard is:%n"),outContent.toString().split(" on ")[0].concat(outContent.toString().split(" on ")[1].substring(23)));
    }
}