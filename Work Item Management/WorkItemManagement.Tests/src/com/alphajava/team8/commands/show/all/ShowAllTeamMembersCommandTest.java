package com.alphajava.team8.commands.show.all;

import com.alphajava.team8.commands.add.AddPersonCommand;
import com.alphajava.team8.commands.creation.CreateBoardCommand;
import com.alphajava.team8.commands.creation.CreatePersonCommand;
import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.factories.WorkItemManagementFactoryImpl;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.contracts.Team;
import com.alphajava.team8.models.teams.implementations.TeamImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ShowAllTeamMembersCommandTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    private Database database;
    private WorkItemManagementFactory factory;
    private List<String> testList;

    @Before
    public void before(){
        this.database = new Database();
        this.factory = new WorkItemManagementFactoryImpl();
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throw_when_argumentCountIsInvalid(){

        //Arrange
        testList = new ArrayList<>();
        testList.add("firstArg");
        testList.add("secondArg");

        ShowAllTeamMembersCommand testObj = new ShowAllTeamMembersCommand();

        //Act
        testObj.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throw_when_parametersAreIncorrect(){

        //Arrange
        Team team = new TeamImpl("Team8");
        database.addTeam(team);

        CreatePersonCommand personObj = new CreatePersonCommand(factory,database);

        List<String> testListPerson = new ArrayList<>();
        testListPerson.add("Svetoslav");

        AddPersonCommand addPersonObj = new AddPersonCommand();

        List<String> addPersonList = new ArrayList<>();
        addPersonList.add("Svetoslav");
        addPersonList.add("Team8");

        personObj.execute(testListPerson);
        addPersonObj.execute(addPersonList);

        testList = new ArrayList<>();
        testList.add(null);

        ShowAllTeamMembersCommand testObj = new ShowAllTeamMembersCommand();

        //Act
        testObj.execute(testList);
    }

    @Test
    public void execute_should_print_allTeamMembers_when_prompted(){

        //Arrange
        Team team = new TeamImpl("Team8");
        database.addTeam(team);

        CreatePersonCommand personObj = new CreatePersonCommand(factory,database);

        List<String> testListPerson = new ArrayList<>();
        testListPerson.add("Svetoslav");

        AddPersonCommand addPersonObj = new AddPersonCommand();

        List<String> addPersonList = new ArrayList<>();
        addPersonList.add("Svetoslav");
        addPersonList.add("Team8");

        personObj.execute(testListPerson);
        addPersonObj.execute(addPersonList);

        testList = new ArrayList<>();
        testList.add("Team8");

        ShowAllTeamMembersCommand testObj = new ShowAllTeamMembersCommand();

        //Act
        testObj.execute(testList);

        //Assert
        Assert.assertEquals(String.format("List of all team members:%n  1.Svetoslav%n"),outContent.toString());
    }
}