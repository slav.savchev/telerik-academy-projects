package com.alphajava.team8.commands.show.activity;

import com.alphajava.team8.commands.creation.CreateBoardCommand;
import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.factories.WorkItemManagementFactoryImpl;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.contracts.Team;
import com.alphajava.team8.models.teams.implementations.PersonImpl;
import com.alphajava.team8.models.teams.implementations.TeamImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ShowPersonActivityCommandTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    private Database database;
    private WorkItemManagementFactory factory;
    private List<String> testList;

    @Before
    public void before(){
        this.database = new Database();
        this.factory = new WorkItemManagementFactoryImpl();
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throw_when_argumentCountIsInvalid(){

        //Arrange
        testList = new ArrayList<>();
        testList.add("firstArg");
        testList.add("secondArg");

        ShowPersonActivityCommand testObj = new ShowPersonActivityCommand();

        //Act
        testObj.execute(testList);
    }

    @Test (expected = IllegalArgumentException.class)
    public void execute_should_throw_when_parametersAreIncorrect(){

        //Arrange
        Person person = new PersonImpl("Svetoslav");

        testList = new ArrayList<>();
        testList.add("Svetoslav");

        ShowPersonActivityCommand testObj = new ShowPersonActivityCommand();

        //Act
        testObj.execute(testList);
    }

    @Test
    public void person_should_print_activity_when_prompted(){

        //Arrange
        Person person = new PersonImpl("Svetoslav");
        database.addPerson(person);

        testList = new ArrayList<>();
        testList.add("Svetoslav");

        ShowPersonActivityCommand testObj = new ShowPersonActivityCommand();

        //Act
        testObj.execute(testList);

        //Assert
        Assert.assertEquals(String.format("Activity history of user Svetoslav:%n"),outContent.toString());
    }
}