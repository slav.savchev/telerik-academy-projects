package com.alphajava.team8.core.factories;

import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.implementations.PersonImpl;
import com.alphajava.team8.models.workitems.contracts.Bug;
import com.alphajava.team8.models.workitems.contracts.Feedback;
import com.alphajava.team8.models.workitems.contracts.Story;
import com.alphajava.team8.models.workitems.contracts.WorkItem;
import com.alphajava.team8.models.workitems.enums.PriorityType;
import com.alphajava.team8.models.workitems.enums.SeverityType;
import com.alphajava.team8.models.workitems.enums.StatusType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class WorkItemManagementFactoryImplTest {
    private WorkItemManagementFactory factory;
    private Database database;

    @Before
    public void before() {
        factory = new WorkItemManagementFactoryImpl();
        database = new Database();
    }

    @Test
    public void factory_should_create_when_correctParameters() {
        //Arrange
        Person person = new PersonImpl("DefaultPerson");
        database.addPerson(person);

        //Act
        Bug bug = factory.createBug("LongBugName", "Fixed", "Low", "Minor", "DefaultPerson", "Default description.");
        Story story = factory.createStory("LongStoryName", "InProgress", "Medium", "Medium", "DefaultPerson", "Default description.");
        Feedback feedback = factory.createFeedback("LongFeedbackName", "Done", 3, "Default description.");

        database.addWorkItem(bug);
        database.addWorkItem(story);

        Bug itemInDatabase = (Bug)Database.getWorkItemByID(bug.getId());
        boolean expectedResults =
                itemInDatabase.getTitle().equals("LongBugName") &&
                        itemInDatabase.getType().equals("Bug") &&
                        itemInDatabase.getStatus().equals(StatusType.FIXED) &&
                        itemInDatabase.getPriority().equals(PriorityType.LOW) &&
                        itemInDatabase.getSeverity().equals(SeverityType.MINOR) &&
                        itemInDatabase.getAssignee().equals(person) &&
                        itemInDatabase.getDescription().equals("Default description.");

        //Assert

        Assert.assertTrue(expectedResults);

    }

    @Test (expected = IllegalArgumentException.class)
    public void command_should_throw_when_incorrectEnums() {
        //Arrange
        Person person = new PersonImpl("DefaultPerson");
        database.addPerson(person);

        //Act
        Bug bug = factory.createBug("LongBugName", "Bla", "Bla", "Bla", "DefaultPerson", "Default description.");
    }
}