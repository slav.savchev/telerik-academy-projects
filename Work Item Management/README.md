# Work Item Management

Here you will find information regarding the OOP Teamwork Assignment of Team 8.

This is the link for our trello: [*Team 8 Trello*](https://trello.com/b/Dr7SsVBA/work-item-management-team-08-java-aplha)

## Project description

Our goal is to design and implement a Work Item Management (WIM) Console Application. 

## Functional details

Application supports multiple teams. Each team should has name, members and boards.

Member has **name**, list of **work items** and **activity history**.

 -  Name should be unique in the application

 - Name is a string between 5 and 15 symbols.

Board has name, list of work items and activity history.

  - Name should be unique in the team

  - Name is a string between 5 and 10 symbols.

There are 3 types of work items: **bug**, **story** and **feedback**. 

### Bug

Bug has ID, title, description, steps to reproduce, priority, severity, status, assignee, comments and history.

- Title is a string between 10 and 50 symbols.

- Description is a string between 10 and 500 symbols.

- Steps to reproduce is a list of strings.

- Priority is one of the following: High, Medium, Low

- Severity is one of the following: Critical, Major, Minor 
 
- Status is one of the following: Active, Fixed 

- Assignee is a member from the team.

- Comments is a list of comments (string messages with author).

- History is a list of all changes (string messages) that were done to the bug. 

### Story

Story has ID, title, description, priority, size, status, assignee, comments and history.

- Title is a string between 10 and 50 symbols.

- Description is a string between 10 and 500 symbols.

- Priority is one of the following: High, Medium, Low

- Size is one of the following: Large, Medium, Small 
 
- Status is one of the following: NotDone, InProgress, Done 
 
- Assignee is a member from the team.

- Comments is a list of comments (string messages with author).

- History is a list of all changes (string messages) that were done to the story.

### Feedback

Feedback has ID, title, description, rating, status, comments and history.

- Title is a string between 10 and 50 symbols.

- Description is a string between 10 and 500 symbols.

- Rating is an integer.

- Status is one of the following: New, Unscheduled, Scheduled, Done 
 
 - Comments is a list of comments (string messages with author).

- History is a list of all changes (string messages) that were done to the feedback.
 
**Note: IDs of work items are unique in the application i.e. if we have a bug with ID X then we can't have Story of Feedback with ID X.** 

### Operations

Application supports the following operations:

- Create a new person

- Show all people

- Show person's activity

- Create a new team

- Show all teams

- Show team's activity

- Add person to team

- Show all team members

- Create a new board in a team

- Show all team boards

- Show board's activity

- Create a new Bug/Story/Feedback in a board

- Change Priority/Severity/Status of a bug

- Change Priority/Size/Status of a story

- Change Rating/Status of a feedback

- Assign/Unassign work item to a person 
 
 - Add comment to a work item

- List work items with options: 
   - List all

   - Filter bugs/stories/feedback only

   - Filter by status and/or asignee

   - Sort by title/priority/severity/size/rating 


## Commands

As a console application, our program recieves commands from the console, processes them and performs the corresponding operations.
This is a list with all the commands and their explanation:

#### Create

 - CreatePerson `[PersonName]` - Creates a new Person with name [PersonName].
 
 - CreateTeam `[TeamName]` - Creates a new Team with name [TeamName].
  
 - CreateBoardInTeam `[BoardName] [TeamName]` - Creates a new Board with name [BoardName] and adds it to an exiting Team with name [TeamName].
 
 - CreateBug `[BugName] [StatusType] [PriorityType] [Severity Type] [AssigneeName]` - Creates a new Bug work item and asks for `[Description]` and `[StepsToReproduce]` in the process.
 
 - CreateStory `[StoryName] [StatusType] [PriorityType] [SizeType] [AssigneeName]` - Creates a new Story work item and asks for `[Description]` in the process.

 - CreateFeedback `[FeedbackName] [StatusType] [Rating]` - Creates a new Feedback work item and asks for `[Description]` in the process.
 
#### Add

 - AddPersonToTeam `[PersonName] [TeamName]` - Adds an existing person to an existing Team.

 - AddCommentToWorkItem `[WorkItemID] [PersonName]` - Adds a comment to a specified work item from a specified person.

#### Assign

 - AssignItemToPerson `[WorkItemID] [PersonName]` - Assigns a specified work item to a specified person.
 
 - UnassignItemFromPerson `[WorkItemID] [PersonName]` - Unassigns a specified work item from a specified person.

#### Change

 - ChangeBugPriority `[WorkItemID] [PriorityType]` - Changes the priority of a specified bug work item.

 - ChangeBugSeverity `[WorkItemID] [SeverityType]` - Changes the severity of a specified bug work item.

 - ChangeBugStatus `[WorkItemID] [StatusType]` - Changes the status of a specified bug work item.

 - ChangeStoryPriority `[WorkItemID] [PriorityType]` - Changes the priority of a specified story work item.
 
 - ChangeStorySize `[WorkItemID] [SizeType]` - Changes the size of a specified story work item.

 - ChangeStoryStatus `[WorkItemID] [StatusType]` - Changes the status of a specified story work item.

 - ChangeFeedbackRating `[WorkItemID] [Rating]` - Changes the rating of a specified feedback work item.
 
 - ChangeFeedbackStatus `[WorkItemID] [StatusType]` - Changes the status of a specified feedback work item.

#### Show
 
 - ShowAllPeople - Shows all created people.

 - ShowPersonActivity `[PersonName]` - Shows the activity history of a specified person.
  
 - ShowTeams - Shows all created teams.

 - ShowTeamMembers `[TeamName]` - Shows all people in a specidied team.
 
 - ShowBoardActivity `[BoardName] [TeamName]` - Shows the board activity history of a specified team.
 
 - ShowTeamActivity `[TeamName]` - Shows the activity history of a specified team.
 
 - ShowTeamBoards `[TeamName]` - Shows all boards in a specified team.
 
#### List

- ListWorkItems `[Option] <Detail1>(Optional) <Detail2>(Optional)` - Lists work items with option (*"-all"*, *"-filter"*, *"-sort"*) and details regarding the option (**-filter**: *"bugs"*, *"stories"*, *"feedback"*, *"status_type"*, *"person_name"*, **-sort**: *"title"*, *"priority"*, *"severity"*, *"size"*, *"rating"*)
   
   - ListWorkItems **-all** - Lists all created work items.
   
   - ListWorkItems **-filter bugs** - Lists all created bugs.
   
   - ListWorkItems **-filter stories** - Lists all created stories.
   
   - ListWorkItems **-filter feedback** - Lists all created feedback.
   
   - ListWorkItems **-filter _status_type_** - Lists all work items with status *status_type*.

   - ListWorkItems **-filter _person_name_** - Lists all work items with assignee *person_name*.
  
   - ListWorkItems **-filter _status_type_  _person_name_** - Lists all work items with status *status_type* **and** assignee *person_name*.
   
   - ListWorkItems **-sort title** - Lists all work items sorted by title.
   
   - ListWorkItems **-sort priority** - Lists all work items sorted by priority.
    
   - ListWorkItems **-sort severity** - Lists all work items sorted by severity.
    
   - ListWorkItems **-sort size** - Lists all work items sorted by size.
    
   - ListWorkItems **-sort rating** - Lists all work items sorted by rating.


### Sample input

```
createPerson Svetoslav

createPerson Emilio

createTeam Team8

addPersonToTeam Svetoslav Team8

addPersonToTeam Emilio Team8

CreateBoardInTeam NewBoard Team8

CreateBug NewBugInTeam Active Medium Major Svetoslav

CreateBug LongBugName1 Fixed Low Major Emilio

CreateBug LongBugName2 Active High Critical Svetoslav

CreateFeedback NewFeedback New 1

CreateFeedback LongFeedback1 UnScheduled 4

CreateFeedback LongFeedback2 Done 2

CreateStory NewStoryInBoard done high large Svetoslav

CreateStory LongStoryName1 InProgress Medium Medium Svetoslav

CreateStory LongStoryName2 Done High Large Emilio

AddCommentToWorkItem 1 Svetoslav

AddCommentToWorkItem 2 Emilio

ChangeBugPriority 1 low

ChangeBugSeverity 1 minor

ChangeBugStatus 1 fixed

ChangeStoryPriority 3 low

ChangeStorySize 7 small

ChangeStoryStatus 7 notDone

ChangeFeedbackRating 4 3

ChangeFeedbackStatus 5 scheduled

AssignItemToPerson 1 Emilio

UnAssignItemFromPerson 1 Emilio

ShowAllPeople

ShowPersonActivity Svetoslav

ShowPersonActivity Emilio

ShowTeams

ShowTeamMembers Team8

ShowBoardActivity NewBoard Team8

ShowTeamActivity Team8

ShowTeamBoards Team8

ListWorkItems -all

ListWorkItems -filter bugs

ListWorkItems -filter stories

ListWorkItems -filter feedback

ListWorkItems -filter done

ListWorkItems -filter Svetoslav

ListWorkItems -sort title

ListWorkItems -sort priority

ListWorkItems -sort severity

ListWorkItems -sort size

ListWorkItems -sort rating


//Commands that should result in proper error messages

createperson bla

createboardinteam bla Team8

createboardinteam BlaBla Team9

addpersontoteam bla Team8

addpersontoteam Emilio bla

createbug bla d

createbug bla Active High Critical Emilio

createbug blablablabla Active High Critical Emilia

createbug blblblblbllblbl Active High Major Emilio //try description with too few symbols

createbug blablablabla bla High Major Emilio

createbug blablablabla Active bla Major Emilio

createbug blablablabla Active High bla Emilio

addcommenttoworkitem 10 Svetoslav

addcommenttoworkitem 4 Someone

changebugpriority 4 Medium

changebugpriority 2 Bla

changefeedbackrating 5 10

changefeedbackstatus 5 InProgress

showboardactivity noboard Team8

showteamboards Team

listworkitems -bla

listworkitems -filter bla

listworkitems -sort bla
```
   
