package com.alphajava.team8.core.contracts;

public interface Reader {
    String readLine();
}
