package com.alphajava.team8.core.contracts;

public interface Engine {
    void start();
}
