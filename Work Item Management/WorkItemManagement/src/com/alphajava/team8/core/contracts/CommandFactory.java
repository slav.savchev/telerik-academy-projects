package com.alphajava.team8.core.contracts;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.repositories.Database;

public interface CommandFactory {
    Command createCommand(String commandTypeAsString, WorkItemManagementFactory factory, Database database);
}
