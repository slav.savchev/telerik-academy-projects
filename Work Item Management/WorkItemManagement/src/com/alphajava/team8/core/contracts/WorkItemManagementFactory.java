package com.alphajava.team8.core.contracts;

import com.alphajava.team8.models.teams.contracts.*;
import com.alphajava.team8.models.workitems.contracts.Bug;
import com.alphajava.team8.models.workitems.contracts.Feedback;
import com.alphajava.team8.models.workitems.contracts.Story;
import com.alphajava.team8.models.workitems.contracts.WorkItem;
import com.alphajava.team8.models.workitems.enums.PriorityType;
import com.alphajava.team8.models.workitems.enums.StatusType;

import java.util.List;

public interface WorkItemManagementFactory {
    Person createPerson(String name);
    Team createTeam(String teamName);
    Board createBoard(String boardName);
    Bug createBug(String title, String status, String priority, String severity, String assigneeName, String description);
    Story createStory(String title, String status, String priority, String size, String assigneeName, String description);
    Feedback createFeedback(String title, String status,int rating, String description);
}