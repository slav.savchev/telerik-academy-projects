package com.alphajava.team8.core.contracts;

public interface Writer {
    void write(String message);

    void writeLine(String message);
}
