package com.alphajava.team8.core;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.contracts.*;
import com.alphajava.team8.core.factories.*;
import com.alphajava.team8.core.providers.*;
import com.alphajava.team8.core.repositories.Database;

import java.util.List;

public class EngineImpl implements Engine {

    private static final String TERMINATION_COMMAND = "Exit";

    private Reader reader;
    private Writer writer;
    private WorkItemManagementFactory workItemManagementFactory;
    private CommandFactory commandFactory;
    private CommandParser commandParser;
    private Database database;

    public EngineImpl() {
        this.reader = new ConsoleReader();
        this.writer = new ConsoleWriter();
        this.workItemManagementFactory = new WorkItemManagementFactoryImpl();
        this.commandFactory = new CommandFactoryImpl();
        this.commandParser = new CommandParserImpl();
        this.database = new Database();
    }

    @Override
    public void start() {
        while (true) {
            try {
                String commandAsString = reader.readLine();
                if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                processCommand(commandAsString);

            } catch (Exception ex) {
                writer.writeLine(ex.getMessage() != null && !ex.getMessage().isEmpty() ? ex.getMessage() : ex.toString());
            }
        }
    }

    private void processCommand(String commandAsString) {
        if (commandAsString == null || commandAsString.trim().equals("")) {
            throw new IllegalArgumentException("Command cannot be null or empty.");
        }

        String commandName = commandParser.parseCommand(commandAsString);
        Command command = commandFactory.createCommand(commandName, workItemManagementFactory, database);
        List<String> parameters = commandParser.parseParameters(commandAsString);
        String executionResult = command.execute(parameters);
        writer.writeLine(executionResult);
    }
}
