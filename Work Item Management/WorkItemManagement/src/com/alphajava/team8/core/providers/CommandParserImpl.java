package com.alphajava.team8.core.providers;

import com.alphajava.team8.core.contracts.CommandParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandParserImpl implements CommandParser {

    @Override
    public String parseCommand(String fullCommand) {
        return fullCommand.split(" ")[0];
    }

    @Override
    public List<String> parseParameters(String fullCommand) {
        String[] commandParts = fullCommand.split(" ");

        List<String> parameters = new ArrayList<>(Arrays.asList(commandParts));
        parameters.remove(0);

        return parameters;
    }
}
