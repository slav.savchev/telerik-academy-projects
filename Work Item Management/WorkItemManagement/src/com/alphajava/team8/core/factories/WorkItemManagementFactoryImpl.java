package com.alphajava.team8.core.factories;

import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Board;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.contracts.Team;
import com.alphajava.team8.models.teams.implementations.BoardImpl;
import com.alphajava.team8.models.teams.implementations.PersonImpl;
import com.alphajava.team8.models.teams.implementations.TeamImpl;
import com.alphajava.team8.models.workitems.contracts.Bug;
import com.alphajava.team8.models.workitems.contracts.Feedback;
import com.alphajava.team8.models.workitems.contracts.Story;
import com.alphajava.team8.models.workitems.enums.PriorityType;
import com.alphajava.team8.models.workitems.enums.SeverityType;
import com.alphajava.team8.models.workitems.enums.SizeType;
import com.alphajava.team8.models.workitems.enums.StatusType;
import com.alphajava.team8.models.workitems.implementations.BugImpl;
import com.alphajava.team8.models.workitems.implementations.FeedbackImpl;
import com.alphajava.team8.models.workitems.implementations.StoryImpl;

public class WorkItemManagementFactoryImpl implements WorkItemManagementFactory {

    @Override
    public Person createPerson(String name) {
        return new PersonImpl(name);
    }

    @Override
    public Team createTeam(String teamName) {
        return new TeamImpl(teamName);
    }

    @Override
    public Board createBoard(String boardName) {
        return new BoardImpl(boardName);
    }

    @Override
    public Bug createBug(String title, String status, String priority, String severity, String assigneeName, String description) {
        checkStatus(status);
        checkPriority(priority);
        checkSeverity(severity);

        return new BugImpl(title, StatusType.valueOf(status.toUpperCase()), description, PriorityType.valueOf(priority.toUpperCase()),
                SeverityType.valueOf(severity.toUpperCase()), Database.getPersonByName(assigneeName));
    }

    @Override
    public Story createStory(String title, String status, String priority, String size, String assigneeName, String description) {
        checkStatus(status);
        checkPriority(priority);
        checkSize(size);

        return new StoryImpl(title, StatusType.valueOf(status.toUpperCase()), PriorityType.valueOf(priority.toUpperCase()),
                SizeType.valueOf(size.toUpperCase()), Database.getPersonByName(assigneeName), description);
    }

    @Override
    public Feedback createFeedback(String title, String status, int rating, String description) {
        checkStatus(status);
        return new FeedbackImpl(title, StatusType.valueOf(status.toUpperCase()), rating, description);
    }

    private void checkStatus(String status) {
        if (!StatusType.contains(status)) {

            throw new IllegalArgumentException(String.format(
                    "'%s' is not a valid Status type. Status can be: %s"
                    , status, StatusType.getValidStatus()));
        }
    }

    private void checkPriority(String priority) {
        if (!PriorityType.contains(priority)) {

            throw new IllegalArgumentException(String.format(
                    "'%s' is not a valid Priority type. Priority can be: %s",
                    priority, PriorityType.getValidPriority()));
        }
    }

    private void checkSize(String size) {
        if (!SizeType.contains(size)) {

            throw new IllegalArgumentException(String.format(
                    "'%s' is not a valid Size type. Size can be: %s",
                    size, SizeType.getValidSize()));
        }
    }

    private void checkSeverity(String severity) {
        if (!SeverityType.contains(severity)) {

            throw new IllegalArgumentException(String.format(
                    "'%s' is not a valid Severity type. Severity can be: %s",
                    severity, SeverityType.getValidSeverity()));
        }
    }

}
