package com.alphajava.team8.core.factories;

import com.alphajava.team8.commands.add.AddCommentCommand;
import com.alphajava.team8.commands.add.AddPersonCommand;
import com.alphajava.team8.commands.assign.AssignWorkItemCommand;
import com.alphajava.team8.commands.assign.UnassignWorkItemCommand;
import com.alphajava.team8.commands.change.bug.ChangeBugPriorityCommand;
import com.alphajava.team8.commands.change.bug.ChangeBugSeverityCommand;
import com.alphajava.team8.commands.change.bug.ChangeBugStatusCommand;
import com.alphajava.team8.commands.change.feedback.ChangeFeedbackRatingCommand;
import com.alphajava.team8.commands.change.feedback.ChangeFeedbackStatusCommand;
import com.alphajava.team8.commands.change.story.ChangeStoryPriorityCommand;
import com.alphajava.team8.commands.change.story.ChangeStorySizeCommand;
import com.alphajava.team8.commands.change.story.ChangeStoryStatusCommand;
import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.commands.creation.*;
import com.alphajava.team8.commands.enums.CommandType;
import com.alphajava.team8.commands.show.activity.ShowBoardActivityCommand;
import com.alphajava.team8.commands.list.ListWorkItemsCommand;
import com.alphajava.team8.commands.show.activity.ShowPersonActivityCommand;
import com.alphajava.team8.commands.show.activity.ShowTeamActivityCommand;
import com.alphajava.team8.commands.show.all.ShowAllPeopleCommand;
import com.alphajava.team8.commands.show.all.ShowAllTeamBoardsCommand;
import com.alphajava.team8.commands.show.all.ShowAllTeamMembersCommand;
import com.alphajava.team8.commands.show.all.ShowAllTeamsCommand;
import com.alphajava.team8.core.contracts.CommandFactory;
import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.repositories.Database;

public class CommandFactoryImpl implements CommandFactory {
    private static final String INVALID_COMMAND = "Invalid command name: %s!";

    @Override
    public Command createCommand(String commandTypeAsString, WorkItemManagementFactory factory, Database database) {

        if (!CommandType.contains(commandTypeAsString.toUpperCase())) {
            throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandTypeAsString));
        }
        CommandType commandType = CommandType.valueOf(commandTypeAsString.toUpperCase());

        switch (commandType){

            case CREATEBUG:
                return new CreateBugCommand(factory,database);
            case CREATEFEEDBACK:
                return new CreateFeedbackCommand(factory,database);
            case CREATESTORY:
                return new CreateStoryCommand(factory,database);
            case CREATEBOARDINTEAM:
                return new CreateBoardCommand(factory);
            case CREATEPERSON:
                return new CreatePersonCommand(factory,database);
            case CREATETEAM:
                return new CreateTeamCommand(factory,database);
            case ADDCOMMENTTOWORKITEM:
                return new AddCommentCommand();
            case ADDPERSONTOTEAM:
                return new AddPersonCommand();
            case ASSIGNITEMTOPERSON:
                return new AssignWorkItemCommand();
            case UNASSIGNITEMFROMPERSON:
                return new UnassignWorkItemCommand();
            case CHANGEBUGPRIORITY:
                return new ChangeBugPriorityCommand();
            case CHANGEBUGSEVERITY:
                return new ChangeBugSeverityCommand();
            case CHANGEBUGSTATUS:
                return new ChangeBugStatusCommand();
            case CHANGESTORYPRIORITY:
                return new ChangeStoryPriorityCommand();
            case CHANGESTORYSIZE:
                return new ChangeStorySizeCommand();
            case CHANGESTORYSTATUS:
                return new ChangeStoryStatusCommand();
            case CHANGEFEEDBACKRATING:
                return new ChangeFeedbackRatingCommand();
            case CHANGEFEEDBACKSTATUS:
                return new ChangeFeedbackStatusCommand();
            case SHOWALLPEOPLE:
                return new ShowAllPeopleCommand();
            case SHOWPERSONACTIVITY:
                return new ShowPersonActivityCommand();
            case SHOWTEAMS:
                return new ShowAllTeamsCommand();
            case SHOWTEAMMEMBERS:
                return new ShowAllTeamMembersCommand();
            case SHOWBOARDACTIVITY:
                return new ShowBoardActivityCommand();
            case LISTWORKITEMS:
                return new ListWorkItemsCommand(factory);
            case SHOWTEAMACTIVITY:
                return new ShowTeamActivityCommand();
            case SHOWTEAMBOARDS:
                return new ShowAllTeamBoardsCommand();
        }
        throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandTypeAsString));
    }
}
