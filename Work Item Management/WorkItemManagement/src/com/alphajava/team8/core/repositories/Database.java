package com.alphajava.team8.core.repositories;

import com.alphajava.team8.models.teams.contracts.Board;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.contracts.Team;
import com.alphajava.team8.models.workitems.contracts.Bug;
import com.alphajava.team8.models.workitems.contracts.Feedback;
import com.alphajava.team8.models.workitems.contracts.Story;
import com.alphajava.team8.models.workitems.contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Database {
    private static List<Person> allPeople;

    private static List<Team> allTeams;

    private static List<WorkItem> allWorkItems;

    public Database() {
        allPeople = new ArrayList<>();
        allTeams = new ArrayList<>();
        allWorkItems = new ArrayList<>();
    }

    public void addPerson(Person person) {
        allPeople.add(person);
    }

    public void addTeam(Team team) {
        allTeams.add(team);
    }

    public void addWorkItem(WorkItem workItem) { allWorkItems.add(workItem); }

    public static List<Person> getAllPeople() {
        return new ArrayList<>(allPeople);
    }

    public static List<Team> getAllTeams() {
        return new ArrayList<>(allTeams);
    }

    public static List<WorkItem> getAllWorkItems() {
        return new ArrayList<>(allWorkItems);
    }

    public static List<Bug> getAllBugs() {
        List<Bug> bugs = new ArrayList<>();
        for (WorkItem item : getAllWorkItems()) {
            if (item.getType().equals("Bug")) {
                bugs.add((Bug)item);
            }
        }
        return bugs;
    }

    public static List<Story> getAllStories () {
        List<Story> stories = new ArrayList<>();
        for (WorkItem item : getAllWorkItems()) {
            if (item.getType().equals("Story")) {
                stories.add((Story)item);
            }
        }
        return stories;
    }

    public static List<Feedback> getAllFeedback() {
        List<Feedback> allFeedback = new ArrayList<>();
        for (WorkItem item : getAllWorkItems()) {
            if (item.getType().equals("Feedback")) {
                allFeedback.add((Feedback) item);
            }
        }
        return allFeedback;
    }


    public static Person getPersonByName(String name) {

        if (!personExists(name)) {
            throw new IllegalArgumentException(String.format("Person with name %s does not exist", name));
        }

        List<Person> personInList = getAllPeople()
                .stream()
                .filter(current -> current.getName().equals(name))
                .collect(Collectors.toList());

        return personInList.get(0);
    }

    public static WorkItem getWorkItemByID(int workItemID){
        if(!workItemExists(workItemID)) {
            throw new IllegalArgumentException(String.format("Work item with ID %d does not exist.",workItemID));
        }

        List<WorkItem> workItemInList = getAllWorkItems()
                .stream()
                .filter(currentWorkItem -> currentWorkItem.getId() == workItemID)
                .collect(Collectors.toList());

        return workItemInList.get(0);
    }

    public static Team getTeamByName(String name){
        if (!teamExists(name)) {
            throw new IllegalArgumentException(String.format("Team with name '%s' does not exist.", name));
        }

        List<Team> teamInList = getAllTeams()
                .stream()
                .filter(currentTeam -> currentTeam.getTeamName().equals(name))
                .collect(Collectors.toList());

        return teamInList.get(0);
    }

    public static Board getBoard(String teamName, String boardName){

        if(!boardExists(boardName, teamName)) {
            throw new IllegalArgumentException(String.format("Board '%s' does not exist in team '%s'", boardName, teamName));
        }

        Team specificTeam = getTeamByName(teamName);
        List<Board> boardsList = specificTeam.getTeamBoards();
        List<Board> boardInList = boardsList
                .stream()
                .filter( boardItem -> boardItem.getBoardName().equals(boardName))
                .collect(Collectors.toList());

        return boardInList.get(0);
    }

    public static boolean personExists(String personName) {
        return getAllPeople().
                stream().
                anyMatch(current -> current.getName().equals(personName));
    }

    public static boolean teamExists(String teamName) {
        return getAllTeams().
                stream().
                anyMatch(current -> current.getTeamName().equals(teamName));
    }

    public static boolean boardExists(String boardName, String teamName) {
        return getTeamByName(teamName).
                getTeamBoards().
                stream().
                anyMatch(board -> board.getBoardName().equals(boardName));
    }

    public static boolean workItemExists(int workItemID){
        return allWorkItems
                .stream()
                .anyMatch(current -> current.getId() == workItemID);
    }
}
