package com.alphajava.team8;

import com.alphajava.team8.core.EngineImpl;
import com.alphajava.team8.core.contracts.Engine;

public class Startup {
    public static void main(String[] args) {

        Engine engine = new EngineImpl();
        engine.start();

    }
}
