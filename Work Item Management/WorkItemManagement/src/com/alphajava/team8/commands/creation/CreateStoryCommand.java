package com.alphajava.team8.commands.creation;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.commands.contracts.Recordable;
import com.alphajava.team8.core.contracts.Reader;
import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.contracts.Writer;
import com.alphajava.team8.core.providers.ConsoleReader;
import com.alphajava.team8.core.providers.ConsoleWriter;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Board;
import com.alphajava.team8.models.workitems.contracts.Story;

import java.time.LocalDateTime;
import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class CreateStoryCommand implements Command, Recordable {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 5;
    private WorkItemManagementFactory factory;
    private Database database;
    private Reader reader;
    private Writer writer;

    public CreateStoryCommand(WorkItemManagementFactory factory, Database database){
        this.factory=factory;
        this.database=database;
        reader = new ConsoleReader();
        writer = new ConsoleWriter();
    }

    private String title;
    private String status;
    private String priority;
    private String size;
    private String assignee;
    private String description;
    private String boardToIncludeStory;
    private String boardTeamName;
    private int storyID;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        description = addDescription();

        checkBoardAndTeam();

        Story story = factory.createStory(title,status,priority,size,assignee,description);
        database.addWorkItem(story);
        this.storyID = story.getId();

        addToBoard(story);

        addToActivityHistory();

        return String.format("Story with ID %d was created", story.getId());
    }


    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try{
            title = parameters.get(0);
            status = parameters.get(1);
            priority = parameters.get(2);
            size = parameters.get(3);
            assignee = parameters.get(4);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse CreateStory command parameters.");
        }
    }

    private String addDescription() {
        writer.writeLine("Please provide story description: ");
        return reader.readLine();
    }

    private void checkBoardAndTeam() {
        writer.writeLine("Which board in a team should include the story?");
        writer.writeLine("Please specify board name: ");
        boardToIncludeStory = reader.readLine();
        writer.writeLine("Please specify team including the board: ");
        boardTeamName = reader.readLine();

        if (!Database.getTeamByName(boardTeamName).getTeamMembers().contains(Database.getPersonByName(assignee))) {
            throw new IllegalArgumentException(String.format("Person '%s' is not a part of team '%s'", assignee, boardTeamName));
        }

        Board specificBoard = Database.getBoard(boardTeamName,boardToIncludeStory);
    }

    private void addToBoard(Story story) {
        Board specificBoard = Database.getBoard(boardTeamName,boardToIncludeStory);
        specificBoard.addWorkItem(story);
    }

    @Override
    public void addToActivityHistory() {
        Database.getPersonByName(assignee).addToActivityHistory(
                String.format("Story with ID: %d was assigned to user %s on ",storyID, assignee) + LocalDateTime.now());
        Database.getWorkItemByID(storyID).addActivityToHistory(
                String.format("Story with ID: %d was added to board with name %s on ",storyID,boardToIncludeStory) + LocalDateTime.now());
        Database.getBoard(boardTeamName,boardToIncludeStory).addActivityToHistory(
                String.format("Story with ID: %d was added to board on ",storyID) + LocalDateTime.now());
    }
}
