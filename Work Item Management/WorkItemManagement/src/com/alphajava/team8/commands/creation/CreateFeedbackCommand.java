package com.alphajava.team8.commands.creation;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.commands.contracts.Recordable;
import com.alphajava.team8.core.contracts.Reader;
import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.contracts.Writer;
import com.alphajava.team8.core.providers.ConsoleReader;
import com.alphajava.team8.core.providers.ConsoleWriter;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Board;
import com.alphajava.team8.models.workitems.contracts.Feedback;

import java.time.LocalDateTime;
import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class CreateFeedbackCommand implements Command, Recordable {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;
    private final WorkItemManagementFactory factory;
    private final Database database;
    private Reader reader;
    private Writer writer;

    public CreateFeedbackCommand(WorkItemManagementFactory factory, Database database){
        this.factory=factory;
        this.database=database;
        reader = new ConsoleReader();
        writer = new ConsoleWriter();
    }

    private String title;
    private String status;
    private int rating;
    private String description;
    private String boardToIncludeFeedback;
    private String boardTeamName;
    private int feedbackID;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        description = addDescription();

        checkBoardAndTeam();

        Feedback feedback = factory.createFeedback(title,status,rating,description);
        database.addWorkItem(feedback);
        this.feedbackID = feedback.getId();

        addToBoard(feedback);

        addToActivityHistory();

        return String.format("Feedback with ID %d was created", feedback.getId());
    }


    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            title = parameters.get(0);
            status = parameters.get(1);
            rating = Integer.parseInt(parameters.get(2));
        } catch (Exception e){
            throw new IllegalArgumentException("Failed to parse CreateFeedback command parameters.");
        }
    }

    private String addDescription() {
        writer.writeLine("Please provide feedback description: ");
        return reader.readLine();
    }

    private void checkBoardAndTeam() {
        writer.writeLine("Which board in a team should include the bug?");
        writer.writeLine("Please specify board name: ");
        boardToIncludeFeedback = reader.readLine();
        writer.writeLine("Please specify team including the board: ");
        boardTeamName = reader.readLine();

        Board specificBoard = Database.getBoard(boardTeamName, boardToIncludeFeedback);
    }

    private void addToBoard(Feedback feedback) {
        Board specificBoard = Database.getBoard(boardTeamName,boardToIncludeFeedback);
        specificBoard.addWorkItem(feedback);
    }

    @Override
    public void addToActivityHistory() {
        Database.getWorkItemByID(feedbackID).addActivityToHistory(
                String.format("Feedback with ID: %d was added to board with name %s on ",feedbackID,boardToIncludeFeedback) + LocalDateTime.now());
        Database.getBoard(boardTeamName,boardToIncludeFeedback).addActivityToHistory(
                String.format("Feedback with ID: %d was added to board on ",feedbackID) + LocalDateTime.now());
    }
}
