package com.alphajava.team8.commands.creation;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Team;

import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class CreateTeamCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private final WorkItemManagementFactory factory;
    private final Database database;

    public CreateTeamCommand(WorkItemManagementFactory factory, Database database){
        this.factory = factory;
        this.database = database;
    }

    private String teamName;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        Team team = factory.createTeam(teamName);
        database.addTeam(team);

        return String.format("Team %s created",team.getTeamName());
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            teamName = parameters.get(0);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse CreateTeam command parameters.");
        }
    }


}
