package com.alphajava.team8.commands.creation;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.commands.contracts.Recordable;
import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;

import java.time.LocalDateTime;
import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class CreatePersonCommand implements Command, Recordable {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private final WorkItemManagementFactory factory;
    private final Database database;

    public CreatePersonCommand(WorkItemManagementFactory factory, Database database){
        this.factory=factory;
        this.database=database;
    }

    private String personName;
    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        Person person = factory.createPerson(personName);
        database.addPerson(person);

        addToActivityHistory();

        return String.format("Person %s created",person.getName());
    }


    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            personName = parameters.get(0);
        } catch (Exception e){
            throw new IllegalArgumentException("Failed to parse CreatePerson command parameters.");
        }
    }

    @Override
    public void addToActivityHistory() {
        Database.getPersonByName(personName).addToActivityHistory(String.format("User %s joined on ", personName) + LocalDateTime.now());
    }
}
