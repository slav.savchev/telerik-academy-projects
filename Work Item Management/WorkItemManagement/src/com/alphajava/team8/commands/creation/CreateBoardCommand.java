package com.alphajava.team8.commands.creation;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.commands.contracts.Recordable;
import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Board;
import com.alphajava.team8.models.teams.contracts.Team;
import com.alphajava.team8.models.teams.implementations.ValidationHelper;

import java.time.LocalDateTime;
import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class CreateBoardCommand implements Command, Recordable {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private final WorkItemManagementFactory factory;

    public CreateBoardCommand(WorkItemManagementFactory factory){
        this.factory = factory;
    }

    private String boardName;
    private String teamName;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        Board board = factory.createBoard(boardName);

        addBoardToTeam(board,teamName);

        addToActivityHistory();

        return String.format("Board %s added to team %s", board.getBoardName(),teamName);
    }


    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            boardName = parameters.get(0);
            teamName = parameters.get(1);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse CreateBoard command parameters.");
        }
    }

    private void addBoardToTeam(Board board, String team) {
        Team specificTeam = Database.getTeamByName(team);
        ValidationHelper.checkIfBoardAlreadyExists(boardName,teamName);
        specificTeam.addBoardToTeam(board);
    }

    @Override
    public void addToActivityHistory() {
        Database.getBoard(teamName,boardName).addActivityToHistory(
                String.format("Board with name %s was added to team with name %s on ",boardName,teamName) + LocalDateTime.now());
    }
}
