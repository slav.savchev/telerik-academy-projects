package com.alphajava.team8.commands.creation;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.commands.contracts.Recordable;
import com.alphajava.team8.core.contracts.Reader;
import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.contracts.Writer;
import com.alphajava.team8.core.providers.ConsoleReader;
import com.alphajava.team8.core.providers.ConsoleWriter;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Board;
import com.alphajava.team8.models.workitems.contracts.Bug;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class CreateBugCommand implements Command, Recordable {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 5;
    private final WorkItemManagementFactory factory;
    private final Database database;
    private Reader reader;
    private Writer writer;

    public CreateBugCommand(WorkItemManagementFactory factory, Database database){
        this.factory = factory;
        this.database = database;
        reader = new ConsoleReader();
        writer = new ConsoleWriter();
        listOfSteps = new ArrayList<>();
    }

    private String title;
    private String status;
    private String priority;
    private String severity;
    private String assignee;
    private String description;
    private List<String> listOfSteps;
    private String boardToIncludeBug;
    private String boardTeamName;
    private int bugID;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        description = addDescription();
        addStepsToReproduce(listOfSteps);

        checkBoardAndTeam();

        Bug bug = factory.createBug(title, status, priority, severity, assignee, description);

        for (String step : listOfSteps) {
            bug.addStepToReproduce(step);
        }

        database.addWorkItem(bug);
        this.bugID = bug.getId();

        addToBoard(bug);

        addToActivityHistory();

        return String.format("Bug with ID %d was created", bug.getId());
    }


    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            title = parameters.get(0);
            status = parameters.get(1);
            priority = parameters.get(2);
            severity = parameters.get(3);
            assignee = parameters.get(4);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse CreateBug command parameters.");
        }
    }

    private String addDescription() {
        writer.writeLine("Please provide bug description: ");
        return reader.readLine();
    }

    private void addStepsToReproduce(List<String> list) {
        String line;
        int counter = 1;
        writer.writeLine("Please provide an explanation as to how one can reproduce the bug, step by step: ");
        writer.writeLine("Enter 'Finished' when you have provided this information.");
        do {
            writer.write(String.format("  %d. ", counter));
            line = reader.readLine();
            list.add(line);
            counter++;
        } while (!line.equalsIgnoreCase("Finished"));
    }

    private void checkBoardAndTeam() {
        writer.writeLine("Which board in a team should include the bug?");
        writer.writeLine("Please specify board name: ");
        boardToIncludeBug = reader.readLine();
        writer.writeLine("Please specify team including the board: ");
        boardTeamName = reader.readLine();

        if (!Database.getTeamByName(boardTeamName).getTeamMembers().contains(Database.getPersonByName(assignee))) {
            throw new IllegalArgumentException(String.format("Person '%s' is not a part of team '%s'", assignee, boardTeamName));
        }

        Board specificBoard = Database.getBoard(boardTeamName, boardToIncludeBug);
    }

    private void addToBoard(Bug bug) {
        Board specificBoard = Database.getBoard(boardTeamName, boardToIncludeBug);
        specificBoard.addWorkItem(bug);
    }

    public void addToActivityHistory() {
        Database.getPersonByName(assignee).addToActivityHistory(
                String.format("Bug with ID: %d was assigned to user %s on ",bugID, assignee) + LocalDateTime.now());
        Database.getWorkItemByID(bugID).addActivityToHistory(
                String.format("Bug with ID: %d was added to board with name %s on ",bugID,boardToIncludeBug) + LocalDateTime.now());
        Database.getBoard(boardTeamName,boardToIncludeBug).addActivityToHistory(
                String.format("Bug with ID: %d was added to board on ",bugID) + LocalDateTime.now());
    }
}
