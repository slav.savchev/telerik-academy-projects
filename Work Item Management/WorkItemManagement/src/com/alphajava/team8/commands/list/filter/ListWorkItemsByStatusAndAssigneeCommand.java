package com.alphajava.team8.commands.list.filter;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.contracts.Writer;
import com.alphajava.team8.core.providers.ConsoleWriter;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.workitems.contracts.WorkItem;
import com.alphajava.team8.models.workitems.enums.StatusType;

import java.util.List;
import java.util.stream.Collectors;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ListWorkItemsByStatusAndAssigneeCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    private Writer writer;

    public ListWorkItemsByStatusAndAssigneeCommand() {
        writer = new ConsoleWriter();
    }

    private StatusType status;
    private Person assignee;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        printWorkItems(getBugsWithStatusAndAssignee());
        printWorkItems(getStoriesWithStatusAndAssignee());

        return String.format("Listed work items with status '%s' and assignee '%s'.", status, assignee.getName());
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            status = StatusType.valueOf(parameters.get(1).toUpperCase());
            assignee = Database.getPersonByName(parameters.get(2));
        } catch (Exception e){
            throw new IllegalArgumentException("Failed to parse ListWorkItemsByStatusAndAssignee command parameters.");
        }
    }

    private List<WorkItem> getBugsWithStatusAndAssignee() {
        return Database.
                getAllBugs().
                stream().
                filter(item -> item.getStatus().equals(status) && item.getAssignee().equals(assignee)).
                collect(Collectors.toList());
    }

    private List<WorkItem> getStoriesWithStatusAndAssignee() {
        return Database.
                getAllStories().
                stream().
                filter(item -> item.getStatus().equals(status) && item.getAssignee().equals(assignee)).
                collect(Collectors.toList());
    }

    private void printWorkItems(List<WorkItem> list) {
        list.forEach(item -> writer.writeLine(item.toString()));
    }
}
