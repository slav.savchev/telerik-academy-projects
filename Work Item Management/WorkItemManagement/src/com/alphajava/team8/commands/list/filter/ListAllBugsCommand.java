package com.alphajava.team8.commands.list.filter;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.contracts.Writer;
import com.alphajava.team8.core.providers.ConsoleWriter;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.workitems.contracts.Bug;

import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ListAllBugsCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private Writer writer;

    public ListAllBugsCommand() {
        writer = new ConsoleWriter();
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        printWorkItems(Database.getAllBugs());

        return "Listed all bugs.";
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void printWorkItems(List<Bug> list) {
        list.forEach(item -> writer.writeLine(item.toString()));
    }
}
