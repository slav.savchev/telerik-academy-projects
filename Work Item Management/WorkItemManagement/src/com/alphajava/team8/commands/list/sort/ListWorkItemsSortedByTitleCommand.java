package com.alphajava.team8.commands.list.sort;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.contracts.Writer;
import com.alphajava.team8.core.providers.ConsoleWriter;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.workitems.contracts.WorkItem;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ListWorkItemsSortedByTitleCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private Writer writer;

    public ListWorkItemsSortedByTitleCommand() {
        writer = new ConsoleWriter();
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        List<WorkItem> allWorkItems = Database.getAllWorkItems();

        allWorkItems = sortByTitle(allWorkItems);

        printWorkItems(allWorkItems);

        return "Listed work items sorted by title.";
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }


    private List<WorkItem> sortByTitle(List<WorkItem> list) {
        return list.
                stream().
                sorted(Comparator.comparing(WorkItem::getTitle)).
                collect(Collectors.toList());
    }

    private void printWorkItems(List<WorkItem> list) {
        list.forEach(item -> writer.writeLine(item.toString()));
    }
}
