package com.alphajava.team8.commands.list.all;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.contracts.Writer;
import com.alphajava.team8.core.providers.ConsoleWriter;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.workitems.contracts.WorkItem;

import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ListAllWorkItemsCommand implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private Writer writer;

    public ListAllWorkItemsCommand() {
        writer = new ConsoleWriter();
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        printWorkItems(Database.getAllWorkItems());

        return "Listed all work items.";
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void printWorkItems(List<WorkItem> list) {
        list.forEach(item -> writer.writeLine(item.toString()));
    }
}
