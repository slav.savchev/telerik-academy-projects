package com.alphajava.team8.commands.list.filter;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.contracts.Writer;
import com.alphajava.team8.core.providers.ConsoleWriter;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.workitems.contracts.WorkItem;

import java.util.List;
import java.util.stream.Collectors;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ListWorkItemsByAssigneeCommand implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private Writer writer;

    public ListWorkItemsByAssigneeCommand() {
        writer = new ConsoleWriter();
    }

    private Person assignee;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        printWorkItems(getBugsWithAssignee());
        printWorkItems(getStoriesWithAssignee());

        return String.format("Listed work items with assignee '%s'.", assignee.getName());
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            assignee = Database.getPersonByName(parameters.get(1));
        } catch (Exception e){
            throw new IllegalArgumentException("Failed to parse ListWorkItemsByAssignee command parameters.");
        }
    }

    private List<WorkItem> getBugsWithAssignee() {
        return Database.
                getAllBugs().
                stream().
                filter(item -> item.getAssignee().equals(assignee)).
                collect(Collectors.toList());
    }

    private List<WorkItem> getStoriesWithAssignee() {
        return Database.
                getAllStories().
                stream().
                filter(item -> item.getAssignee().equals(assignee)).
                collect(Collectors.toList());
    }

    private void printWorkItems(List<WorkItem> list) {
        list.forEach(item -> writer.writeLine(item.toString()));
    }

}
