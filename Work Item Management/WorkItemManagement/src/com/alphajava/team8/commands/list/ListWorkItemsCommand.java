package com.alphajava.team8.commands.list;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.commands.list.all.ListAllWorkItemsCommand;
import com.alphajava.team8.commands.list.filter.*;
import com.alphajava.team8.commands.list.sort.*;
import com.alphajava.team8.core.contracts.WorkItemManagementFactory;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.workitems.enums.StatusType;

import java.util.ArrayList;
import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS_LIST;

public class ListWorkItemsCommand implements Command {
    private static final int MIN_NUMBER_OF_ARGUMENTS = 1;
    private static final int MAX_NUMBER_OF_ARGUMENTS = 3;

    private String option;
    private List<String> details;

    public ListWorkItemsCommand(WorkItemManagementFactory factory) {
        details = new ArrayList<>();
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);
        formatParameters();

        Command listCommand;

        switch (option) {
            case "-ALL": {
                listCommand = new ListAllWorkItemsCommand();
                return listCommand.execute(parameters);
            }

            case "-FILTER": {
                String filterOption = details.get(0).toUpperCase();

                if (filterOption.equals("BUGS")) {
                    listCommand = new ListAllBugsCommand();
                    return listCommand.execute(parameters);
                }
                if (filterOption.equals("STORIES")) {
                    listCommand = new ListAllStoriesCommand();
                    return listCommand.execute(parameters);
                }
                if (filterOption.equals("FEEDBACK")) {
                    listCommand = new ListAllFeedbackCommand();
                    return listCommand.execute(parameters);
                }

                if (StatusType.contains(filterOption)) {
                    if (parameters.size() == 3) {
                        listCommand = new ListWorkItemsByStatusAndAssigneeCommand();
                        return listCommand.execute(parameters);
                    } else {
                        listCommand = new ListWorkItemsByStatusCommand();
                        return listCommand.execute(parameters);
                    }

                } else {
                    if (Database.personExists(details.get(0))) {
                        listCommand = new ListWorkItemsByAssigneeCommand();
                        return listCommand.execute(parameters);
                    }
                }

                throw new IllegalArgumentException(String.format("Invalid filter option '%s'", details.get(0)));
            }

            case "-SORT": {
                String sortOption = details.get(0).toUpperCase();

                switch (sortOption) {
                    case "TITLE": {
                        listCommand = new ListWorkItemsSortedByTitleCommand();
                        return listCommand.execute(parameters);
                    }
                    case "PRIORITY": {
                        listCommand = new ListWorkItemsSortedByPriorityCommand();
                        return listCommand.execute(parameters);
                    }
                    case "SEVERITY": {
                        listCommand = new ListWorkItemsSortedBySeverityCommand();
                        return listCommand.execute(parameters);
                    }
                    case "SIZE": {
                        listCommand = new ListWorkItemsSortedBySizeCommand();
                        return listCommand.execute(parameters);
                    }
                    case "RATING": {
                        listCommand = new ListWorkItemsSortedByRatingCommand();
                        return listCommand.execute(parameters);
                    }

                    default: throw new IllegalArgumentException(String.format("Invalid sort option '%s'", details.get(0)));
                }
            }
        }

        throw new IllegalArgumentException("Failed to parse ListWorkItems command parameters.");
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() < MIN_NUMBER_OF_ARGUMENTS || parameters.size() > MAX_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(
                    INVALID_NUMBER_OF_ARGUMENTS_LIST, MIN_NUMBER_OF_ARGUMENTS, MAX_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            option = parameters.get(0);
            if (parameters.size() >= 2) {
                details.add(parameters.get(1));
            }
            if (parameters.size() == 3) {
                details.add(parameters.get(2));
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ListWorkItems command parameters.");
        }
    }

    private void formatParameters() {
        option = option.toUpperCase();
    }
}
