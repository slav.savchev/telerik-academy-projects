package com.alphajava.team8.commands.list.sort;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.contracts.Writer;
import com.alphajava.team8.core.providers.ConsoleWriter;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.workitems.contracts.WorkItem;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ListWorkItemsSortedByPriorityCommand implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private Writer writer;

    public ListWorkItemsSortedByPriorityCommand() {
        writer = new ConsoleWriter();
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        List<WorkItem> workItemsWithPriority = getWorkItemsWithPriority();

        sortByPriority(workItemsWithPriority);

        printWorkItems(workItemsWithPriority);

        return "Listed work items sorted by priority.";
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }


    private List<WorkItem> getWorkItemsWithPriority() {
        return Database.
                getAllWorkItems().
                stream().
                filter(item -> item.toString().split("Description:")[0].contains("Priority: ")).
                collect(Collectors.toList());
    }

    private void sortByPriority(List<WorkItem> list) {
        list.sort(priorityComparator);
    }

    private Comparator<WorkItem> priorityComparator = new Comparator<WorkItem>() {
        @Override
        public int compare(WorkItem o1, WorkItem o2) {
            if (o1.toString().contains("Priority: High")) {
                return -1;
            }

            else if (o1.toString().contains("Priority: Medium")) {
                if (o2.toString().contains("Priority: High")) {
                    return 1;
                } else return -1;
            }

            else return 1;
        }

    };
    
    private void printWorkItems(List<WorkItem> list) {
        list.forEach(item -> writer.writeLine(item.toString()));
    }

}
