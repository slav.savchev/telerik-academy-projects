package com.alphajava.team8.commands.list.filter;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.contracts.Writer;
import com.alphajava.team8.core.providers.ConsoleWriter;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.workitems.contracts.WorkItem;
import com.alphajava.team8.models.workitems.enums.StatusType;

import java.util.List;
import java.util.stream.Collectors;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ListWorkItemsByStatusCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private Writer writer;

    public ListWorkItemsByStatusCommand() {
        writer = new ConsoleWriter();
    }

    private StatusType status;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        printWorkItems(getWorkItemsWithStatus());

        return String.format("Listed work items with status '%s'.", status);
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            status = StatusType.valueOf(parameters.get(1).toUpperCase());
        } catch (Exception e){
            throw new IllegalArgumentException("Failed to parse ListWorkItemsByStatus command parameters.");
        }
    }

    private List<WorkItem> getWorkItemsWithStatus() {
        return Database.
                getAllWorkItems().
                stream().
                filter(item -> item.getStatus().equals(status)).
                collect(Collectors.toList());
    }

    private void printWorkItems(List<WorkItem> list) {
        list.forEach(item -> writer.writeLine(item.toString()));
    }

}
