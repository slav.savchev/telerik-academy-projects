package com.alphajava.team8.commands.list.sort;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.contracts.Writer;
import com.alphajava.team8.core.providers.ConsoleWriter;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.workitems.contracts.Feedback;

import java.util.List;
import java.util.stream.Collectors;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ListWorkItemsSortedByRatingCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private Writer writer;

    public ListWorkItemsSortedByRatingCommand() {
        writer = new ConsoleWriter();
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        List<Feedback> workItemsWithFeedback = Database.getAllFeedback();

        workItemsWithFeedback = sortByRating(workItemsWithFeedback);

        printWorkItems(workItemsWithFeedback);

        return "Listed work items sorted by rating.";
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }


    private List<Feedback> sortByRating(List<Feedback> list) {
        return list.
                stream().
                sorted((item1, item2) -> Integer.compare(item2.getRating(), item1.getRating())).
                collect(Collectors.toList());
    }

    private void printWorkItems(List<Feedback> list) {
        list.forEach(item -> writer.writeLine(item.toString()));
    }
}
