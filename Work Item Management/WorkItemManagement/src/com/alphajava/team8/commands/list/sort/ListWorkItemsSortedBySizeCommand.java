package com.alphajava.team8.commands.list.sort;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.contracts.Writer;
import com.alphajava.team8.core.providers.ConsoleWriter;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.workitems.contracts.Story;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ListWorkItemsSortedBySizeCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private Writer writer;

    public ListWorkItemsSortedBySizeCommand() {
        writer = new ConsoleWriter();
    }

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        List<Story> workItemsWithSize = Database.getAllStories();

        workItemsWithSize = sortBySize(workItemsWithSize);

        printWorkItems(workItemsWithSize);

        return "Listed work items sorted by size.";
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }


    private List<Story> sortBySize(List<Story> list) {
        return list.
                stream().
                sorted(Comparator.comparing(Story::getSize)).
                collect(Collectors.toList());
    }

    private void printWorkItems(List<Story> list) {
        list.forEach(item -> writer.writeLine(item.toString()));
    }
}
