package com.alphajava.team8.commands.enums;

public enum CommandType {
    CREATEPERSON,
    CREATETEAM,
    CREATEBOARDINTEAM,
    CREATEBUG,
    CREATESTORY,
    CREATEFEEDBACK,
    SHOWALLPEOPLE,
    SHOWPERSONACTIVITY,
    SHOWTEAMS,
    SHOWTEAMACTIVITY,
    SHOWTEAMMEMBERS,
    SHOWTEAMBOARDS,
    SHOWBOARDACTIVITY,
    ADDPERSONTOTEAM,
    ADDCOMMENTTOWORKITEM,
    CHANGEBUGPRIORITY,
    CHANGEBUGSEVERITY,
    CHANGEBUGSTATUS,
    CHANGESTORYPRIORITY,
    CHANGESTORYSIZE,
    CHANGESTORYSTATUS,
    CHANGEFEEDBACKRATING,
    CHANGEFEEDBACKSTATUS,
    ASSIGNITEMTOPERSON,
    UNASSIGNITEMFROMPERSON,
    LISTWORKITEMS;

    public static boolean contains(String value) {
        for (CommandType command : values()) {
            if (command.toString().equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }
}
