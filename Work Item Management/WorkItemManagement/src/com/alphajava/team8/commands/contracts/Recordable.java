package com.alphajava.team8.commands.contracts;

public interface Recordable {
    void addToActivityHistory();
}
