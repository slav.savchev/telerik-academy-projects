package com.alphajava.team8.commands.assign;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.commands.contracts.Recordable;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.workitems.contracts.WorkItem;

import java.time.LocalDateTime;
import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class UnassignWorkItemCommand implements Command, Recordable {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private int workItemID;
    private String personName;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        unAssignWorkItem();

        addToActivityHistory();

        return String.format("Work item with ID: %d unassigned from person %s", workItemID,personName);
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters){
        try {
            workItemID = Integer.parseInt(parameters.get(0));
            personName = parameters.get(1);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse UnAssignItemToPerson command parameters.");
        }
    }

    private void unAssignWorkItem() {
        WorkItem specificItem = Database.getWorkItemByID(workItemID);
        Person specificPerson = Database.getPersonByName(personName);
        specificPerson.unAssignItem(specificItem);
    }

    @Override
    public void addToActivityHistory() {
        Database.getPersonByName(personName).addToActivityHistory(
                String.format("Work item with ID: %d was unassigned from user %s on ", workItemID, personName) + LocalDateTime.now());
        Database.getWorkItemByID(workItemID).addActivityToHistory(
                String.format("Work item with ID: %d was unassigned from user %s on ", workItemID, personName) + LocalDateTime.now());
    }
}
