package com.alphajava.team8.commands.add;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.commands.contracts.Recordable;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.contracts.Team;

import java.time.LocalDateTime;
import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class AddPersonCommand implements Command, Recordable {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private String personName;
    private String teamName;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        addPersonToTeam();

        addToActivityHistory();

        return String.format("Person %s added to team %s", personName,teamName);
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            personName = parameters.get(0);
            teamName = parameters.get(1);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse AddPersonToTeam command parameters.");
        }
    }

    private void addPersonToTeam() {
        Person specificPerson = Database.getPersonByName(personName);
        Team specificTeam = Database.getTeamByName(teamName);
        specificTeam.addPersonToTeam(specificPerson);
    }

    @Override
    public void addToActivityHistory() {
        Database.getPersonByName(personName).addToActivityHistory(
                String.format("User %s was added to team '%s' on ", personName, teamName) + LocalDateTime.now());
    }
}
