package com.alphajava.team8.commands.add;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.commands.contracts.Recordable;
import com.alphajava.team8.core.contracts.Reader;
import com.alphajava.team8.core.contracts.Writer;
import com.alphajava.team8.core.providers.ConsoleReader;
import com.alphajava.team8.core.providers.ConsoleWriter;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.workitems.contracts.WorkItem;

import java.time.LocalDateTime;
import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class AddCommentCommand implements Command, Recordable {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private Reader reader;
    private Writer writer;

    public AddCommentCommand(){
        reader = new ConsoleReader();
        writer = new ConsoleWriter();
    }

    private int workItemID;
    private String author;
    private String comment;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        this.comment = addComment();

        addCommentToWorkItem();

        addToActivityHistory();

        return String.format("Comment added to work item with ID: %d",workItemID);
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            workItemID = Integer.parseInt(parameters.get(0));
            author = parameters.get(1);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse AddCommentToWorkItem command parameters.");
        }
    }

    private String addComment() {
        writer.writeLine("Please enter comment: ");
        return reader.readLine();
    }

    private void addCommentToWorkItem() {
        WorkItem specificWorkItem = Database.getWorkItemByID(workItemID);
        specificWorkItem.addComment(comment, Database.getPersonByName(author));
    }

    @Override
    public void addToActivityHistory() {
        Database.getPersonByName(author).addToActivityHistory(
                String.format("User %s added comment to Work item with ID: %d on ", author, workItemID) + LocalDateTime.now());
    }
}
