package com.alphajava.team8.commands;

public class CommandsConstants {
    public static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments. Expected: %d, Received: %d";
    public static final String INVALID_NUMBER_OF_ARGUMENTS_LIST = "Invalid number of arguments. Expected: %d to %d, Received: %d";
}
