package com.alphajava.team8.commands.change.bug;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.commands.contracts.Recordable;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.workitems.contracts.WorkItem;
import com.alphajava.team8.models.workitems.enums.StatusType;

import java.time.LocalDateTime;
import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ChangeBugStatusCommand implements Command, Recordable {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private int workItemID;
    private StatusType newBugStatus;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        changeBugStatus();

        addToActivityHistory();

        return String.format("Bug ID: %d status changed to %s.",workItemID,newBugStatus);
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            workItemID = Integer.parseInt(parameters.get(0));
            newBugStatus = StatusType.valueOf(parameters.get(1).toUpperCase());
        } catch (IllegalArgumentException invalidStatus) {

            throw new IllegalArgumentException(String.format(
                    "'%s' is not a valid Status type. Status can be: %s",
                    parameters.get(1), StatusType.getValidStatus()));

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ChangeBugStatus command parameters.");
        }
    }

    private void changeBugStatus() {
        WorkItem specificItem = Database.getWorkItemByID(workItemID);
        if (specificItem.getType().equals("Bug")) {
            specificItem.setStatus(newBugStatus);
        }
        else {
            throw new IllegalArgumentException(String.format("Work item with ID %d is not a Bug.", workItemID));
        }
    }

    @Override
    public void addToActivityHistory() {
        Database.getWorkItemByID(workItemID).addActivityToHistory(
                String.format("Bug with ID: %d status changed to %s on ",workItemID,newBugStatus) + LocalDateTime.now());
    }
}
