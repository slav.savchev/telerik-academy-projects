package com.alphajava.team8.commands.change.feedback;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.commands.contracts.Recordable;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.workitems.contracts.WorkItem;
import com.alphajava.team8.models.workitems.enums.StatusType;

import java.time.LocalDateTime;
import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ChangeFeedbackStatusCommand implements Command, Recordable {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private int workItemID;
    private StatusType newFeedbackStatus;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        changeFeedbackStatus();

        addToActivityHistory();


        return String.format("Feedback with ID: %d status changed to %s.",workItemID,newFeedbackStatus);
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            workItemID = Integer.parseInt(parameters.get(0));
            newFeedbackStatus = StatusType.valueOf(parameters.get(1).toUpperCase());
        } catch (IllegalArgumentException invalidStatus) {

            throw new IllegalArgumentException(String.format(
                    "'%s' is not a valid Status type. Status can be: %s",
                    parameters.get(1), StatusType.getValidStatus()));

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ChangeFeedbackStatus command parameters.");
        }
    }

    private void changeFeedbackStatus() {
        WorkItem specificItem = Database.getWorkItemByID(workItemID);
        if (specificItem.getType().equals("Feedback")) {
            specificItem.setStatus(newFeedbackStatus);
        }
        else {
            throw new IllegalArgumentException(String.format("Work item with ID %d is not a feedback.", workItemID));
        }
    }

    @Override
    public void addToActivityHistory() {
        Database.getWorkItemByID(workItemID).addActivityToHistory(
                String.format("Feedback with ID: %d status changed to %s on ",workItemID,newFeedbackStatus) + LocalDateTime.now());
    }
}
