package com.alphajava.team8.commands.change.feedback;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.commands.contracts.Recordable;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.workitems.contracts.Feedback;
import com.alphajava.team8.models.workitems.contracts.WorkItem;

import java.time.LocalDateTime;
import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ChangeFeedbackRatingCommand implements Command, Recordable {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private int workItemID;
    private int newFeedbackRating;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        changeFeedbackRating();

        addToActivityHistory();

        return String.format("Feedback with ID: %d rating changed to %d.",workItemID,newFeedbackRating);
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            workItemID = Integer.parseInt(parameters.get(0));
            newFeedbackRating = Integer.parseInt(parameters.get(1));
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ChangeFeedbackRating command parameters.");
        }
    }

    private void changeFeedbackRating() {
        WorkItem specificItem = Database.getWorkItemByID(workItemID);
        if (specificItem.getType().equals("Feedback")) {
            ((Feedback) specificItem).setRating(newFeedbackRating);
        }
        else {
            throw new IllegalArgumentException(String.format("Work item with ID %d is not a feedback.", workItemID));
        }
    }

    @Override
    public void addToActivityHistory() {
        Database.getWorkItemByID(workItemID).addActivityToHistory(
                String.format("Feedback with ID: %d rating changed to %d on ",workItemID,newFeedbackRating) + LocalDateTime.now());
    }
}
