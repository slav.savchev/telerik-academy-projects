package com.alphajava.team8.commands.change.bug;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.commands.contracts.Recordable;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.workitems.contracts.Bug;
import com.alphajava.team8.models.workitems.contracts.WorkItem;
import com.alphajava.team8.models.workitems.enums.PriorityType;

import java.time.LocalDateTime;
import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ChangeBugPriorityCommand implements Command, Recordable {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private int workItemID;
    private PriorityType newBugPriority;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        changeBugPriority();

        addToActivityHistory();

        return String.format("Bug ID: %d priority changed to %s.",workItemID,newBugPriority);
    }


    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            workItemID = Integer.parseInt(parameters.get(0));
            newBugPriority = PriorityType.valueOf(parameters.get(1).toUpperCase());
        } catch (IllegalArgumentException invalidPriority) {

            throw new IllegalArgumentException(String.format(
                    "'%s' is not a valid Priority type. Priority can be: %s",
                    parameters.get(1), PriorityType.getValidPriority()));

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ChangeBugPriority command parameters.");
        }
    }

    private void changeBugPriority() {
        WorkItem specificItem = Database.getWorkItemByID(workItemID);
        if (specificItem.getType().equals("Bug")) {
            ((Bug) specificItem).setPriority(newBugPriority);
        }
        else {
            throw new IllegalArgumentException(String.format("Work item with ID %d is not a Bug.", workItemID));
        }
    }

    @Override
    public void addToActivityHistory() {
       Database.getWorkItemByID(workItemID).addActivityToHistory(
               String.format("Bug with ID: %d priority changed to %s on ",workItemID,newBugPriority) + LocalDateTime.now());
    }
}
