package com.alphajava.team8.commands.change.story;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.commands.contracts.Recordable;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.workitems.contracts.Story;
import com.alphajava.team8.models.workitems.contracts.WorkItem;
import com.alphajava.team8.models.workitems.enums.SizeType;

import java.time.LocalDateTime;
import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ChangeStorySizeCommand implements Command, Recordable {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private int workItemID;
    private SizeType newStorySize;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        changeStorySize();
        addToActivityHistory();

        return String.format("Story with ID: %d size changed to %s.",workItemID,newStorySize);
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            workItemID = Integer.parseInt(parameters.get(0));
            newStorySize = SizeType.valueOf(parameters.get(1).toUpperCase());
        } catch (IllegalArgumentException invalidSize) {

            throw new IllegalArgumentException(String.format(
                    "'%s' is not a valid Size type. Size can be: %s",
                    parameters.get(1), SizeType.getValidSize()));

        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ChangeStorySize command parameters.");
        }
    }

    private void changeStorySize() {
        WorkItem specificItem = Database.getWorkItemByID(workItemID);
        if (specificItem.getType().equals("Story")) {
            ((Story) specificItem).setSize(newStorySize);
        }
        else {
            throw new IllegalArgumentException(String.format("Work item with ID %d is not a Story.", workItemID));
        }
    }

    @Override
    public void addToActivityHistory() {
        Database.getWorkItemByID(workItemID).addActivityToHistory(
                String.format("Story with ID: %d size changed to %s on ",workItemID,newStorySize) + LocalDateTime.now());
    }
}
