package com.alphajava.team8.commands.show.activity;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.contracts.Writer;
import com.alphajava.team8.core.providers.ConsoleWriter;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Team;

import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ShowTeamActivityCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private final Writer writer;

    public ShowTeamActivityCommand(){
        writer = new ConsoleWriter();
    }

    private String teamName;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        printTeamHistory(Database.getTeamByName(teamName));

        return String.format("Team activity shown.",teamName);
    }



    private void validateInput(List<String> parameters){
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try{
            teamName = parameters.get(0);
        } catch (Exception e){
            throw new IllegalArgumentException("Failed to parse ShowTeamActivity command parameters.");
        }
    }

    private void printTeamHistory(Team teamByName) {
        writer.writeLine(String.format("Activity history of team %s is:",teamName));

        teamByName.getTeamMembers()
                .stream()
                .flatMap(memberActivity -> memberActivity.getActivityHistory().stream())
                .forEach(activity -> writer.writeLine(String.format("  - %s", activity)));
    }
}
