package com.alphajava.team8.commands.show.activity;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.contracts.Writer;
import com.alphajava.team8.core.providers.ConsoleWriter;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Board;

import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ShowBoardActivityCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private final Writer writer;

    public ShowBoardActivityCommand(){
        writer = new ConsoleWriter();
    }

    private String boardName;
    private String teamName;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        printBoardHistory(Database.getBoard(teamName,boardName));

        return String.format("Board %s in team %s activity shown.",boardName,teamName);
    }



    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            boardName = parameters.get(0);
            teamName = parameters.get(1);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ShowBoardActivity command parameters.");
        }
    }

    private void printBoardHistory(Board board) {
        writer.writeLine(String.format("Activity history of board %s in team %s is:", board.getBoardName(), teamName));

        board.getActivityHistory()
                .stream()
                .forEach(activity -> writer.writeLine(String.format("  - %s", activity)) );

        writer.writeLine("===========================================");
        writer.writeLine(String.format("Activity history of work items in board %s is:", board.getBoardName()));

        board.getWorkItems()
                .stream()
                .flatMap(workItems -> workItems.getHistory().stream())
                .forEach(history -> writer.writeLine(String.format("  - %s", history)));

    }

}
