package com.alphajava.team8.commands.show.all;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.contracts.Writer;
import com.alphajava.team8.core.providers.ConsoleWriter;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Team;

import java.util.ArrayList;
import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ShowAllTeamsCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;

    private final Writer writer;

    public ShowAllTeamsCommand() {
        allTeams = new ArrayList<>();
        writer = new ConsoleWriter();
    }

    private List<Team> allTeams;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        allTeams = Database.getAllTeams();
        printAllPeople();

        return "All team have been shown.";
    }


    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void printAllPeople() {
        int count = 1;

        writer.writeLine("List of all teams:");

        for (Team team : allTeams) {
            writer.writeLine(String.format("  %d.%s", count, team.getTeamName()));
            count++;
        }
    }
}
