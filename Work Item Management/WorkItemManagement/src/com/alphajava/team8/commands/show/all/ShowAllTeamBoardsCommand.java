package com.alphajava.team8.commands.show.all;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.contracts.Writer;
import com.alphajava.team8.core.providers.ConsoleWriter;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Team;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ShowAllTeamBoardsCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private final Writer writer;

    public ShowAllTeamBoardsCommand(){
        writer = new ConsoleWriter();
    }

    private String teamName;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        printAllTeamBoards(Database.getTeamByName(teamName));

        return String.format("All %s boards have been shown.",teamName);
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try{
            teamName = parameters.get(0);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ShowTeamBoards command parameters.");
        }
    }

    private void printAllTeamBoards(Team teamByName) {
        AtomicInteger count = new AtomicInteger(0);

        writer.writeLine("List of all team boards:");

        teamByName.getTeamBoards()
                .stream()
                .map(boardNames -> boardNames.getBoardName())
                .forEach(board -> writer.writeLine(String.format("  %d.%s", count.incrementAndGet(), board)));

    }

}
