package com.alphajava.team8.commands.show.all;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.contracts.Writer;
import com.alphajava.team8.core.providers.ConsoleWriter;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;

import java.util.ArrayList;
import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ShowAllPeopleCommand implements Command {

    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;

    private final Writer writer;

    public ShowAllPeopleCommand() {
        allPeople = new ArrayList<>();
        writer = new ConsoleWriter();
    }

    private List<Person> allPeople;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        allPeople = Database.getAllPeople();
        printAllPeople();

        return "All users have been shown.";
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void printAllPeople() {
        int count = 1;

        writer.writeLine("List of all users:");

        for (Person user : allPeople) {
            writer.writeLine(String.format("  %d.%s", count, user.getName()));
            count++;
        }

    }

}