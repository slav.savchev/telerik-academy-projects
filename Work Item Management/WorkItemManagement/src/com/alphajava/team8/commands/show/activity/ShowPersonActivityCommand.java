package com.alphajava.team8.commands.show.activity;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.contracts.Writer;
import com.alphajava.team8.core.providers.ConsoleWriter;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;

import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ShowPersonActivityCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private final Writer writer;

    public ShowPersonActivityCommand() {
        writer = new ConsoleWriter();
    }

    private String personName;

    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        printPersonHistory(Database.getPersonByName(personName));

        return String.format("Person %s's activity shown.",personName);
    }

    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try {
            personName = parameters.get(0);
        } catch (Exception e){
            throw new IllegalArgumentException("Failed to parse ShowPersonActivity command parameters.");
        }
    }


    private void printPersonHistory(Person person) {
        writer.writeLine(String.format("Activity history of user %s:", person.getName()));

        person.getActivityHistory()
                .stream()
                .forEach(activity -> writer.writeLine(String.format("  - %s", activity)));
    }
}