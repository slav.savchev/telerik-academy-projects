package com.alphajava.team8.commands.show.all;

import com.alphajava.team8.commands.contracts.Command;
import com.alphajava.team8.core.contracts.Writer;
import com.alphajava.team8.core.providers.ConsoleWriter;
import com.alphajava.team8.core.repositories.Database;
import com.alphajava.team8.models.teams.contracts.Person;

import java.util.ArrayList;
import java.util.List;

import static com.alphajava.team8.commands.CommandsConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ShowAllTeamMembersCommand implements Command {
    private static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private final Writer writer;

    public ShowAllTeamMembersCommand() {
        allTeamMembers = new ArrayList<>();
        writer = new ConsoleWriter();
    }

    private List<Person> allTeamMembers;
    private String teamName;


    @Override
    public String execute(List<String> parameters) {
        validateInput(parameters);

        parseParameters(parameters);

        allTeamMembers = Database.getTeamByName(teamName).getTeamMembers();
        printAllTeamMembers();

        return String.format("All %s members have been shown.",teamName);
    }


    private void validateInput(List<String> parameters) {
        if (parameters.size() != EXPECTED_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(String.format(INVALID_NUMBER_OF_ARGUMENTS, EXPECTED_NUMBER_OF_ARGUMENTS, parameters.size()));
        }
    }

    private void parseParameters(List<String> parameters) {
        try{
            teamName = parameters.get(0);
        } catch (Exception e) {
            throw new IllegalArgumentException("Failed to parse ShowAllTeamMembers command parameters.");
        }
    }

    private void printAllTeamMembers() {
        int count = 1;

        writer.writeLine("List of all team members:");

        for (Person teamMember : allTeamMembers) {
            writer.writeLine(String.format("  %d.%s", count, teamMember.getName()));
            count++;
        }
    }
}
