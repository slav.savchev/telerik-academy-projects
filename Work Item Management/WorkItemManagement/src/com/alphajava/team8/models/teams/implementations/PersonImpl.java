package com.alphajava.team8.models.teams.implementations;

import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.workitems.contracts.WorkItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class PersonImpl implements Person {

    private String name;
    private List<String> activityHistory;
    private List<WorkItem> workItemList;

    public PersonImpl(String name){
        setName(name);
        activityHistory = new ArrayList<>();
        workItemList = new ArrayList<>();
    }

    @Override
    public List<WorkItem> getWorkItems() {
        return new ArrayList<>(workItemList);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public List<String> getActivityHistory() {
        return new ArrayList<>(activityHistory);
    }

    private void setName(String name) {
        ValidationHelper.checkPersonName(name);
        this.name = name;
    }

    public void assignItem(WorkItem item){
        workItemList.add(item);
    }

    public void unAssignItem(WorkItem item){

        workItemList.removeIf(specificItem -> specificItem.equals(item));
    }

    @Override
    public void addToActivityHistory(String activity) {
        activityHistory.add(activity);
    }


}
