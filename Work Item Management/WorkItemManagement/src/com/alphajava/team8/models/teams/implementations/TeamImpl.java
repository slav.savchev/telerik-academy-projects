package com.alphajava.team8.models.teams.implementations;

import com.alphajava.team8.models.teams.contracts.Board;
import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.teams.contracts.Team;

import java.util.ArrayList;
import java.util.List;

public class TeamImpl implements Team {

    private String teamName;
    private List<Person> teamMembersList;
    private List<Board> boardsList;

    public TeamImpl(String teamName){
        setTeamName(teamName);
        teamMembersList = new ArrayList<>();
        boardsList = new ArrayList<>();
    }

    @Override
    public String getTeamName() {
        return this.teamName;
    }

    @Override
    public List<Person> getTeamMembers() {
        return new ArrayList<>(teamMembersList);
    }

    @Override
    public List<Board> getTeamBoards() {
        return new ArrayList<>(boardsList);
    }

    private void setTeamName(String teamName) {
        ValidationHelper.checkTeamName(teamName);
        this.teamName = teamName;
    }

    public void addPersonToTeam(Person person){
        ValidationHelper.checkNull(person);
        if (personIsInTeam(person)) {
            throw new IllegalArgumentException(String.format("Person with name %s is already in team %s", person.getName(), getTeamName()));
        }
        teamMembersList.add(person);
    }

    public void addBoardToTeam(Board board){
        ValidationHelper.checkNull(board);
        if (boardIsInTeam(board)) {
            throw new IllegalArgumentException(String.format("Board with name %s already exists in %s", board.getBoardName(), getTeamName()));
        }
        boardsList.add(board);
    }

    public boolean personIsInTeam(Person person) {
        return teamMembersList
                .stream()
                .anyMatch(current -> current.getName().equals(person.getName()));
    }

    public boolean boardIsInTeam(Board board) {
        return boardsList
                .stream()
                .anyMatch(current -> current.getBoardName().equals(board.getBoardName()));
    }
}
