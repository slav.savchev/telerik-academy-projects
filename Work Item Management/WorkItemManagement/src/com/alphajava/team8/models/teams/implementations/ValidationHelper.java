package com.alphajava.team8.models.teams.implementations;

import com.alphajava.team8.core.repositories.Database;

public class ValidationHelper {

    // Person

    private static final int PERSON_MIN_NAME_LENGTH= 5;
    private static final int PERSON_MAX_NAME_LENGTH= 15;

    private static final String PERSON_NAME_LENGTH_ERROR = String.format(
            "Person's name must be between %d and %d symbols long.",
            PERSON_MIN_NAME_LENGTH, PERSON_MAX_NAME_LENGTH);

    private static final String PERSON_NAME_NOT_UNIQUE_ERROR = "Name %s already taken.";

    // Board

    private static final int BOARD_MIN_NAME_LENGTH= 5;
    private static final int BOARD_MAX_NAME_LENGTH= 10;

    private static final String BOARD_NAME_LENGTH_ERROR = String.format(
            "Board's name must be between %d and %d symbols long.",
            BOARD_MIN_NAME_LENGTH, BOARD_MAX_NAME_LENGTH);

    private static final String BOARD_NAME_NOT_UNIQUE_IN_TEAM_ERROR = "Board with name %s already exists in team %s.";

    // Team

    private static final String TEAM_NAME_NOT_UNIQUE_ERROR = "Team with name %s already exists.";

    // Null

    private static final String NULL_VALUE_ERROR = "Value cannot be null.";

    // Methods

    static void checkNull(Object object) {
        if (object == null) {
            throw new IllegalArgumentException(NULL_VALUE_ERROR);
        }
    }

    static void checkPersonName(String name) {
        checkNull(name);

        if (name.length() < PERSON_MIN_NAME_LENGTH || name.length() > PERSON_MAX_NAME_LENGTH) {
            throw new IllegalArgumentException(PERSON_NAME_LENGTH_ERROR);
        }

        if (Database.personExists(name)) {
            throw new IllegalArgumentException(String.format(PERSON_NAME_NOT_UNIQUE_ERROR, name));
        }
    }

    static void checkBoardName(String name) {
        checkNull(name);

        if (name.length() < BOARD_MIN_NAME_LENGTH || name.length() > BOARD_MAX_NAME_LENGTH) {
            throw new IllegalArgumentException(BOARD_NAME_LENGTH_ERROR);
        }
    }

    static void checkTeamName(String name) {
        checkNull(name);

        if (Database.teamExists(name)) {
            throw new IllegalArgumentException(String.format(TEAM_NAME_NOT_UNIQUE_ERROR, name));
        }
    }

    public static void checkIfBoardAlreadyExists(String boardName, String teamName){
        if(Database.boardExists(boardName,teamName)){
            throw new IllegalArgumentException(String.format(BOARD_NAME_NOT_UNIQUE_IN_TEAM_ERROR,boardName,teamName));
        }
    }

}
