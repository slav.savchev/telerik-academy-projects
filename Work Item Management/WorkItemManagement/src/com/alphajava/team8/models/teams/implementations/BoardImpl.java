package com.alphajava.team8.models.teams.implementations;

import com.alphajava.team8.models.teams.contracts.Board;
import com.alphajava.team8.models.workitems.contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;

public class BoardImpl implements Board {

    private String boardName;
    private List<String> activityHistory;
    private List<WorkItem> workItemList;

    public BoardImpl(String boardName){
        setBoardName(boardName);
        activityHistory = new ArrayList<>();
        workItemList = new ArrayList<>();
    }

    @Override
    public String getBoardName() {
        return this.boardName;
    }

    @Override
    public List<WorkItem> getWorkItems() {
        return new ArrayList<>(workItemList);
    }

    @Override
    public List<String> getActivityHistory() {
        return new ArrayList<>(activityHistory);
    }

    @Override
    public void addWorkItem(WorkItem item) {
        workItemList.add(item);
    }

    @Override
    public void addActivityToHistory(String activity) {
        activityHistory.add(activity);
    }

    private void setBoardName(String boardName) {
        ValidationHelper.checkBoardName(boardName);
        this.boardName = boardName;
    }
}
