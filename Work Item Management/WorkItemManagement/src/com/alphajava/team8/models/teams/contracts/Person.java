package com.alphajava.team8.models.teams.contracts;

import com.alphajava.team8.models.workitems.contracts.WorkItem;

import java.util.List;

public interface Person {
    List<WorkItem> getWorkItems();
    String getName();
    List<String> getActivityHistory();
    void assignItem(WorkItem item);
    void unAssignItem(WorkItem item);
    void addToActivityHistory(String activity);
}
