package com.alphajava.team8.models.teams.contracts;

import com.alphajava.team8.models.workitems.contracts.WorkItem;

import java.util.List;

public interface Board {

    String getBoardName();
    List<WorkItem> getWorkItems();
    List<String> getActivityHistory();
    void addWorkItem(WorkItem item);
    void addActivityToHistory(String activity);
}
