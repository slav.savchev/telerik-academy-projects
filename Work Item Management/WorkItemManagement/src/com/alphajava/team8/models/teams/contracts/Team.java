package com.alphajava.team8.models.teams.contracts;

import java.util.List;

public interface Team {
    String getTeamName();
    List<Person> getTeamMembers();
    List<Board> getTeamBoards();
    void addPersonToTeam(Person person);
    void addBoardToTeam(Board board);
    public boolean personIsInTeam(Person person);
    public boolean boardIsInTeam(Board board);
}
