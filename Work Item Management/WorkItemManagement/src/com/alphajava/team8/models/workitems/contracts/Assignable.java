package com.alphajava.team8.models.workitems.contracts;

import com.alphajava.team8.models.workitems.enums.PriorityType;
import com.alphajava.team8.models.teams.contracts.Person;

public interface Assignable {

    PriorityType getPriority();
    Person getAssignee();
    void setPriority(PriorityType priority);

}
