package com.alphajava.team8.models.workitems.enums;

public enum SizeType {
    LARGE,
    MEDIUM,
    SMALL;

    @Override
    public String toString() {
        switch (this){
            case LARGE:
                return "Large";
            case MEDIUM:
                return "Medium";
            case SMALL:
                return "Small";
        }
        throw new IllegalArgumentException("Invalid size");
    }

    public static boolean contains(String value) {
        for (SizeType type : values()) {
            if (type.toString().equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }

    public static String getValidSize() {
        StringBuilder validSizes = new StringBuilder("");
        for (SizeType size : values()) {
            validSizes.append(size.toString().concat(", "));
        }
        validSizes.delete(validSizes.length()-2, validSizes.length());

        return validSizes.toString();
    }
}
