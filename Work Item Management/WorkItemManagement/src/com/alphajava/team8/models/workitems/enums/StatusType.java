package com.alphajava.team8.models.workitems.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum StatusType {
    NEW,
    UNSCHEDULED,
    SCHEDULED,
    DONE,
    NOTDONE,
    INPROGRESS,
    ACTIVE,
    FIXED;

    @Override
    public String toString() {
        switch (this){
            case NEW:
                return "New";
            case UNSCHEDULED:
                return "Unscheduled";
            case SCHEDULED:
                return "Scheduled";
            case DONE:
                return "Done";
            case NOTDONE:
                return "NotDone";
            case INPROGRESS:
                return "InProgress";
            case ACTIVE:
                return "Active";
            case FIXED:
                return "Fixed";
        }
        throw new IllegalArgumentException("Invalid status type");
    }

    public static boolean contains(String value) {
        for (StatusType type : values()) {
            if (type.toString().equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }

    public static String getValidStatus() {
        StringBuilder validStatuses = new StringBuilder("");
        for (StatusType status : values()) {
            validStatuses.append(status.toString().concat(", "));
        }
        validStatuses.delete(validStatuses.length()-2, validStatuses.length());

        return validStatuses.toString();
    }
}
