package com.alphajava.team8.models.workitems.contracts;

import com.alphajava.team8.models.workitems.enums.PriorityType;
import com.alphajava.team8.models.workitems.enums.SeverityType;

import java.util.List;

public interface Bug extends WorkItem, Assignable {

    List<String> getStepsToReproduce();
    SeverityType getSeverity();
    public void addStepToReproduce(String explanation);
    void setSeverity(SeverityType severity);
}
