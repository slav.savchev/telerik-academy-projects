package com.alphajava.team8.models.workitems.enums;

public enum PriorityType {
    HIGH,
    MEDIUM,
    LOW;

    @Override
    public String toString() {
        switch (this){
            case HIGH:
                return "High";
            case MEDIUM:
                return "Medium";
            case LOW:
                return "Low";
        }
        throw new IllegalArgumentException("Invalid priority type");
    }

    public static boolean contains(String value) {
        for (PriorityType type : values()) {
            if (type.toString().equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }

    public static String getValidPriority() {
        StringBuilder validPriorities = new StringBuilder("");
        for (PriorityType priority : values()) {
            validPriorities.append(priority.toString().concat(", "));
        }
        validPriorities.delete(validPriorities.length()-2, validPriorities.length());

        return validPriorities.toString();
    }
}
