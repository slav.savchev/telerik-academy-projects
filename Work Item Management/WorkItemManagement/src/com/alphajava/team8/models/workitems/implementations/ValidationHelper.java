package com.alphajava.team8.models.workitems.implementations;

import com.alphajava.team8.models.workitems.contracts.WorkItem;
import com.alphajava.team8.models.workitems.enums.SeverityType;
import com.alphajava.team8.models.workitems.enums.StatusType;

class ValidationHelper {

    // WorkItem

    private static final int WORK_ITEM_TITLE_MIN_LENGTH = 10;
    private static final int WORK_ITEM_TITLE_MAX_LENGTH = 100;

    private static final int WORK_ITEM_DESCRIPTION_MIN_LENGTH = 10;
    private static final int WORK_ITEM_DESCRIPTION_MAX_LENGTH = 500;

    private static final String  WORK_ITEM_TITLE_LENGTH_ERROR = String.format(
            "Work item title length must be between %d and %d symbols long.",
            WORK_ITEM_TITLE_MIN_LENGTH, WORK_ITEM_TITLE_MAX_LENGTH);

    private static final String  WORK_ITEM_DESCRIPTION_LENGTH_ERROR = String.format(
            "Work item description length must be between %d and %d symbols long.",
            WORK_ITEM_DESCRIPTION_MIN_LENGTH, WORK_ITEM_DESCRIPTION_MAX_LENGTH);

    private static final String NULL_VALUE_ERROR = "Value cannot be null.";

    //Bug
    private static final String BUG_STATUS_ERROR = "Invalid status type! Acceptable bug status: Active, Fixed.";

    //Story
    private static final String STORY_STATUS_ERROR = "Invalid status type! Acceptable story status: NotDone, InProgress, Done.";

    //Feedback
    private static final int FEEDBACK_MIN_RATING = 1;
    private static final int FEEDBACK_MAX_RATING = 5;

    private static final String FEEDBACK_STATUS_ERROR = "Invalid status type! Acceptable feedback status: New, Unscheduled, Scheduled, Done.";
    private static final String FEEDBACK_RATING_ERROR = "Rating must be between %d and %d stars";

    //Method

    static void checkTitle(String title) {
        checkNull(title);
        checkValueInInterval(
                title.length(),
                WORK_ITEM_TITLE_MIN_LENGTH, WORK_ITEM_TITLE_MAX_LENGTH,
                WORK_ITEM_TITLE_LENGTH_ERROR);
    }

    static void checkDescription(String description) {
        checkNull(description);
        checkValueInInterval(
                description.length(),
                WORK_ITEM_DESCRIPTION_MIN_LENGTH, WORK_ITEM_DESCRIPTION_MAX_LENGTH,
                WORK_ITEM_DESCRIPTION_LENGTH_ERROR);
    }

    static void checkValueInInterval(int value, int min, int max, String errorMessage) {
        if (value < min || value > max) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    static void checkNull(Object object) {
        if (object == null) {
            throw new IllegalArgumentException(NULL_VALUE_ERROR);
        }
    }

    static void ratingValidation(int rating) {
        if(rating< FEEDBACK_MIN_RATING ||rating> FEEDBACK_MAX_RATING){
            throw new IllegalArgumentException(String.format(FEEDBACK_RATING_ERROR, FEEDBACK_MIN_RATING, FEEDBACK_MAX_RATING));
        }
    }

    static void checkStatus(WorkItem item, StatusType status) {
        if (item.getType().equals("Bug") && !(status.equals(StatusType.ACTIVE) || status.equals(StatusType.FIXED))) {
            throw new IllegalArgumentException(BUG_STATUS_ERROR);
        }

        if (item.getType().equals("Story") && !(status.equals(StatusType.NOTDONE)
                || status.equals(StatusType.INPROGRESS) || status.equals(StatusType.DONE))) {

            throw new IllegalArgumentException(STORY_STATUS_ERROR);
        }

        if (item.getType().equals("Feedback") && !(status.equals(StatusType.NEW)
                || status.equals(StatusType.SCHEDULED) || status.equals(StatusType.UNSCHEDULED) || status.equals(StatusType.DONE))) {

            throw new IllegalArgumentException(FEEDBACK_STATUS_ERROR);
        }
    }

}
