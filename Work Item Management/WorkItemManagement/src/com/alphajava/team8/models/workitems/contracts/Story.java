package com.alphajava.team8.models.workitems.contracts;

import com.alphajava.team8.models.workitems.enums.SizeType;

public interface Story extends WorkItem, Assignable {

    SizeType getSize();
    void setSize(SizeType size);

}
