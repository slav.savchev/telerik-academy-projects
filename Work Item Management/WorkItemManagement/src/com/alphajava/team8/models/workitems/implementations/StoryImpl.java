package com.alphajava.team8.models.workitems.implementations;

import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.workitems.contracts.Story;
import com.alphajava.team8.models.workitems.enums.PriorityType;
import com.alphajava.team8.models.workitems.enums.SizeType;
import com.alphajava.team8.models.workitems.enums.StatusType;

public class StoryImpl extends WorkItemImpl implements Story {

    private PriorityType priority;
    private SizeType size;
    private Person assignee;

    public StoryImpl(String title, StatusType status, PriorityType priority, SizeType size, Person assignee, String description) {
        super(title, status, description);
        setPriority(priority);
        setSize(size);
        setAssignee(assignee);
    }

    @Override
    public PriorityType getPriority() {
        return priority;
    }

    @Override
    public SizeType getSize() {
        return size;
    }

    @Override
    public Person getAssignee() {
        return assignee;
    }

    public void setPriority(PriorityType priority) {
        ValidationHelper.checkNull(priority);
        this.priority = priority;
    }

    public void setSize(SizeType size) {
        ValidationHelper.checkNull(size);
        this.size = size;
    }

    private void setAssignee(Person assignee) {
        ValidationHelper.checkNull(assignee);
        this.assignee = assignee;
    }

    public String getType() {
        return "Story";
    }

    protected String additionalInfo() {
        return String.format(", Priority: %s, Size: %s, Assignee: %s", getPriority(), getSize(), getAssignee().getName());
    }

}
