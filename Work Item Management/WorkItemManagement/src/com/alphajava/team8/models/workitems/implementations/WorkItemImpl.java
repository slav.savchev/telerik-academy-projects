package com.alphajava.team8.models.workitems.implementations;

import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.workitems.enums.StatusType;
import com.alphajava.team8.models.workitems.contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;

public abstract class WorkItemImpl implements WorkItem {

    private static int idCounter;

    private int id;
    private String title;
    private String description;
    private StatusType status;
    private List<String> comments;
    private List<String> history;

    WorkItemImpl(String title, StatusType status, String description){
        comments = new ArrayList<>();
        history = new ArrayList<>();
        setTitle(title);
        setStatus(status);
        setDescription(description);
        setId();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public StatusType getStatus() {
        return status;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public List<String> getComments() {
        return comments;
    }

    @Override
    public List<String> getHistory() {
        return new ArrayList<>(history);
    }

    public void addComment(String comment, Person author) {
        ValidationHelper.checkNull(comment);
        ValidationHelper.checkNull(author);
        String toBeAdded = comment + " - author: " + author.getName();
        comments.add(toBeAdded);
    }

    public void addActivityToHistory(String activity) {
        ValidationHelper.checkNull(activity);
        history.add(activity);
    }

    private void setId() {
        this.id = ++idCounter;
    }

    private void setTitle(String title) {
        ValidationHelper.checkTitle(title);
        this.title = title;
    }

    public void setStatus(StatusType status) {
        ValidationHelper.checkStatus(this, status);
        this.status = status;
    }

    private void setDescription(String description) {
        ValidationHelper.checkDescription(description);
        this.description = description;
    }

    public abstract String getType();
    protected abstract String additionalInfo();

    @Override
    public String toString() {
        return String.format(
                "WorkItem ID: %d -----\n"
                        + "  WorkItem type: %s, Title: %s\n"
                        + "  Status: %s%s\n"
                        + "  Description:\n  %s",
                getId(), getType(), getTitle(), getStatus(), additionalInfo(), getDescription());
    }
}
