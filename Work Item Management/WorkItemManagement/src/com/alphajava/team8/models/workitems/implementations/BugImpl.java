package com.alphajava.team8.models.workitems.implementations;

import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.workitems.contracts.Bug;
import com.alphajava.team8.models.workitems.enums.*;

import java.util.ArrayList;
import java.util.List;

public class BugImpl extends WorkItemImpl implements Bug {

    private List<String> stepsToReproduce;
    private PriorityType priority;
    private SeverityType severity;
    private Person assignee;

    public BugImpl(String title, StatusType status, String description, PriorityType priority, SeverityType severity, Person assignee) {
        super(title, status, description);
        stepsToReproduce = new ArrayList<>();
        setPriority(priority);
        setSeverity(severity);
        setAssignee(assignee);

    }

    @Override
    public List<String> getStepsToReproduce() {
        return stepsToReproduce;
    }

    @Override
    public SeverityType getSeverity() {
        return severity;
    }

    @Override
    public PriorityType getPriority() {
        return priority;
    }

    @Override
    public Person getAssignee() {
        return assignee;
    }

    public void setPriority(PriorityType priority) {
        ValidationHelper.checkNull(priority);
        this.priority = priority;
    }

    public void setSeverity(SeverityType severity) {
        ValidationHelper.checkNull(severity);
        this.severity = severity;
    }

    public void addStepToReproduce(String explanation) {
        ValidationHelper.checkNull(explanation);
        stepsToReproduce.add(explanation);
    }

    private void setAssignee(Person assignee) {
        ValidationHelper.checkNull(assignee);
        this.assignee = assignee;
    }

    public String getType() {
        return "Bug";
    }

    protected String additionalInfo() {
        return String.format(", Priority: %s, Severity: %s, Assignee: %s", getPriority(), getSeverity(), getAssignee().getName());
    }
}
