package com.alphajava.team8.models.workitems.implementations;

import com.alphajava.team8.models.workitems.enums.StatusType;
import com.alphajava.team8.models.workitems.contracts.Feedback;

public class FeedbackImpl extends WorkItemImpl implements Feedback {


    private int rating;

    public FeedbackImpl(String title, StatusType status, int rating, String description){
        super(title, status, description);
        setRating(rating);
    }

    @Override
    public int getRating() {
        return this.rating;
    }

    public void setRating(int rating) {
        ValidationHelper.ratingValidation(rating);
        this.rating = rating;
    }

    public String getType() {
        return "Feedback";
    }

    protected String additionalInfo() {
        return String.format(", Rating: %d", getRating());
    }
}
