package com.alphajava.team8.models.workitems.contracts;

public interface Feedback extends WorkItem {

    int getRating();
    void setRating(int rating);
}
