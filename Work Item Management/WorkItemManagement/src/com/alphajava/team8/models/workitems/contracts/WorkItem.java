package com.alphajava.team8.models.workitems.contracts;

import com.alphajava.team8.models.teams.contracts.Person;
import com.alphajava.team8.models.workitems.enums.PriorityType;
import com.alphajava.team8.models.workitems.enums.SeverityType;
import com.alphajava.team8.models.workitems.enums.SizeType;
import com.alphajava.team8.models.workitems.enums.StatusType;

import java.util.List;

public interface WorkItem {

    int getId();
    String getTitle();
    StatusType getStatus();
    String getDescription();
    List<String> getComments();
    List<String> getHistory();
    void addComment(String comment, Person author);
    void setStatus(StatusType status);
    void addActivityToHistory(String activity);
    String getType();
}
