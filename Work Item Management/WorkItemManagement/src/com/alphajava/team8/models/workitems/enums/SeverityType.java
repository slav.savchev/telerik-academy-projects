package com.alphajava.team8.models.workitems.enums;

public enum SeverityType {
    CRITICAL,
    MAJOR,
    MINOR;

    @Override
    public String toString() {
        switch (this){
            case CRITICAL:
                return "Critical";
            case MAJOR:
                return "Major";
            case MINOR:
                return "Minor";
        }
        throw new IllegalArgumentException("Invalid severity type");
    }

    public static boolean contains(String value) {
        for (SeverityType type : values()) {
            if (type.toString().equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }

    public static String getValidSeverity() {
        StringBuilder validSeverities = new StringBuilder("");
        for (SeverityType severity : values()) {
            validSeverities.append(severity.toString().concat(", "));
        }
        validSeverities.delete(validSeverities.length()-2, validSeverities.length());

        return validSeverities.toString();
    }
}
