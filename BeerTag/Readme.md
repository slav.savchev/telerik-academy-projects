# Beer Tag Web Application

Here you will find information regarding the Web Application Assignment of Team 8.

This is the link for our Trello: https://trello.com/b/qIgRdrDb/beer-project-team-08

## Project description

Our goal is to design and implement a Restful Web Application for beer lovers alike.

## Functional details

BEER TAG enables users to manage all the beers that they have drank and want to drink. 
Each beer has detailed information about it from the ABV (alcohol by volume) to 
the style and description. Data is community driven and every registered beer lover 
can add new beers. Furthermore, BEER TAG allows users to rate a beers and see average 
rating from different users.

## General Information:

Each user has **name**, **email** and **profile picture** (optional).

Each beer has **name**, **description**, **origin country**, **brewery** that produces it, **style** (pre-defined),
**ABV**, **tags** (zero, one or many) and **picture**.

Application provides functionality based on the role of the user that is using it.
There are three types of users: `[GUEST]`, `[USER]` and `[ADMIN]`.

`[GUEST]` can browse all beers and their details. They can also filter 
by origin country, style, tags and sort by name, ABV and rating (using query params).

`[USER]` can create new beers, add a beer to their wishlist and dranklist as well as rate a beer. 
They can also edit and delete their own beers. Furhtermore, USERS can modify their personal information as well.

`[ADMINS]` have full control over all beers and all users.

## REST API

Here you will find documentation/information regarding the free-to-access `[REST]` 
endpoints of Web Application Assignment of Team 8.

This is a link to our **SWAGGER** documentation:  https://bit.ly/2Nj7uxJ

# Front-End:

## PUBLIC PART

The public part of the application is accessible without authentication. This includes 
the application start page, the user login and user registration forms, as well as list of all 
beers that have been added in the system. `[GUEST]` cannot see any user specific details, they can only 
browse the beers and see details of them.

## PRIVATE PART

`[USERS]` have the private part in the web application accessible after a successful login.
The web application provides them with UI to add/edit/delete (CRUD) beers. Each user can rate a beer. 

## ADMIN PART

ADMINS have administrative access to the system and permissions to administer all
entities e.g. to edit/delete **(CRUD)** users and other administrators or edit/delete beers and related data if they decide to.
