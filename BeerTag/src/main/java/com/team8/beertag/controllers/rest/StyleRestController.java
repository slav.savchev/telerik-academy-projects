package com.team8.beertag.controllers.rest;

import com.team8.beertag.exceptions.DuplicateEntityException;
import com.team8.beertag.exceptions.EntityNotFoundException;
import com.team8.beertag.models.Style;
import com.team8.beertag.services.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/styles")
public class StyleRestController {

    private StyleService styleService;

    @Autowired
    public StyleRestController(StyleService styleService) {
        this.styleService = styleService;
    }

    @GetMapping
    public List<Style> getAllStyles() {
        return styleService.getAllStyles();
    }

    @GetMapping("/{id}")
    public Style getStyleByID(@PathVariable int id) {
        try {
            return styleService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Style createStyle(@RequestBody @Valid Style style) {
        try {
            return styleService.createStyle(style);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Style updateStyle(@PathVariable int id, @RequestBody @Valid Style style) {
        try {
            style.setStyleId(id);
            return styleService.updateStyle(style);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @DeleteMapping("/{id}")
    public void deleteStyle(@PathVariable int id) {
        try {
            styleService.deleteStyle(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
