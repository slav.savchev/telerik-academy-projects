package com.team8.beertag.controllers;

import com.team8.beertag.models.*;
import com.team8.beertag.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class BeerController {

    private BeerService beerService;
    private StyleService styleService;
    private DtoMapper dtoMapper;
    private BreweryService breweryService;
    private CountryService countryService;
    private StorageService storageService;
    private TagService tagService;
    private UserService userService;

    @Autowired
    public BeerController(BeerService beerService, StyleService styleService, DtoMapper dtoMapper, BreweryService breweryService, CountryService countryService, StorageService storageService, TagService tagService, UserService userService){
        this.beerService=beerService;
        this.styleService=styleService;
        this.dtoMapper=dtoMapper;
        this.breweryService=breweryService;
        this.countryService=countryService;
        this.storageService=storageService;
        this.tagService = tagService;
        this.userService = userService;
    }

    @GetMapping("/beers")
    public String showBeers(Model model,@RequestParam (required = false) String name,
                            @RequestParam (required = false) String abv,
                            @RequestParam (required = false) String style,
                            @RequestParam (required = false) String brewery,
                            @RequestParam (required = false) String country,
                            @RequestParam (required = false) String tag,
                            @RequestParam (required = false) String sort,
                            @RequestParam (defaultValue = "0") int page){

        Map<Integer,String> flagMapMVC = new HashMap<>();
        if(name!=null){
            flagMapMVC.put(1,name);
        }
        if (abv!=null){
            flagMapMVC.put(2,abv);
        }
        if(style!=null){
            flagMapMVC.put(3,style);
        }
        if(brewery!=null){
            flagMapMVC.put(4,brewery);
        }
        if(country!=null){
            flagMapMVC.put(5,country);
        }
        if(tag!=null){
            flagMapMVC.put(10,tag);
        }

        if(sort!=null){
            switch (sort) {
                case "name":
                    flagMapMVC.put(6, "name");
                    break;
                case "abv":
                    flagMapMVC.put(6, "abv");
                    break;
                case "style":
                    flagMapMVC.put(6, "style");
                    break;
                case "brewery":
                    flagMapMVC.put(6, "brewery");
                    break;
                case "country":
                    flagMapMVC.put(6, "country");
                    break;
                case "rating":
                    flagMapMVC.put(6, "rating");
                    break;
            }
        }

        model.addAttribute("beers",beerService.getAllBeers(flagMapMVC));
        model.addAttribute("popular_styles", styleService.getPopularStyles());
        model.addAttribute("displayBeers", beerService.getAllBeers(flagMapMVC));
         List<List<DisplayBeerDto>> pageList;
        try{
            pageList = beerService.getPages(flagMapMVC);
        } catch (IndexOutOfBoundsException e){
            pageList = new ArrayList<>();
        }
        if(pageList.size()!=0){
            model.addAttribute("numOfPages",pageList.size()-1);
        } else {
            model.addAttribute("numOfPages",pageList.size());
        }

        List<DisplayBeerDto> requestedPage;
        if(pageList.size()!=0){
            requestedPage = pageList.get(page);
        } else {
            requestedPage = new ArrayList<>();
        }
        model.addAttribute("pages", requestedPage);
        model.addAttribute("popular_tags", tagService.getPopularTags());
        model.addAttribute("latest_beers", dtoMapper.toDisplayBeerDto(beerService.getNewestBeers()));
        model.addAttribute("username_id_map", userService.usernameToIdMap());
        return "../static/products";
    }

    @GetMapping("/beers/{id}")
    public String getBeerById(@PathVariable int id, Model model, Authentication authentication){

        Beer beer = beerService.getBeerById(id);

        boolean isAdmin = false;
        boolean isCreator = false;
        int userId = 0;

        if(authentication != null) {
            GrantedAuthority authority = (GrantedAuthority) authentication.getAuthorities().toArray()[0];
            String asString = authority.getAuthority();
            if (asString.equals("ROLE_ADMIN")) {
                isAdmin = true;
            }
            if (userService.usernameToIdMap().get(authentication.getName()) == beer.getCreatorId()) {
                isCreator = true;
            }
            userId = userService.usernameToIdMap().get(authentication.getName());
        }

        DisplayBeerDto displayBeerDto = dtoMapper.toDisplayBeerDto(beer);
        model.addAttribute("displayBeer", displayBeerDto);
        model.addAttribute("similar_beers", dtoMapper.toDisplayBeerDto(beerService.getSimilarBeers(beer)));
        model.addAttribute("username_id_map", userService.usernameToIdMap());
        model.addAttribute("is_admin", isAdmin);
        model.addAttribute("is_creator", isCreator);
        model.addAttribute("user_id", userId);
        model.addAttribute("user_rating", new UserRating());

        return "../static/single-product";
    }

//    @PostMapping("/beers/{id}/rating")
//    public String addRatingToBeer(@ModelAttribute("emptyRating") UserRating rating,@PathVariable int id){
//
//        beerService.addRating(rating.getRating(),id);


    @PostMapping("/beers/{id}/rating/{user_id}")
    public String addRatingToBeer(@ModelAttribute("user_rating") UserRating rating, @PathVariable int id, @PathVariable int user_id){

        if(user_id == 0) {
            return "redirect:/login/";
        }

        rating.setBeer(beerService.getBeerById(id));
        rating.setUser(userService.getUserById(user_id));
        beerService.addRating(rating);

        return String.format("redirect:/beers/%d",id);
    }

    @ModelAttribute("styles")
    public List<Style> populateStyles() {
        return styleService.getAllStyles();
    }

    @ModelAttribute("breweries")
    public List<Brewery> populateBreweries() {
        return breweryService.getAllBreweries();
    }

    @ModelAttribute("countries")
    public List<Country> populateCountries() {
        return countryService.getAllCountries();
    }

    @GetMapping("/beers/new")
    public String showNewBeerForm(Model model) {
        model.addAttribute("beer", new CreateBeerDto());
        model.addAttribute("username_id_map", userService.usernameToIdMap());
        return "../static/create-beer";
    }

    @PostMapping("/beers/new")
    public String createBeer(@Valid @ModelAttribute("beer") CreateBeerDto createBeerDto, BindingResult errors, Principal principal) {
        if (errors.hasErrors()) {
            return "../static/create-beer";
        }

      //  Beer beer = dtoMapper.fromCreateBeerDto(createBeerDto,file);
        createBeerDto.setCreatorId(userService.usernameToIdMap().get(principal.getName()));
        Beer beer = dtoMapper.fromCreateBeerDto(createBeerDto);
        beerService.createBeer(beer);
        return "redirect:/beers";
    }

    @GetMapping("beers/{id}/update")
    public String showUpdateBeerForm(@PathVariable int id, Model model){
        DisplayBeerDto displayBeerDto = dtoMapper.toDisplayBeerDto(beerService.getBeerById(id));
        model.addAttribute("oldBeer", displayBeerDto);
        model.addAttribute("emptyBeer", new CreateBeerDto());
        model.addAttribute("username_id_map", userService.usernameToIdMap());

        return "../static/update-beer";
    }

    @PutMapping("beers/{id}")
    public String updateBeer(@ModelAttribute("emptyBeer") CreateBeerDto createBeerDto, @PathVariable int id, BindingResult errors){
        if (errors.hasErrors()) {
            return "../static/update-beer";
        }

        Beer oldBeer = beerService.getBeerById(id);

        byte[] image;
        if(createBeerDto.getImage().getOriginalFilename().equals("")){
            image = oldBeer.getImage();
        } else {
            image = new byte[0];
        }
        if(createBeerDto.getTags().equalsIgnoreCase("")){
            List<Tag> tagList = oldBeer.getTags();
            String[] tagNameArr = new String[tagList.size()];
            for (int i = 0; i <tagList.size() ; i++) {
                tagNameArr[i] = tagList.get(i).getName();
            }

            createBeerDto.setTags(String.join(", ", tagNameArr));
        }
        if(createBeerDto.getDescription().equalsIgnoreCase("")){
            createBeerDto.setDescription(oldBeer.getDescription());
        }
        if(createBeerDto.getName().equalsIgnoreCase("")){
            createBeerDto.setName(oldBeer.getName());
        }
        if(createBeerDto.getABV()==0){
            createBeerDto.setABV(oldBeer.getABV());
        }

        createBeerDto.setCreatorId(oldBeer.getCreatorId());
        Beer beer = dtoMapper.fromCreateBeerDto(createBeerDto);
        beer.setId(id);
        if (image.length > 0) {
            beer.setImage(image);
        }

        beerService.updateBeer(beer);

        return String.format("redirect:/beers/%d",id);
    }

/*
    @GetMapping("/beers/new")
    public String showNewBeerForm(Model model){
        model.addAttribute("beer",new CreateBeerDto());
        model.addAttribute("styles", styleService.getAllStyles());
        model.addAttribute("breweries",breweryService.getAllBreweries());
        model.addAttribute("countries",countryService.getAllCountries());
        return "beer";
    }

    @PostMapping("/beers/new")
    public String createBeer(@Valid @ModelAttribute("beer") CreateBeerDto createBeerDto, BindingResult errors, Model model){
        if(errors.hasErrors()){
            return "beer";
        }
        Beer beer = dtoMapper.fromCreateBeerDto(createBeerDto);
        beerService.createBeer(beer);
        return "redirect:/beers";
    }
*/

    @DeleteMapping("/beers/{id}")
    public String deleteBeer(@PathVariable int id){
        beerService.deleteBeer(id);
        return "redirect:/beers";
    }


    //TODO UPDATE MAPPING
}
