package com.team8.beertag.controllers;

import com.team8.beertag.models.*;
import com.team8.beertag.services.StorageService;
import com.team8.beertag.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import com.team8.beertag.services.BeerService;
import com.team8.beertag.services.UserService;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Controller
public class UserController {

    private UserService userService;
    private BeerService beerService;
    private DtoMapper dtoMapper;
    private StorageService storageService;

    @Autowired
    public UserController(UserService userService, BeerService beerService, DtoMapper dtoMapper, StorageService storageService) {
        this.userService = userService;
        this.beerService = beerService;
        this.dtoMapper = dtoMapper;
        this.storageService = storageService;
    }


    @GetMapping("/community")
    public String getCommunity(@RequestParam(defaultValue = "0") int page, Model model) {

        List<UserDto> allUsers = dtoMapper.toUserDtoList(userService.getAllUsers());


        List<List<UserDto>> pageList;

        try {
            pageList = userService.getUserPages(allUsers);
        } catch (IndexOutOfBoundsException e) {
            pageList = new ArrayList<>();
        }
        if (pageList.size() != 0) {
            model.addAttribute("numOfPages", pageList.size() - 1);
        } else {
            model.addAttribute("numOfPages", pageList.size());
        }

        List<UserDto> requestedPage;
        if (pageList.size() != 0) {
            requestedPage = pageList.get(page);
        } else {
            requestedPage = new ArrayList<>();
        }

        model.addAttribute("allUsers", userService.getAllUsers());
        model.addAttribute("pages", requestedPage);
        model.addAttribute("username_id_map", userService.usernameToIdMap());

        return "../static/community";
    }

    @GetMapping("/users/{id}/wishlist")
    public String getUserWishlist(@PathVariable int id, Model model, @RequestParam(defaultValue = "0") int page) {

        List<DisplayBeerDto> userWishlist = dtoMapper.toDisplayBeerDto(userService.getWishListBeers(id));
        List<List<DisplayBeerDto>> pageList;

        try {
            pageList = userService.listToPageListConverter(userWishlist);
        } catch (IndexOutOfBoundsException e) {
            pageList = new ArrayList<>();
        }
        if (pageList.size() != 0) {
            model.addAttribute("numOfPages", pageList.size() - 1);
        } else {
            model.addAttribute("numOfPages", pageList.size());
        }

        List<DisplayBeerDto> requestedPage;
        if (pageList.size() != 0) {
            requestedPage = pageList.get(page);
        } else {
            requestedPage = new ArrayList<>();
        }

        model.addAttribute("pages", requestedPage);
        model.addAttribute("userId", id);
        model.addAttribute("username_id_map", userService.usernameToIdMap());

        return "../static/user-wishlist";
    }

    @GetMapping("/users/{id}/dranklist")
    public String getUserDranklist(@PathVariable int id, Model model, @RequestParam(defaultValue = "0") int page){
        List<DisplayBeerDto> userDranklist = dtoMapper.toDisplayBeerDto(userService.getDrankListBeers(id));
        List<List<DisplayBeerDto>> pageList;

        try {
            pageList = userService.listToPageListConverter(userDranklist);
        } catch (IndexOutOfBoundsException e) {
            pageList = new ArrayList<>();
        }
        if (pageList.size() != 0) {
            model.addAttribute("numOfPages", pageList.size() - 1);
        } else {
            model.addAttribute("numOfPages", pageList.size());
        }

        List<DisplayBeerDto> requestedPage;
        if (pageList.size() != 0) {
            requestedPage = pageList.get(page);
        } else {
            requestedPage = new ArrayList<>();
        }

        model.addAttribute("pages", requestedPage);
        model.addAttribute("userId", id);
        model.addAttribute("username_id_map", userService.usernameToIdMap());

        return "../static/user-dranklist";
    }

    @GetMapping("/users/{id}/update")
    public String showUserUpdateForm(@PathVariable int id, Model model){
        UserDto userDto = dtoMapper.toUserDto(userService.getUserById(id));
        model.addAttribute("oldUser",userDto);
        model.addAttribute("emptyUser", new CreateUserDto());

        return "../static/update-user";
    }


    @PostMapping("/users/{id}/picupdate")
    public String updateUserPic(@ModelAttribute("emptyProfilePic") UserProfileImage profileImage,@PathVariable int id){
        User user = userService.getUserById(id);

        byte[] image = storageService.toByteArray(profileImage.getImage());

        user.setImage(image);

        userService.constructUser(user);

        return String.format("redirect:/users/%d",id);
    }

    @PutMapping("/users/{id}")
    public String updateUser(@ModelAttribute("emptyUser") CreateUserDto createUserDto, @PathVariable int id, BindingResult errors){
        if (errors.hasErrors()) {
            return "../static/update-user";
        }

        User oldUser = userService.getUserById(id);

        createUserDto.setUsername(oldUser.getUsername());
        createUserDto.setPassword(oldUser.getPassword());
        createUserDto.setPasswordConfirmation(oldUser.getUsername());

        User user = dtoMapper.fromCreateUserDto(createUserDto);
        user.setId(id);
        try {
            user.setImage(Base64.getDecoder().decode(Constants.DEFAULT_USER_PROFILE_PICTURE.getBytes("UTF-8")));
        } catch (UnsupportedEncodingException e){
            e.printStackTrace();
        }
        userService.constructUser(user);

        return String.format("redirect:/users/%d",id);
    }


    @GetMapping("/users/{id}")
    public String showUser(@PathVariable int id, Model model, Principal principal) {
        if (id == 0) {
            id = userService.usernameToIdMap().get(principal.getName());
            return "redirect:/users/".concat(String.valueOf(id));
        }

        User user = userService.getUserById(id);
        model.addAttribute("user", dtoMapper.toUserDto(user));
        model.addAttribute("suggested_beers", dtoMapper.toDisplayBeerDto(beerService.getSuggestedBeers(user)));
        model.addAttribute("username_id_map", userService.usernameToIdMap());
        model.addAttribute("emptyProfilePic", new UserProfileImage());

        return "../static/user-profile";
    }

    @DeleteMapping("/users/{id}")
    public String deleteBeer(@PathVariable int id) {
        userService.deleteUser(id);
        return "redirect:/users";
    }

    @PostMapping("users/{id}/wishlist/{beerId}")
    public String addBeerToWishList(@PathVariable int id, @PathVariable int beerId){
        userService.addBeerToWishList(id,beerId);
        return "redirect:/users/".concat(String.valueOf(id)).concat("/wishlist");
    }

    @PostMapping("users/{id}/dranklist/{beerId}")
    public String addBeerToDrankList(@PathVariable int id, @PathVariable int beerId){
        userService.addBeerToDrankList(id,beerId);
        return "redirect:/users/".concat(String.valueOf(id)).concat("/dranklist");
    }

}