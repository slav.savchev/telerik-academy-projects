package com.team8.beertag.controllers.rest;

import com.team8.beertag.exceptions.DuplicateEntityException;
import com.team8.beertag.exceptions.EntityNotFoundException;
import com.team8.beertag.models.Beer;
import com.team8.beertag.models.CreateBeerDto;
import com.team8.beertag.models.DisplayBeerDto;
import com.team8.beertag.models.DtoMapper;
import com.team8.beertag.services.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/api/beers")
public class BeerRestController {

    private BeerService beerService;
    private DtoMapper dtoMapper;


    public BeerRestController(){
    }

    @Autowired
    public BeerRestController(BeerService beerService, DtoMapper dtoMapper) {
        this.beerService = beerService;
        this.dtoMapper = dtoMapper;
    }

    @GetMapping
    public List<DisplayBeerDto> getBeers(@RequestParam (required = false) String name,
                                              @RequestParam (required = false) String abv,
                                              @RequestParam (required = false) String style,
                                              @RequestParam (required = false) String brewery,
                                              @RequestParam (required = false) String country,
                                              @RequestParam (required = false) String sort) {
        Map<Integer,String> flagMap = new HashMap<>();
        if(name!=null){
            flagMap.put(1,name);
        }
        if (abv!=null){
            flagMap.put(2,abv);
        }
        if(style!=null){
            flagMap.put(3,style);
        }
        if(brewery!=null){
            flagMap.put(4,brewery);
        }
        if(country!=null){
            flagMap.put(5,country);
        }
        if(sort!=null){
            switch (sort) {
                case "name":
                    flagMap.put(6, "name");
                    break;
                case "abv":
                    flagMap.put(6, "abv");
                    break;
                case "style":
                    flagMap.put(6, "style");
                    break;
                case "brewery":
                    flagMap.put(6, "brewery");
                    break;
                case "country":
                    flagMap.put(6, "country");
                    break;
                case "rating":
                    flagMap.put(6, "rating");
                    break;
            }
        }
        return dtoMapper.toDisplayBeerDto(beerService.getAllBeers(flagMap));
    }


    @GetMapping("/{id}")
    public DisplayBeerDto getBeerByID(@PathVariable int id) {
        try {
            return dtoMapper.toDisplayBeerDto(beerService.getBeerById(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @PostMapping
    public DisplayBeerDto createBeer(@RequestBody @Valid CreateBeerDto createBeerDto) {
        try {
            Beer newBeer = dtoMapper.fromCreateBeerDto(createBeerDto);
            return dtoMapper.toDisplayBeerDto(beerService.createBeer(newBeer));
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public DisplayBeerDto updateBeer(@PathVariable int id, @RequestBody @Valid CreateBeerDto createBeerDto) {
        try {
            getBeerByID(id);
            Beer updatedBeer = dtoMapper.fromCreateBeerDto(createBeerDto);
            updatedBeer.setId(id);
            return dtoMapper.toDisplayBeerDto(beerService.updateBeer(updatedBeer));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @DeleteMapping("/{id}")
    public void deleteBeer(@PathVariable int id) {
        try {
            beerService.deleteBeer(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}

