package com.team8.beertag.controllers.rest;

import com.team8.beertag.exceptions.DuplicateEntityException;
import com.team8.beertag.exceptions.EntityNotFoundException;
import com.team8.beertag.models.Country;
import com.team8.beertag.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/countries")
public class CountryRestController {

    private CountryService countryService;

    public CountryRestController(){}

    @Autowired
    public CountryRestController(CountryService countryService) {
        this.countryService = countryService;
    }

    @GetMapping
    public List<Country> getAllCountries() {
        return countryService.getAllCountries();
    }

    @GetMapping("/{id}")
    public Country getCountryByID(@PathVariable int id) {
        try {
            return countryService.getCountryByID(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Country createCountry(@RequestBody @Valid Country country) {
        try {
            return countryService.createCountry(country);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Country updateCountry(@PathVariable int id, @RequestBody @Valid Country country) {
        try {
            country.setCountryId(id);
            return countryService.updateCountry(country);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @DeleteMapping("/{id}")
    public void deleteCountry(@PathVariable int id) {
        try {
            countryService.deleteCountry(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
