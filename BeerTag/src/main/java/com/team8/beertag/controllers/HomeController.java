package com.team8.beertag.controllers;

import com.team8.beertag.models.Beer;
import com.team8.beertag.models.DisplayBeerDto;
import com.team8.beertag.models.DtoMapper;
import com.team8.beertag.services.BeerService;
import com.team8.beertag.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.security.Principal;
import java.util.HashMap;


@Controller
public class HomeController {

    private BeerService beerService;
    private DtoMapper dtoMapper;
    private UserService userService;

    public HomeController(BeerService beerService, DtoMapper dtoMapper, UserService userService) {
        this.beerService = beerService;
        this.dtoMapper = dtoMapper;
        this.userService = userService;
    }

    @GetMapping("/")
    public String showHomePage(Model model){
        model.addAttribute("newest_beers", dtoMapper.toDisplayBeerDto(beerService.getNewestBeers()));
        model.addAttribute("lucky_beer_id", beerService.getRandomBeerId());
        model.addAttribute("username_id_map", userService.usernameToIdMap());
        return "../static/index";
    }

    @GetMapping("/{id}")
    public String getBeerById(@PathVariable int id, Model model){
        Beer beer = beerService.getBeerById(id);
        DisplayBeerDto displayBeerDto = dtoMapper.toDisplayBeerDto(beer);
        model.addAttribute("displayBeer", displayBeerDto);
        return "../static/single-product";
    }

    @GetMapping("/admin")
    public String showAdminPage() {return "admin";}
}
