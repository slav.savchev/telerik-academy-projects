package com.team8.beertag.controllers.rest;


import com.team8.beertag.exceptions.DuplicateEntityException;
import com.team8.beertag.exceptions.EntityNotFoundException;
import com.team8.beertag.models.Brewery;
import com.team8.beertag.services.BreweryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/breweries")
public class BreweryRestController {

    private BreweryService breweryService;

    public BreweryRestController(){}

    @Autowired
    public BreweryRestController(BreweryService breweryService) {
        this.breweryService = breweryService;
    }

    @GetMapping
    public List<Brewery> getAllBreweries(){
        return breweryService.getAllBreweries();
    }

    @GetMapping("/{id}")
    public Brewery getBreweryById(@PathVariable int id){
        try{
            return breweryService.getBreweryById(id);
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Brewery createBrewery(@RequestBody @Valid Brewery brewery){
        try{
            return breweryService.createBrewery(brewery);
        } catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Brewery updateBrewery(@PathVariable int id, @RequestBody Brewery brewery){
        try{
            brewery.setId(id);
            return breweryService.updateBrewery(brewery);
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteBrewery(@PathVariable int id){
        try{
            breweryService.deleteBrewery(id);
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
