package com.team8.beertag.controllers.rest;

import com.team8.beertag.exceptions.DuplicateEntityException;
import com.team8.beertag.exceptions.EntityNotFoundException;
import com.team8.beertag.models.Beer;
import com.team8.beertag.models.DisplayBeerDto;
import com.team8.beertag.models.DtoMapper;
import com.team8.beertag.models.User;
import com.team8.beertag.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private UserService services;
    private DtoMapper dtoMapper;

    @Autowired
    public UserRestController(UserService services, DtoMapper dtoMapper) {
        this.dtoMapper = dtoMapper;
        this.services = services;
    }

    @GetMapping
    public List<User> getAllUsers(){
        return services.getAllUsers();
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable int id){
        try {
            return services.getUserById(id);
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/wishlist")
    public List<DisplayBeerDto> getWishListBeers(@PathVariable int id){
        return dtoMapper.toDisplayBeerDto(services.getWishListBeers(id));
    }

    @GetMapping("/{id}/dranklist")
    public List<Beer> getDrankListBeers(@PathVariable int id){
        return services.getDrankListBeers(id);
    }

    @PostMapping
    public User createUser(@RequestBody @Valid User user){
        try{
            return services.createUser(user);
        } catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }

    @PostMapping("/{id}/wishlist/{beerId}")
    public List<Beer> addBeerToWishList(@PathVariable int id, @PathVariable int beerId){
        return services.addBeerToWishList(id,beerId);
    }

    @PostMapping("/{id}/dranklist/{beerId}")
    public List<Beer> addBeerToDrankList(@PathVariable int id, @PathVariable int beerId){
        return services.addBeerToDrankList(id,beerId);
    }

    @PutMapping("/{id}")
    public User updateUser(@PathVariable int id,@RequestBody @Valid User user){
        try {
            user.setId(id);
            return services.updateUser(user);
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable int id){
        try {
            services.deleteUser(id);
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}/wishlist/{beerId}")
    public void deleteBeerFromWishList(@PathVariable int id, @PathVariable Integer beerId){
        services.deleteBeerFromWishList(id,beerId);
    }

    @DeleteMapping("/{id}/dranklist/{beerId}")
    public void deleteBeerFromDrankList(@PathVariable int id, @PathVariable Integer beerId){
        services.deleteBeerFromDrankList(id,beerId);
    }
}
