package com.team8.beertag.controllers.rest;


import com.team8.beertag.exceptions.DuplicateEntityException;
import com.team8.beertag.exceptions.EntityNotFoundException;
import com.team8.beertag.models.Tag;
import com.team8.beertag.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/tags")
public class TagRestController {

    private TagService tagService;

    public TagRestController(){}

    @Autowired
    public TagRestController(TagService tagService){
        this.tagService=tagService;
    }

    @GetMapping
    public List<Tag> getAllTags(){
        return tagService.getAllTags();
    }

    @GetMapping("/{id}")
    public Tag getTagById(@PathVariable int id){
        try{
            return tagService.getTagById(id);
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Tag createTag(@RequestBody Tag tag){
        try {
            return tagService.createTag(tag);
        } catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Tag updateTag(@PathVariable int id, @RequestBody Tag tag){
        try {
            tag.setId(id);
            return tagService.updateTag(tag);
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteTag(@PathVariable int id){
        try{
            tagService.deleteTag(id);
        } catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
