package com.team8.beertag.services;

import com.team8.beertag.models.Beer;
import com.team8.beertag.models.DisplayBeerDto;
import com.team8.beertag.models.User;
import com.team8.beertag.models.UserRating;

import java.util.List;
import java.util.Map;

public interface BeerService {
    Beer createBeer(Beer beer);

    List<Beer> getAllBeers(Map<Integer,String> flagMap);

    Beer getBeerById(int id);

    boolean checkBeerExists(String name);

    Beer updateBeer(Beer beer);

    void deleteBeer(int id);

    List<Beer> getNewestBeers();

    int getRandomBeerId();

    List<List<DisplayBeerDto>> getPages(Map<Integer,String> flagMap);

    List<Beer> getSimilarBeers(Beer beer);

    List<Beer> getSuggestedBeers(User user);

    int getNumberOfSameTags(Beer mainBeer, Beer otherBeer);

    void addRating(UserRating rating);
}
