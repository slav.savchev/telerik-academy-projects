package com.team8.beertag.services;

import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.nio.file.Path;
import java.util.stream.Stream;

public interface StorageService {


    byte[] toByteArray(MultipartFile file);

    String toBase64Image(byte[] bArray);

}
