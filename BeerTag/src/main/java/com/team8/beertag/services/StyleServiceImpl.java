package com.team8.beertag.services;

import com.team8.beertag.exceptions.DuplicateEntityException;
import com.team8.beertag.models.Style;
import com.team8.beertag.repositories.StyleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class StyleServiceImpl implements StyleService {

    private StyleRepository styleRepository;

    @Autowired
    public StyleServiceImpl(StyleRepository styleRepository){
        this.styleRepository=styleRepository;
    }

    @Override
    public Style getById(int id) {
        return styleRepository.getById(id);
    }

    @Override
    public List<Style> getAllStyles() {
        return styleRepository.getAllStyles();
    }

    @Override
    public Style createStyle(Style style) {
        if (checkStyleExists(style.getName())) {
            throw new DuplicateEntityException(String.format("Style '%s' already exists!", style.getName()));
        }

        return styleRepository.createStyle(style);
    }

    @Override
    public boolean checkStyleExists(String name) {
        return styleRepository.checkStyleExists(name);
    }

    @Override
    public Style updateStyle(Style style) {
        return styleRepository.updateStyle(style);
    }

    @Override
    public void deleteStyle(int id) {
        styleRepository.deleteStyle(id);
    }

    @Override
    public List<Style> getPopularStyles() {
        List<Style> usedStyles = styleRepository.getAllUsedStyles();
        Map<Style, Integer> styleCount = new HashMap<>();

        for (Style style : usedStyles) {
            styleCount.putIfAbsent(style, 0);
            styleCount.put(style, styleCount.get(style) + 1);
        }

        List<Style> popularStyles = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            popularStyles.add(getMaxAndRemoveIt(styleCount));
        }

        return popularStyles;
    }

    private Style getMaxAndRemoveIt(Map<Style, Integer> styleCount) {
        AtomicInteger max = new AtomicInteger(Integer.MIN_VALUE);
        styleCount.forEach((style, count) -> {
            if (count > max.get()) {
                max.set(count);
            }
        });

        AtomicReference<Style> topStyle = new AtomicReference<>(new Style());

        styleCount.forEach((style, count) -> {
            if(count == max.get()) {
                topStyle.set(style);
            }
        });

        styleCount.remove(topStyle.get());
        return topStyle.get();
    }
}
