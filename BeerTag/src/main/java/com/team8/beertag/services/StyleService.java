package com.team8.beertag.services;

import com.team8.beertag.models.Style;

import java.util.List;

public interface StyleService {

    Style getById(int id);

    List<Style> getAllStyles();

    Style createStyle(Style style);

    boolean checkStyleExists(String name);

    Style updateStyle(Style style);

    void deleteStyle(int id);

    List<Style> getPopularStyles();
}
