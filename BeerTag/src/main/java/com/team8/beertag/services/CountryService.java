package com.team8.beertag.services;

import com.team8.beertag.models.Country;

import java.util.List;

public interface CountryService {

    Country getCountryByID(int id);

    List<Country> getAllCountries();

    Country createCountry(Country country);

    boolean checkCountryExists(String name);

    Country updateCountry(Country country);

    void deleteCountry(int id);
}
