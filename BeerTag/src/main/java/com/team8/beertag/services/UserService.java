package com.team8.beertag.services;

import com.team8.beertag.models.Beer;
import com.team8.beertag.models.DisplayBeerDto;
import com.team8.beertag.models.User;
import com.team8.beertag.models.UserDto;

import java.util.List;
import java.util.Map;

public interface UserService {

    List<User> getAllUsers();

    User createUser(User user);

    User updateUser(User user);

    void deleteUser(int id);

    List<Beer> getWishListBeers(int id);

    List<Beer> getDrankListBeers(int id);

    User getUserById(int id);

    int getNextAvailableId();

    User constructUser(User user);

    boolean checkUserExists(String name);

    void deleteBeerFromWishList(int userId, int beerId);

    void deleteBeerFromDrankList(int userId, int beerId);

    List<Beer> addBeerToWishList(int userId, int beerId);

    List<Beer> addBeerToDrankList(int userId, int beerId);

    List<List<UserDto>> getUserPages(List<UserDto> userDtoList);

    List<List<DisplayBeerDto>> listToPageListConverter(List<DisplayBeerDto> allBeers);

    Map<String, Integer> usernameToIdMap();


}
