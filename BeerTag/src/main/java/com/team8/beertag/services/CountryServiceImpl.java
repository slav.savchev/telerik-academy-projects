package com.team8.beertag.services;

import com.team8.beertag.exceptions.DuplicateEntityException;
import com.team8.beertag.models.Country;
import com.team8.beertag.repositories.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    private CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public Country getCountryByID(int id) {
        return countryRepository.getCountryByID(id);
    }

    @Override
    public List<Country> getAllCountries() {
        return countryRepository.getAllCountries();
    }

    @Override
    public Country createCountry(Country country) {
        if (checkCountryExists(country.getCountryName())) {
            throw new DuplicateEntityException(String.format("Country with name '%s' already exists!", country.getCountryName()));
        }

        return countryRepository.createCountry(country);
    }

    @Override
    public boolean checkCountryExists(String name) {
        return countryRepository.checkCountryExists(name);
    }

    @Override
    public Country updateCountry(Country country) {
        return countryRepository.updateCountry(country);
    }

    @Override
    public void deleteCountry(int id) {
        countryRepository.deleteCountry(id);
    }
}
