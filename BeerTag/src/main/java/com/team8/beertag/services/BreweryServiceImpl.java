package com.team8.beertag.services;

import com.team8.beertag.exceptions.DuplicateEntityException;
import com.team8.beertag.models.Brewery;
import com.team8.beertag.repositories.BreweryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BreweryServiceImpl implements BreweryService {

    private BreweryRepository breweryRepository;

    @Autowired
    public BreweryServiceImpl(BreweryRepository breweryRepository) {
        this.breweryRepository = breweryRepository;
    }

    @Override
    public List<Brewery> getAllBreweries() {
        return breweryRepository.getAllBreweries();
    }

    @Override
    public Brewery getBreweryById(int id) {
        return breweryRepository.getBreweryById(id);
    }

    @Override
    public Brewery createBrewery(Brewery brewery) {
        if(checkBreweryExists(brewery.getName())){
            throw new DuplicateEntityException(String.format("Brewery '%s' already exists",brewery.getName()));
        }
        return breweryRepository.createBrewery(brewery);
    }

    @Override
    public Brewery updateBrewery(Brewery brewery) {
        return breweryRepository.updateBrewery(brewery);
    }

    @Override
    public void deleteBrewery(int id) {
        breweryRepository.deleteBrewery(id);
    }

    @Override
    public boolean checkBreweryExists(String name) {
        return breweryRepository.checkBreweryExists(name);
    }
}
