package com.team8.beertag.services;


import com.team8.beertag.exceptions.DuplicateEntityException;
import com.team8.beertag.models.*;
import com.team8.beertag.repositories.BeerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class BeerServiceImpl implements BeerService {


    private BeerRepository beerRepository;
    private DtoMapper dtoMapper;

    @Autowired
    public BeerServiceImpl(BeerRepository beerRepository, DtoMapper dtoMapper) {
        this.beerRepository = beerRepository;
        this.dtoMapper=dtoMapper;
    }

    @Override
    public Beer createBeer(Beer beer) {
        if (checkBeerExists(beer.getName())) {
            throw new DuplicateEntityException(String.format("Beer with name '%s' already exists!", beer.getName()));
        }
        return beerRepository.createBeer(beer);
    }


    @Override
    public List<Beer> getAllBeers(Map<Integer,String> flagMap) {
        return beerRepository.getAllBeers(flagMap);
    }

    @Override
    public Beer getBeerById(int id) {
        return beerRepository.getBeerById(id);
    }

    @Override
    public boolean checkBeerExists(String name) {
        return beerRepository.checkBeerExists(name);
    }

    @Override
    public Beer updateBeer(Beer beer) {
        return beerRepository.updateBeer(beer);
    }

    @Override
    public void deleteBeer(int id) {
        beerRepository.deleteBeer(id);
    }

    @Override
    public List<Beer> getNewestBeers() {
        return beerRepository.getNewestBeers();
    }

    @Override
    public int getRandomBeerId() {
        return beerRepository.getRandomBeerId();
    }

    @Override
    public List<List<DisplayBeerDto>> getPages(Map<Integer,String> flagMap) {
        List<Beer> allBeers = beerRepository.getAllBeers(flagMap);
        int numberOfBeers = allBeers.size();
        int beersPerPage = 6;
        int listsToBeCreated = (int) Math.ceil((double)numberOfBeers / (double)beersPerPage);
        List<List<DisplayBeerDto>> pageLists = new ArrayList<>();
        int counter = 0;
        int stopFill =0;

        for (int i = 0; i <listsToBeCreated ; i++) {

            List<DisplayBeerDto> toBeFilled = new ArrayList<>();

            for (int j = 0; j <beersPerPage ; j++) {
                if(counter < numberOfBeers){
                    toBeFilled.add(dtoMapper.toDisplayBeerDto(allBeers.get(counter)));
                    counter++;
                } else {
                    stopFill =1;
                    break;
                }
            }
            pageLists.add(toBeFilled);
            if(stopFill==1){
                break;
            }
        }

        return pageLists;
    }

    @Override
    public List<Beer> getSimilarBeers(Beer mainBeer) {
        List<Beer> beers = beerRepository.getAllBeers();

        List<Beer> similarBeers = new ArrayList<>();

        Map<Beer, Integer> similarityMap = new HashMap<>();

        beers.forEach(beer -> {
            if(beer.getId() != mainBeer.getId()) {
                similarityMap.put(beer, getNumberOfSameTags(mainBeer, beer));
            }
        });

        for(int i = 0; i < 4; i++) {
            similarBeers.add(getMaxAndRemoveIt(similarityMap));
        }

        return similarBeers;
    }

    @Override
    public int getNumberOfSameTags(Beer mainBeer, Beer otherBeer) {
        Set<Integer> mainBeerTags = new HashSet<>();

        for (Tag tag : mainBeer.getTags()) {
            mainBeerTags.add(tag.getId());
        }

        int count = 0;

        for(Tag tag : otherBeer.getTags()) {
            if(mainBeerTags.contains(tag.getId())) {
                count++;
            }
        }

        return count;
    }

    private Beer getMaxAndRemoveIt(Map<Beer, Integer> similarityMap) {
        AtomicInteger max = new AtomicInteger(Integer.MIN_VALUE);
        similarityMap.forEach((beer, count) -> {
            if (count > max.get()) {
                max.set(count);
            }
        });

        AtomicReference<Beer> mostSimilarBeer = new AtomicReference<>(new Beer());

        similarityMap.forEach((beer, count) -> {
            if(count == max.get()) {
                mostSimilarBeer.set(beer);
            }
        });

        similarityMap.remove(mostSimilarBeer.get());
        return mostSimilarBeer.get();
    }

    @Override
    public List<Beer> getSuggestedBeers(User user) {
        List<Beer> drankList = user.getDrankList();
        Beer beer = getBeerById(getRandomBeerId());

        if(drankList.size() > 0) {
            drankList.sort(Comparator.comparingDouble(this::getRating));
            beer = drankList.get(drankList.size() - 1);
        }

        return getSimilarBeers(beer);
    }


    @Override
    public void addRating(UserRating rating) {
        beerRepository.addRating(rating);
    }

    private double getRating(Beer beer) {
        double numberOfRatings = beer.getRatings().size();

        if (numberOfRatings == 0) {
            return 0;
        }

        double sumOfRatings = beer.getRatings().stream().reduce(0, Integer::sum);

        return sumOfRatings/numberOfRatings;
    }
}
