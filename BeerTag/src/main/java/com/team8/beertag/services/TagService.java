package com.team8.beertag.services;

import com.team8.beertag.models.Tag;

import java.util.List;
import java.util.Set;

public interface TagService {

    List<Tag> getAllTags();

    Tag getTagById(int id);

    Tag createTag(Tag tag);

    Tag updateTag(Tag tag);

    void deleteTag(int id);

    boolean checkTagExists(String name);

    List<Tag> getTagByName(String name);

    List<Tag> getPopularTags();
}
