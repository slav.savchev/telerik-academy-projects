package com.team8.beertag.services;

import com.team8.beertag.exceptions.DuplicateEntityException;
import com.team8.beertag.models.*;
import com.team8.beertag.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService{

    private UserRepository repository;

    @Autowired
    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<User> getAllUsers() {
        return repository.getAllUsers();
    }

    @Override
    public User createUser(User user) {
        if(checkUserExists(user.getUsername())){
            throw new DuplicateEntityException(String.format("User with username '%s' already exists!", user.getUsername()));
        }
        repository.createUser(user);
        return user;
    }

    @Override
    public User updateUser(User user) {
        return repository.updateUser(user);
    }

    @Override
    public void deleteUser(int id) {
        repository.deleteUser(id);
    }

    @Override
    public List<Beer> getWishListBeers(int id) {
        return repository.getWishListBeers(id);
    }

    @Override
    public List<Beer> getDrankListBeers(int id) {
        return repository.getDrankListBeers(id);
    }

    @Override
    public User getUserById(int id) {
        return repository.getUserById(id);
    }

    @Override
    public int getNextAvailableId() {return repository.getNextAvailableId();}

    @Override
    public User constructUser(User user) {
        return repository.constructUser(user);
    }

    @Override
    public boolean checkUserExists(String name) {
        return repository.checkUserExists(name);
    }

    @Override
    public void deleteBeerFromWishList(int userId, int beerId) {
        repository.deleteBeerFromWishList(userId,beerId);
    }

    @Override
    public void deleteBeerFromDrankList(int userId, int beerId) {
        repository.deleteBeerFromDrankList(userId,beerId);
    }

    @Override
    public List<Beer> addBeerToWishList(int userId, int beerId) {
        return repository.addBeerToWishList(userId,beerId);
    }

    @Override
    public List<Beer> addBeerToDrankList(int userId, int beerId) {
        return repository.addBeerToDrankList(userId,beerId);
    }

    @Override
    public List<List<UserDto>> getUserPages(List<UserDto> allUsers) {
        int numOfusers = allUsers.size();
        int usersPerPage = 6;
        int listsToBeCreated = (int) Math.ceil((double)numOfusers / (double)usersPerPage);
        List<List<UserDto>> pageLists = new ArrayList<>();
        int counter = 0;
        int stopFill =0;

        for (int i = 0; i <listsToBeCreated ; i++) {

            List<UserDto> toBeFilled = new ArrayList<>();

            for (int j = 0; j <usersPerPage ; j++) {
                if(counter < numOfusers){
                    toBeFilled.add(allUsers.get(counter));
                    counter++;
                } else {
                    stopFill =1;
                    break;
                }
            }
            pageLists.add(toBeFilled);
            if(stopFill==1){
                break;
            }
        }

        return pageLists;
    }

    @Override
    public List<List<DisplayBeerDto>> listToPageListConverter(List<DisplayBeerDto> allBeers) {
        int numberOfBeers = allBeers.size();
        int beersPerPage = 6;
        int listsToBeCreated = (int) Math.ceil((double)numberOfBeers / (double)beersPerPage);
        List<List<DisplayBeerDto>> pageLists = new ArrayList<>();
        int counter = 0;
        int stopFill =0;

        for (int i = 0; i <listsToBeCreated ; i++) {

            List<DisplayBeerDto> toBeFilled = new ArrayList<>();

            for (int j = 0; j <beersPerPage ; j++) {
                if(counter < numberOfBeers){
                    toBeFilled.add(allBeers.get(counter));
                    counter++;
                } else {
                    stopFill =1;
                    break;
                }
            }
            pageLists.add(toBeFilled);
            if(stopFill==1){
                break;
            }
        }

        return pageLists;
    }

    @Override
    public Map<String, Integer> usernameToIdMap() {
        List<User> users = getAllUsers();
        Map<String, Integer> usernameIdMap = new HashMap<>();

        users.forEach(user -> usernameIdMap.put(user.getUsername(), user.getId()));

        usernameIdMap.put("anonymousUser", 0);

        return usernameIdMap;
    }


}
