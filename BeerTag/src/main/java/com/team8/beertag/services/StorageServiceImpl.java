package com.team8.beertag.services;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.nio.file.Path;
import java.util.Base64;
import java.util.stream.Stream;

@Component
public class StorageServiceImpl implements StorageService{


    @Override
    public byte[] toByteArray(MultipartFile file) {
        try{
            return file.getBytes();
        } catch (Exception e){
            throw new IllegalArgumentException("Bytes not found");
        }

    }

    @Override
    public String toBase64Image(byte[] bArray) {

        return Base64.getEncoder().encodeToString(bArray);
    }

}
