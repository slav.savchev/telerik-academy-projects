package com.team8.beertag.services;

import com.team8.beertag.exceptions.DuplicateEntityException;
import com.team8.beertag.models.Tag;
import com.team8.beertag.repositories.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.team8.beertag.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class TagServiceImpl implements TagService {

    private TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository){
        this.tagRepository = tagRepository;
    }

    @Override
    public List<Tag> getAllTags() {
        return tagRepository.getAllTags();
    }

    @Override
    public Tag getTagById(int id) {
        return tagRepository.getTagById(id);
    }

    @Override
    public Tag createTag(Tag tag) {
        if(checkTagExists(tag.getName())){
            throw new DuplicateEntityException(String.format("Tag '%s' already exists",tag.getName()));
        }
        return tagRepository.createTag(tag);
    }

    @Override
    public Tag updateTag(Tag tag) {
        return tagRepository.updateTag(tag);
    }

    @Override
    public void deleteTag(int id) {
        tagRepository.deleteTag(id);
    }

    @Override
    public boolean checkTagExists(String name) {
        return tagRepository.checkTagExists(name);
    }

    @Override
    public List<Tag> getTagByName(String name) {
        return tagRepository.getTagByName(name);
    }

    @Override
    public List<Tag> getPopularTags() {
        List<Tag> usedTags = tagRepository.getUsedTags();

        Map<Tag, Integer> tagCount = new HashMap<>();

        for (Tag tag : usedTags) {
            tagCount.putIfAbsent(tag, 0);
            tagCount.put(tag, tagCount.get(tag) + 1);
        }

        List<Tag> popularTags = new ArrayList<>();

        for (int i = 0; i < Constants.NUMBER_OF_TAGS_TO_DISPLAY; i++) {
            popularTags.add(getMaxAndRemoveIt(tagCount));
        }

        return popularTags;
    }

    private Tag getMaxAndRemoveIt(Map<Tag, Integer> tagCount) {
        AtomicInteger max = new AtomicInteger(Integer.MIN_VALUE);
        tagCount.forEach((tag, count) -> {
            if (count > max.get()) {
                max.set(count);
            }
        });

        AtomicReference<Tag> topTag = new AtomicReference<>(new Tag());

        tagCount.forEach((tag, count) -> {
            if(count == max.get()) {
                topTag.set(tag);
            }
        });

        tagCount.remove(topTag.get());
        return topTag.get();
    }

}
