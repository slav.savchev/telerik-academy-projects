package com.team8.beertag.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private DataSource securityDataSource;

    @Autowired
    public SecurityConfig(DataSource securityDataSource) {
        this.securityDataSource = securityDataSource;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder authenticationManager) throws Exception {
        //authenticationManager.inMemoryAuthentication().
        //        withUser(User.withUsername("emo").password("{noop}emo").roles("USER", "ADMIN"));

        authenticationManager.jdbcAuthentication().
                dataSource(securityDataSource);
    }

    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.authorizeRequests().
                antMatchers("/beers/").
                    permitAll().
                antMatchers("/beers/new").
                    hasRole("USER").
                antMatchers("/beers/?").
                    permitAll().
                antMatchers("beers/?/update").
                    hasRole("USER").
                antMatchers("beers/?/rating/**").
                    hasRole("USER").
                antMatchers("/community/**").
                    hasRole("USER").
                antMatchers("/users/**").
                    hasRole("USER").
                antMatchers("/admin").
                    hasRole("ADMIN").
                antMatchers("/").
                    permitAll().
                and().
                formLogin().
                    loginPage("/login").
                    loginProcessingUrl("/authenticate").
                    permitAll().
                and().
                    logout().
                    permitAll().
                and().
                exceptionHandling().
                    accessDeniedPage("/access-denied");
    }

    @Bean
    public UserDetailsManager userDetailsManager() {
        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
        jdbcUserDetailsManager.setDataSource(securityDataSource);
        return jdbcUserDetailsManager;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
