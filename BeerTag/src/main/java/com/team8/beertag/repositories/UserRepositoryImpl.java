package com.team8.beertag.repositories;

import com.team8.beertag.exceptions.EntityNotFoundException;
import com.team8.beertag.models.Beer;
import com.team8.beertag.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private SessionFactory sessionFactory;
    private BeerRepository beerRepository;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory, BeerRepository beerRepository){
        this.sessionFactory = sessionFactory;
        this.beerRepository = beerRepository;
    }


    @Override
    public List<User> getAllUsers() {
        try(Session session = sessionFactory.openSession()){
            Query<User> userQuery = session.createQuery("from User where enabled = :enabled",User.class);
            userQuery.setParameter("enabled",true);
            return userQuery.list();
        }
    }

    @Override
    public User getUserById(int id) {
        try(Session session = sessionFactory.openSession()){
            Query<User> userQuery = session.createQuery("from User where id = :user_id and enabled = :enabled",User.class);
            userQuery.setParameter("user_id",id);
            userQuery.setParameter("enabled",true);
            List<User> userList = userQuery.list();
            if(userList.isEmpty()){
                throw new EntityNotFoundException(String.format("User with id '%d' does not exist.",id));
            }
            else {
                User user = userList.get(0);
                if(!user.isEnabled()) {
                    throw new EntityNotFoundException(String.format("User with id '%d' does not exist", id));
                } else {
                    return user;
                }
            }
        }
    }

    @Override
    public int getNextAvailableId() {
        try(Session session = sessionFactory.openSession()) {
            Query<User> userQuery = session.createQuery("from User order by id desc",User.class);
            userQuery.setMaxResults(1);
            List<User> userList = userQuery.list();
            if (userList.get(0) == null) {
                return 1;
            }
            return userList.get(0).getId() + 1;
        }
    }

    @Override
    public User createUser(User user) {
        try(Session session = sessionFactory.openSession()){
            session.save(user);
            return user;
        }
    }

    @Override
    public User updateUser(User user) {
            getUserById(user.getId());
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();

            session.update(user);
            session.getTransaction().commit();
        }
        return user;
    }

    @Override
    public void deleteUser(int id) {
        try(Session session = sessionFactory.openSession()){
            User user = getUserById(id);
            session.beginTransaction();
            user.setEnabled(false);

            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public User constructUser(User user) {
        try(Session session = sessionFactory.openSession()) {

            session.beginTransaction();

            Query userQuery = session.createQuery(
                    "update User as u set " +
                            "id = :user_id," +
                            "firstName = :first_name," +
                            "lastName = :last_name," +
                            "email = :email," +
                            "image = :image where username = :username"
            );

            userQuery.setParameter("user_id", user.getId());
            userQuery.setParameter("first_name", user.getFirstName());
            userQuery.setParameter("last_name", user.getLastName());
            userQuery.setParameter("email", user.getEmail());
            userQuery.setParameter("username", user.getUsername());
            userQuery.setParameter("image", user.getImage());

            userQuery.executeUpdate();

            session.getTransaction().commit();
        }
        return user;
    }

    @Override
    public List<Beer> getWishListBeers(int id) {
        try(Session session = sessionFactory.openSession()){
            User user = session.get(User.class, id);
            List<Beer> wishList = user.getWishList();

            return wishList.stream()
                    .filter(beer -> !beer.isDeleted())
                    .collect(Collectors.toList());
        }
    }

    @Override
    public List<Beer> addBeerToWishList(int userId, int beerId) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            Beer beer = beerRepository.getBeerById(beerId);

            User user = session.get(User.class, userId);
            List<Beer> wishList = user.getWishList();
            List<Beer> drankList = user.getDrankList();

            //user.setDrankList(drankList.stream().filter(beer1 -> beer.getId() != beerId).collect(Collectors.toList()));
            drankList.removeIf(specificItem -> specificItem.getId() == beerId);

            if(!wishList.contains(beer)){
                wishList.add(beer);
                wishList = wishList.stream().filter(distinctById(Beer::getId)).collect(Collectors.toList());
                user.setWishList(wishList);
            }

            session.getTransaction().commit();

            return wishList;
        }
    }

    @Override
    public List<Beer> addBeerToDrankList(int userId, int beerId) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            Beer beer = beerRepository.getBeerById(beerId);

            User user = session.get(User.class, userId);
            List<Beer> drankList = user.getDrankList();
            List<Beer> wishList = user.getWishList();

            //user.setWishList(wishList.stream().filter(beer1 -> beer.getId() != beerId).collect(Collectors.toList()));
            wishList.removeIf(specificItem -> specificItem.getId() == beerId);

            if(!drankList.contains(beer)){
                drankList.add(beer);
                drankList = drankList.stream().filter(distinctById(Beer::getId)).collect(Collectors.toList());
                user.setDrankList(drankList);
            }

            session.getTransaction().commit();

            return drankList;
        }
    }

    @Override
    public void deleteBeerFromWishList(int userId, int beerId) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            User user = session.get(User.class, userId);
            List<Beer> wishList = user.getWishList();

            Beer toBeRemoved = wishList.stream().filter(beer -> beer.getId() == beerId).collect(Collectors.toList()).get(0);
            wishList.remove(toBeRemoved);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteBeerFromDrankList(int userId, int beerId) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            User user = session.get(User.class, userId);
            List<Beer> drankList = user.getDrankList();

            Beer toBeRemoved = drankList.stream().filter(beer -> beer.getId() == beerId).collect(Collectors.toList()).get(0);
            drankList.remove(toBeRemoved);
            session.getTransaction().commit();
        }
    }


    @Override
    public List<Beer> getDrankListBeers(int id) {
        try(Session session = sessionFactory.openSession()){
            User user = session.get(User.class,id);
            List<Beer> drankList = user.getDrankList();

            return drankList.stream()
                    .filter(beer -> !beer.isDeleted())
                    .collect(Collectors.toList());
        }
    }

    @Override
    public boolean checkUserExists(String username) {
        try(Session session = sessionFactory.openSession()){
            Query<User> userQuery = session.createQuery("from User where username = :username and enabled = :enabled",User.class);
            userQuery.setParameter("username",username);
            userQuery.setParameter("enabled",true);
            List<User> userList = userQuery.list();
            return userList.size() != 0;
        }
    }


    private static <Beer> Predicate<Beer> distinctById(Function<? super Beer, ?> idExtractor) {
        Set<Beer> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add((Beer) idExtractor.apply(t));
    }



}
