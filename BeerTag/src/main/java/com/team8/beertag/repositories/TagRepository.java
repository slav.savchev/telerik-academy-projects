package com.team8.beertag.repositories;

import com.team8.beertag.models.Tag;

import java.util.List;

public interface TagRepository {

    List<Tag> getAllTags();

    Tag getTagById(int id);

    Tag createTag(Tag tag);

    Tag updateTag(Tag tag);

    void deleteTag(int id);

    boolean checkTagExists(String name);

    List<Tag> getTagByName(String name);

    List<Tag> getUsedTags();

}
