package com.team8.beertag.repositories;

import com.team8.beertag.models.Beer;
import com.team8.beertag.models.UserRating;

import java.util.List;
import java.util.Map;

public interface BeerRepository {
    Beer createBeer(Beer beer);

    List<Beer> getAllBeers(Map<Integer,String> flagMap);

    Beer getBeerById(int id);

    boolean checkBeerExists(String name);

    Beer updateBeer(Beer beer);

    void deleteBeer(int id);

    List<Beer> getBeersByName(String name);

    List<Beer> getNewestBeers();

    int getRandomBeerId();

    List<Beer> getAllBeers();

    void addRating(UserRating rating);
}
