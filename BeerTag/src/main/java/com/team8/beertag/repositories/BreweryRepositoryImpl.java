package com.team8.beertag.repositories;

import com.team8.beertag.exceptions.EntityNotFoundException;
import com.team8.beertag.models.Brewery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BreweryRepositoryImpl implements BreweryRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public BreweryRepositoryImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Brewery> getAllBreweries() {
        try(Session session = sessionFactory.openSession()){
            Query<Brewery> breweryQuery = session.createQuery("from Brewery where isDeleted = :isDeleted",Brewery.class);
            breweryQuery.setParameter("isDeleted",false);
            return breweryQuery.list();
        }
    }

    @Override
    public Brewery getBreweryById(int id) {
        try(Session session = sessionFactory.openSession()){
            Brewery brewery = session.get(Brewery.class,id);
            if(brewery == null){
                throw new EntityNotFoundException(String.format("Brewery with id '%d' does not exist",id));
            }
            if(brewery.getDeleted()) {
                throw new EntityNotFoundException(String.format("Brewery with id '%d' does not exist", id));
            } else {
                return brewery;
            }
        }
    }

    @Override
    public Brewery createBrewery(Brewery brewery) {
        try(Session session = sessionFactory.openSession()){
            session.save(brewery);
            return brewery;
        }
    }

    @Override
    public Brewery updateBrewery(Brewery brewery) {
            getBreweryById(brewery.getId());
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(brewery);
            session.getTransaction().commit();
            return brewery;
        }
    }

    @Override
    public void deleteBrewery(int id) {
        try(Session session = sessionFactory.openSession()){
            Brewery brewery = getBreweryById(id);
            brewery.setDeleted(true);
            session.beginTransaction();
            session.update(brewery);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean checkBreweryExists(String name) {
        try(Session session = sessionFactory.openSession()){
            Query<Brewery> breweryQuery = session.createQuery("from Brewery where name = :name and isDeleted = :isDeleted",Brewery.class);
            breweryQuery.setParameter("name", name);
            breweryQuery.setParameter("isDeleted", false);
            List<Brewery> breweryList = breweryQuery.list();
            return breweryList.size() != 0;
        }
    }
}
