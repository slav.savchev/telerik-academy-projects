package com.team8.beertag.repositories;

import com.team8.beertag.models.Style;

import java.util.List;

public interface StyleRepository {
    List<Style> getAllStyles();

    Style getById(int id);

    Style createStyle(Style style);

    boolean checkStyleExists(String name);

    Style updateStyle(Style style);

    void deleteStyle(int id);

    List<Style> getAllUsedStyles();

}
