package com.team8.beertag.repositories;

import com.team8.beertag.exceptions.EntityNotFoundException;
import com.team8.beertag.models.Beer;
import com.team8.beertag.models.Style;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StyleRepositoryImpl implements StyleRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public StyleRepositoryImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Style> getAllStyles() {
        try(Session session = sessionFactory.openSession()){
            Query<Style> styleQuery = session.createQuery("from Style where isDeleted = :isDeleted",Style.class);
            styleQuery.setParameter("isDeleted",false);
            return styleQuery.list();
        }
    }

    @Override
    public Style getById(int id) {
        try(Session session = sessionFactory.openSession()){
            Style style = session.get(Style.class,id);
            if(style == null){
                throw new EntityNotFoundException(String.format("Style with id '%d' does not exist",id));
            }
            if(style.getDeleted()) {
                throw new EntityNotFoundException(String.format("Style with id '%d' does not exist", id));
            } else {
                return style;
            }
        }
    }

    @Override
    public Style createStyle(Style style) {
        try(Session session = sessionFactory.openSession()){
            session.save(style);
            return style;
        }
    }

    @Override
    public boolean checkStyleExists(String name) {
        try(Session session = sessionFactory.openSession()){
            Query<Style> styleQuery = session.createQuery("from Style where name = :name and isDeleted = :isDeleted",Style.class);
            styleQuery.setParameter("name",name);
            styleQuery.setParameter("isDeleted",false);
            List<Style> styleList = styleQuery.list();
            return styleList.size() != 0;
        }
    }

    @Override
    public Style updateStyle(Style style) {
        getById(style.getStyleId());
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(style);
            session.getTransaction().commit();
            return style;
        }
    }

    @Override
    public void deleteStyle(int id) {
        try(Session session = sessionFactory.openSession()){
            Style style = getById(id);
            style.setDeleted(true);
            session.beginTransaction();
            session.update(style);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Style> getAllUsedStyles() {
        try(Session session = sessionFactory.openSession()){
            Query<Beer> beerQuery = session.createQuery("from Beer where isDeleted = :isDeleted ",Beer.class);
            beerQuery.setParameter("isDeleted",false);
            List<Style> styles = new ArrayList<>();
            List<Beer> beers = beerQuery.list();
            for (Beer beer : beers) {
                styles.add(beer.getStyle());
            }
            return styles;
        }
    }
}
