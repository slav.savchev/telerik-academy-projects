package com.team8.beertag.repositories;

import com.team8.beertag.models.Brewery;

import java.util.List;

public interface BreweryRepository {

    List<Brewery> getAllBreweries();

    Brewery getBreweryById(int id);

    Brewery createBrewery(Brewery brewery);

    Brewery updateBrewery(Brewery brewery);

    void deleteBrewery(int id);

    boolean checkBreweryExists(String name);
}
