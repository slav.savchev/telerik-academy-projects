package com.team8.beertag.repositories;

import com.team8.beertag.exceptions.EntityNotFoundException;
import com.team8.beertag.models.Country;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountryRepositoryImpl implements CountryRepository{

    private SessionFactory sessionFactory;

    @Autowired
    public CountryRepositoryImpl(SessionFactory sessionFactory){
        this.sessionFactory= sessionFactory;
    }

    @Override
    public List<Country> getAllCountries() {
        try(Session session = sessionFactory.openSession()){
            Query<Country> countryQuery = session.createQuery("from Country where isDeleted = :isDeleted",Country.class);
            countryQuery.setParameter("isDeleted",false);
            return countryQuery.list();
        }
    }

    @Override
    public Country getCountryByID(int id) {
        try(Session session = sessionFactory.openSession()){
            Country country = session.get(Country.class,id);
            if(country == null){
                throw new EntityNotFoundException(String.format("Country with id '%d' does not exist.",id));
            }
            if(country.getDeleted()) {
                throw new EntityNotFoundException(String.format("Country with id '%d' does not exist", id));
            } else {
                return country;
            }
        }
    }

    @Override
    public Country createCountry(Country country) {
        try(Session session = sessionFactory.openSession()){
            session.save(country);
            return country;
        }
    }

    @Override
    public boolean checkCountryExists(String name) {
        try(Session session = sessionFactory.openSession()){
            Query<Country> countryQuery = session.createQuery("from Country  where countryName = :countryName and isDeleted = :isDeleted",Country.class);
            countryQuery.setParameter("countryName", name);
            countryQuery.setParameter("isDeleted", false);
            List<Country> countryList = countryQuery.list();
            if(countryList.size() !=0){
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public Country updateCountry(Country country) {
            getCountryByID(country.getCountryId());
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(country);
            session.getTransaction().commit();
            return country;
        }
    }

    @Override
    public void deleteCountry(int id) {
        try(Session session = sessionFactory.openSession()){
            Country country = getCountryByID(id);
            country.setDeleted(true);
            session.beginTransaction();
            session.update(country);
            session.getTransaction().commit();
        }
    }
}
