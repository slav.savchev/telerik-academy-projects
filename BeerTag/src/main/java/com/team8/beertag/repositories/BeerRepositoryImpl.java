package com.team8.beertag.repositories;

import com.team8.beertag.exceptions.EntityNotFoundException;
import com.team8.beertag.models.Beer;
import com.team8.beertag.models.Tag;
import com.team8.beertag.models.UserRating;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

@Repository
public class BeerRepositoryImpl implements BeerRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public BeerRepositoryImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Beer createBeer(Beer beer) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(beer);
            session.getTransaction().commit();
        }
        return beer;
    }

    @Override
    public List<Beer> getAllBeers(Map<Integer,String> flagMap) {
        try(Session session = sessionFactory.openSession()){
            Query<Beer> beerQuery = buildQuery(flagMap,session);

            List<Beer> filteredBeers = filterList(beerQuery);

            if(flagMap.containsKey(10)){
                List<Beer> filteredByTags = new ArrayList<>();
                for (int i = 0; i <filteredBeers.size() ; i++) {

                    List<Tag> tagList = filteredBeers.get(i).getTags();

                    for (int j = 0; j <tagList.size() ; j++) {
                        if(tagList.get(j).getName().equalsIgnoreCase(flagMap.get(10))){
                            filteredByTags.add(filteredBeers.get(i));
                        }
                    }
                }
                filteredBeers = filteredByTags;
            }

            return sortByRating(flagMap, filteredBeers);
        }
    }

    @Override
    public List<Beer> getAllBeers() {
        try(Session session = sessionFactory.openSession()){
            Query<Beer> beerQuery = session.createQuery("from Beer where isDeleted = :isDeleted", Beer.class);
            beerQuery.setParameter("isDeleted", false);

            return beerQuery.list();
        }
    }

    @Override
    public void addRating(UserRating rating) {
        try(Session session = sessionFactory.openSession()){
            Query<UserRating> ratingQuery = session.createQuery("from UserRating", UserRating.class);

            List<UserRating> ratings = ratingQuery.list().
                    stream().
                    filter(userRating -> userRating.getUser().getId() == rating.getUser().getId() && userRating.getBeer().getId() == rating.getBeer().getId()).
                    collect(Collectors.toList());

            if(ratings.isEmpty()) {
                session.beginTransaction();
                session.save(rating);
                session.getTransaction().commit();
            }

            else {
                ratings.get(0).setRating(rating.getRating());

                session.beginTransaction();
                session.update(ratings.get(0));
                session.getTransaction().commit();
            }

        }
    }

    @Override
    public Beer getBeerById(int id) {
        try(Session session = sessionFactory.openSession()){
            Beer beer = session.get(Beer.class,id);
            if(beer == null){
                throw new EntityNotFoundException(String.format("Beer with id '%d' not found",id));
            }
            if(beer.isDeleted()) {
                throw new EntityNotFoundException(String.format("Beer with id '%d' does not exist", id));
            } else {
                return beer;
            }
        }
    }



    @Override
    public boolean checkBeerExists(String name) {
        return getBeersByName(name).size() != 0;
    }

    @Override
    public Beer updateBeer(Beer beer) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();

            session.update(beer);
            session.getTransaction().commit();
            return beer;
        }
    }

    @Override
    public void deleteBeer(int id) {
        try(Session session = sessionFactory.openSession()){
            Beer beer = getBeerById(id);
            session.beginTransaction();
            beer.setDeleted(true);

            session.update(beer);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Beer> getBeersByName(String name) {
        try(Session session = sessionFactory.openSession()){
            Query<Beer> beerQuery = session.createQuery("from Beer where name = :name and isDeleted = :isDeleted",Beer.class);
            beerQuery.setParameter("name",name);
            beerQuery.setParameter("isDeleted",false);
           return beerQuery.list();
        }
    }

    @Override
    public List<Beer> getNewestBeers() {
        try(Session session = sessionFactory.openSession()){
            Query<Beer> beerQuery = session.createQuery("from Beer where isDeleted = :isDeleted order by id desc",Beer.class);
            beerQuery.setMaxResults(8);
            beerQuery.setParameter("isDeleted",false);
            return beerQuery.list();
        }
    }

    @Override
    public int getRandomBeerId() {
        try(Session session = sessionFactory.openSession()){
            Query<Beer> minQuery = session.createQuery("from Beer where isDeleted = :isDeleted order by id",Beer.class);
            Query<Beer> maxQuery = session.createQuery("from Beer where isDeleted = :isDeleted order by id desc",Beer.class);

            minQuery.setMaxResults(1);
            minQuery.setParameter("isDeleted",false);

            maxQuery.setMaxResults(1);
            maxQuery.setParameter("isDeleted",false);

            int minNumber = minQuery.list().get(0).getId();
            int maxNumber = maxQuery.list().get(0).getId();

            Beer beer = null;

            boolean deletedBeer = true;

            while(deletedBeer) {
                int randomNumber = ThreadLocalRandom.current().nextInt(minNumber, maxNumber + 1);
                beer =  session.get(Beer.class, randomNumber);
                deletedBeer = beer.isDeleted();
            }

            return beer.getId();
        }
    }



    private List<Beer> filterList(Query<Beer> beerQuery) {
        return beerQuery.list().
                stream().
                peek(beer -> {
                    if(beer.getStyle().getDeleted()) beer.setStyle(null);
                    if(beer.getBrewery().getDeleted()) beer.setBrewery(null);
                    if(beer.getOriginCountry().getDeleted()) beer.setOriginCountry(null);
                    beer.setTags(
                            beer.getTags().
                                    stream().
                                    filter(tag -> !tag.getDeleted()).
                                    collect(Collectors.toList())
                    );
                })
                .collect(Collectors.toList());
    }

    private Query<Beer> buildQuery(Map<Integer, String> flagMap,Session session) {
        StringBuffer queryString = new StringBuffer("from Beer where isDeleted = :isDeleted");
        boolean filterName = flagMap.containsKey(1);
        boolean filterAbv = flagMap.containsKey(2);
        boolean filterStyle = flagMap.containsKey(3);
        boolean filterBrewery = flagMap.containsKey(4);
        boolean filterCountry = flagMap.containsKey(5);
        boolean sort = flagMap.containsKey(6);

        if(filterName){
            queryString.append(" and name like :name");
        }
        if(filterAbv){
            queryString.append(" and abv = :abv");
        }
        if(filterStyle){
            queryString.append(" and style.name = :style");
        }
        if(filterBrewery){
            queryString.append(" and brewery.name = :brewery");
        }
        if(filterCountry){
            queryString.append(" and originCountry.countryName = :country");
        }
        if(sort){
            String sortBy = flagMap.get(6);
            switch (sortBy) {
                case "name":
                    queryString.append(" order by name");
                    break;
                case "abv":
                    queryString.append(" order by abv");
                    break;
                case "style":
                    queryString.append(" and style.isDeleted = :isDeleted order by style.name");
                    break;
                case "brewery":
                    queryString.append(" and brewery.isDeleted = :isDeleted order by brewery.name");
                    break;
                case "country":
                    queryString.append(" and originCountry.isDeleted = :isDeleted order by originCountry");
                    break;
            }
        }
        Query<Beer> beerQuery = session.createQuery(String.valueOf(queryString),Beer.class);
        beerQuery.setParameter("isDeleted",false);

        if(filterName){
            beerQuery.setParameter("name","%" + flagMap.get(1) + "%");
        }
        if(filterAbv){
            beerQuery.setParameter("abv",flagMap.get(2));
        }
        if(filterStyle){
            beerQuery.setParameter("style",flagMap.get(3));
        }
        if(filterBrewery){
            beerQuery.setParameter("brewery",flagMap.get(4));
        }
        if(filterCountry){
            beerQuery.setParameter("country",flagMap.get(5));
        }

        return beerQuery;
    }

    private List<Beer> sortByRating(Map<Integer, String> flagMap, List<Beer> filteredBeers) {
        if(flagMap.containsKey(6)) {
            if (flagMap.get(6).equals("rating")) {
                filteredBeers = filteredBeers.
                        stream().
                        filter(beer -> !beer.getRatings().isEmpty()).
                        collect(Collectors.toList());

                filteredBeers.sort(Comparator.comparing(Beer::getRatings, (ratings1, ratings2) -> {
                    double sumOfRatings1 = ratings1.stream().reduce(0, Integer::sum);
                    double rating1 = sumOfRatings1/ratings1.size();

                    double sumOfRatings2 = ratings2.stream().reduce(0, Integer::sum);
                    double rating2 = sumOfRatings2/ratings2.size();

                    return Double.compare(rating2, rating1);
                }));
            }
        }
        return filteredBeers;
    }
}
