package com.team8.beertag.repositories;

import com.team8.beertag.models.Country;

import java.util.List;

public interface CountryRepository {
    List<Country> getAllCountries();

    Country getCountryByID(int id);

    Country createCountry(Country country);

    boolean checkCountryExists(String name);

    Country updateCountry(Country country);

    void deleteCountry(int id);
}
