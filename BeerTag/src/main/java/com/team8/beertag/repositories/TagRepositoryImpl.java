package com.team8.beertag.repositories;

import com.team8.beertag.exceptions.EntityNotFoundException;
import com.team8.beertag.models.Beer;
import com.team8.beertag.models.Tag;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TagRepositoryImpl implements TagRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Tag> getAllTags() {
        try(Session session = sessionFactory.openSession()){
            Query<Tag> tagQuery = session.createQuery("from Tag where isDeleted = :isDeleted",Tag.class);
            tagQuery.setParameter("isDeleted", false);
            return tagQuery.list();
        }
    }

    @Override
    public Tag getTagById(int id) {
        try(Session session = sessionFactory.openSession()){
            Tag tag = session.get(Tag.class,id);
            if(tag == null){
                throw new EntityNotFoundException(String.format("Tag with id '%d' does not exist.",id));
            }
            if(tag.getDeleted()) {
                throw new EntityNotFoundException(String.format("Tag with id '%d' does not exist", id));
            } else {
                return tag;
            }
        }
    }

    @Override
    public Tag createTag(Tag tag) {
        try(Session session = sessionFactory.openSession()) {
            session.save(tag);
            return tag;
        }
    }

    @Override
    public Tag updateTag(Tag tag) {
            getTagById(tag.getId());
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.update(tag);
            session.getTransaction().commit();
            return tag;
        }
    }

    @Override
    public void deleteTag(int id) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            Tag tag = getTagById(id);
            tag.setDeleted(true);
            session.update(tag);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean checkTagExists(String name) {
        return getTagByName(name).size() != 0;
    }

    @Override
    public List<Tag> getTagByName(String name) {
        try(Session session = sessionFactory.openSession()){
            Query<Tag> tagQuery = session.createQuery("from Tag where name = :name and isDeleted = :isDeleted",Tag.class);
            tagQuery.setParameter("name",name);
            tagQuery.setParameter("isDeleted",false);
            return tagQuery.list();
        }
    }

    @Override
    public List<Tag> getUsedTags() {
        try(Session session = sessionFactory.openSession()){
            Query<Beer> beerQuery = session.createQuery("from Beer where isDeleted = :isDeleted",Beer.class);
            beerQuery.setParameter("isDeleted", false);

            List<Tag> usedTags = new ArrayList<>();
            beerQuery.list().forEach(beer -> usedTags.addAll(beer.getTags()));

            return usedTags;
        }
    }


}
