package com.team8.beertag.repositories;

import com.team8.beertag.models.Beer;
import com.team8.beertag.models.User;

import java.util.List;

public interface UserRepository {
    List<User> getAllUsers();

    User createUser(User user);

    User updateUser(User user);

    void deleteUser(int id);

    List<Beer> getWishListBeers(int id);

    List<Beer> getDrankListBeers(int id);

    User getUserById(int id);

    int getNextAvailableId();

    User constructUser(User user);

    boolean checkUserExists(String username);

    void deleteBeerFromWishList(int userId, int beerId);

    void deleteBeerFromDrankList(int userId, int beerId);

    List<Beer> addBeerToWishList(int userId, int beerId);

    List<Beer> addBeerToDrankList(int userId, int beerId);
}
