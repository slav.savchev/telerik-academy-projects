package com.team8.beertag.models;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "styles")
public class Style {

    @NotNull
    @NotBlank
    @Size(min = 2, max = 40, message = "Style must be between 2 and 40 symbols long.")
    @Column(name = "name")
    private String name;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @PositiveOrZero(message = "Style id must be a positive number or zero.")
    @Column(name = "style_id")
    private int styleId;

    @Column(name = "is_deleted")
    private Boolean isDeleted=false;

    public Style(){}

    public Style(String name, int styleId) {
        setName(name);
        setStyleId(styleId);
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStyleId() {
        return styleId;
    }

    public void setStyleId(int styleId) {
        this.styleId = styleId;
    }
}
