package com.team8.beertag.models;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.List;

@Component
public class UserDto {

    private int id;

    private String username;

    private String password;

    private String firstName;

    private String lastName;

    private String email;

    private List<Integer> wishListBeerIds;

    private List<Integer> drankListBeerIds;

    private String image;

    public UserDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Integer> getWishListBeerIds() {
        return wishListBeerIds;
    }

    public void setWishListBeerIds(List<Integer> wishListBeerIds) {
        this.wishListBeerIds = wishListBeerIds;
    }

    public List<Integer> getDrankListBeerIds() {
        return drankListBeerIds;
    }

    public void setDrankListBeerIds(List<Integer> drankListBeerIds) {
        this.drankListBeerIds = drankListBeerIds;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
