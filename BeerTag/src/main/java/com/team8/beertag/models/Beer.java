package com.team8.beertag.models;



import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "beers")
public class Beer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "beer_id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "abv")
    private double ABV;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "brewery_id")
    private Brewery brewery;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country originCountry;

    @ManyToOne
    @JoinColumn(name = "style_id")
    private Style style;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "beers_tags",
            joinColumns = @JoinColumn(name = "beer_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private List<Tag> tags;

    @Column(name = "is_deleted")
    private Boolean isDeleted=false;

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(
            name = "users_beers_rating",
            joinColumns = @JoinColumn(name = "users_beers_rating_beer_id")
    )
    @Column(name = "rating")
    private List<Integer> ratings;

    @Column(name = "picture")
    private byte[] image;

    @Column(name = "creator_id")
    private int creatorId;

    public Beer() {
        tags = new ArrayList<>();
        ratings = new ArrayList<>();
    }


    public Beer(String name, double ABV, String description) {
        tags = new ArrayList<>();
        ratings = new ArrayList<>();

        setName(name);
        setABV(ABV);
        setDescription(description);
    }

    public Beer(int id,String name,String description,double ABV) {
        tags = new ArrayList<>();
        ratings = new ArrayList<>();

        setId(id);
        setName(name);
        setABV(ABV);
        setDescription(description);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Brewery getBrewery() {
        return brewery;
    }

    public Country getOriginCountry() {
        return originCountry;
    }

    public Style getStyle() {
        return style;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public Boolean isDeleted() {
        return isDeleted;
    }

    public double getABV() {
        return ABV;
    }

    public List<Integer> getRatings() {
        return ratings;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setABV(double ABV) {
        this.ABV = ABV;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setBrewery(Brewery brewery) {
        this.brewery = brewery;
    }

    public void setOriginCountry(Country originCountry) {
        this.originCountry = originCountry;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public void setTags(List<Tag> tagList) {
        tags = tagList;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public void setRatings(List<Integer> ratings) {
        this.ratings = ratings;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }
}

