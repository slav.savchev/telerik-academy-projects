package com.team8.beertag.models;

import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Component
public class CreateUserDto {

    @NotNull
    @NotBlank
    @Size(min = 1, message="Username is required.")
    private String username;

    @NotNull
    @NotBlank
    @Size(min = 1, message="Password is required.")
    private String password;

    @NotNull
    @NotBlank
    @Size(min = 1, message="Password confirmation is required.")
    private String passwordConfirmation;

    @NotNull
    @NotBlank
    @Size(min = 2, message="First name is required.")
    private String firstName;

    @NotNull
    @NotBlank
    @Size(min = 2, message="Last Name is required.")
    private String lastName;

    @NotNull
    @NotBlank
    private String email;

    public CreateUserDto(){}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
