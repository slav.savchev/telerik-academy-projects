package com.team8.beertag.models;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "countries")
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "country_id")
    private int countryId;

    @NotNull
    @NotBlank
    @Size(min = 2, max = 40, message = "Country name must be between 2 and 25 symbols long.")
    @Column(name = "name")
    private String countryName;

    @Column(name = "is_deleted")
    private Boolean isDeleted=false;

    public Country() {
    }

    public Country(String countryName, int countryId) {
        setCountryName(countryName);
        setCountryId(countryId);
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

}
