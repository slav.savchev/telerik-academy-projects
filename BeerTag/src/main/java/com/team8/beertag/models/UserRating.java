package com.team8.beertag.models;

import org.springframework.stereotype.Component;

import javax.persistence.*;

@Entity
@Table(name = "users_beers_rating")
public class UserRating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "users_beers_rating_id")
    private int id;

    @Column(name = "rating")
    private int rating;

    @ManyToOne
    @JoinColumn(name = "users_beers_rating_user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "users_beers_rating_beer_id")
    private Beer beer;

    public UserRating(){

    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }
}
