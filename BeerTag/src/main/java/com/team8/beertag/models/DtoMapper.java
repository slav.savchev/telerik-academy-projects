package com.team8.beertag.models;



import com.team8.beertag.services.*;
import com.team8.beertag.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.util.*;

@Component
public class DtoMapper {

    private StyleService styleService;
    private CountryService countryService;
    private BreweryService breweryService;
    private TagService tagService;
    private UserService userService;
    private StorageService storageService;

    @Autowired
    public DtoMapper(StyleService styleService, CountryService countryService, BreweryService breweryService, TagService tagService, UserService userService, StorageService storageService) {
        this.styleService = styleService;
        this.countryService = countryService;
        this.breweryService = breweryService;
        this.tagService = tagService;
        this.userService = userService;
        this.storageService = storageService;
    }

    public Beer fromCreateBeerDto(CreateBeerDto createBeerDto){
        Beer beer = new Beer(createBeerDto.getName(), createBeerDto.getABV(), createBeerDto.getDescription());

        Style newStyle = styleService.getById(createBeerDto.getStyleID());
        beer.setStyle(newStyle);

        Country newCountry = countryService.getCountryByID(createBeerDto.getOriginCountryID());
        beer.setOriginCountry(newCountry);

        Brewery newBrewery = breweryService.getBreweryById(createBeerDto.getBreweryID());
        beer.setBrewery(newBrewery);

        String[] tagsArray = createBeerDto.getTags().split(", ");

        Set<Tag> newTagSet = new HashSet<>();
        for (String elem: tagsArray) {
            if(!tagService.checkTagExists(elem)) {
                Tag newTag = tagService.createTag(new Tag(elem));
                newTagSet.add(newTag);
            } else {
                newTagSet.add(tagService.getTagByName(elem).get(0));
            }

        }
        beer.setTags(new ArrayList<>(newTagSet));

        if(createBeerDto.getImage()!=null){
            beer.setImage(storageService.toByteArray(createBeerDto.getImage()));
        }

        beer.setCreatorId(createBeerDto.getCreatorId());

        return beer;
    }

    public DisplayBeerDto toDisplayBeerDto(Beer beer) {
        DisplayBeerDto dto = new DisplayBeerDto();

        dto.setId(beer.getId());
        dto.setABV(beer.getABV());
        dto.setName(beer.getName());
        dto.setDescription(beer.getDescription());
        dto.setStyle(beer.getStyle());
        dto.setBrewery(beer.getBrewery());
        dto.setOriginCountry(beer.getOriginCountry());
        dto.setTags(beer.getTags());
        dto.setCreatorId(beer.getCreatorId());

        double numberOfRatings = beer.getRatings().size();

        double sumOfRatings = beer.getRatings().
                stream().
                reduce(0, Integer::sum);

        double rating = sumOfRatings/numberOfRatings;

        dto.setRating(rating);

        dto.setImage(storageService.toBase64Image(beer.getImage()));

        return dto;
    }

    public List<DisplayBeerDto> toDisplayBeerDto(List<Beer> beers) {
        List<DisplayBeerDto> dtoList = new ArrayList<>();
        for (Beer beer : beers) {
            dtoList.add(toDisplayBeerDto(beer));
        }
        return dtoList;
    }

    public User fromCreateUserDto(CreateUserDto dto) {
        User user = new User(dto.getUsername(), dto.getPassword(), dto.getFirstName(), dto.getLastName(), dto.getEmail());
        user.setId(userService.getNextAvailableId());
        try {
            user.setImage(Base64.getDecoder().decode(Constants.DEFAULT_USER_PROFILE_PICTURE.getBytes("UTF-8")));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return user;
    }

    public UserDto toUserDto(User user) {
        UserDto dto = new UserDto();

        dto.setId(user.getId());
        dto.setUsername(user.getUsername());
        dto.setPassword(user.getPassword());
        dto.setEmail(user.getEmail());
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());

        List<Integer> wishListBeerIds = new ArrayList<>();
        List<Integer> drankListBeerIds = new ArrayList<>();

        for (Beer beer : user.getWishList()) {
            wishListBeerIds.add(beer.getId());
        }

        for (Beer beer : user.getDrankList()) {
            drankListBeerIds.add(beer.getId());
        }

        dto.setWishListBeerIds(wishListBeerIds);
        dto.setDrankListBeerIds(drankListBeerIds);


        dto.setImage(storageService.toBase64Image(user.getImage()));

        return dto;
    }

    public List<UserDto> toUserDtoList(List<User> userList){

        List<UserDto> userDtoList = new ArrayList<>();

        for (int i = 0; i <userList.size() ; i++) {

            userDtoList.add(toUserDto(userList.get(i)));
        }

        return userDtoList;
    }

}
