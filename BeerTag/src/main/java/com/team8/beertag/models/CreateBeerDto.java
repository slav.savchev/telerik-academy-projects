package com.team8.beertag.models;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Component
public class CreateBeerDto {

    private int id;

    @NotNull
    @NotBlank
    @Size(min = 2, max = 40, message = "Beer name must be between 2 and 40 symbols long.")
    private String name;

    @PositiveOrZero(message = "ABV cannot be a negative value.")
    private double ABV;

//    @NotNull
//    @NotBlank (message = "Please provide a valid description for this beer.")
    private String description;

//    @NotNull
//    private Brewery brewery;

    private int breweryID;
    private int styleID;
    private int originCountryID;

    private String tags;

    private MultipartFile image;

    private int creatorId;

//    public CreateBeerDto() {
//        tags = new ArrayList<>();
//    }

    public CreateBeerDto(){
    }

    public CreateBeerDto(String name, double ABV, String description, int styleID, int originCountryID, int breweryID, String tags) {
//        this.tags = new ArrayList<>();

        setName(name);
        setABV(ABV);
        setDescription(description);
        setStyleID(styleID);
        setOriginCountryID(originCountryID);
        setBreweryID(breweryID);
        setTags(tags);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getBreweryID() {
        return breweryID;
    }

    public int getOriginCountryID() {
        return originCountryID;
    }

    public String getTags() {
        return tags;
    }

    public int getStyleID() {
        return styleID;
    }

    public void setStyleID(int styleID) {
        this.styleID = styleID;
    }

    public double getABV() {
        return ABV;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setABV(double ABV) {
        this.ABV = ABV;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setBreweryID(int breweryID) {
        this.breweryID = breweryID;
    }

    public void setOriginCountry(int originCountryID) {
        this.originCountryID = originCountryID;
    }

    public void setOriginCountryID(int originCountryID) {
        this.originCountryID = originCountryID;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

//    public void addTag(String tag) {
//        tags.add(tag);
//    }


    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }
}
