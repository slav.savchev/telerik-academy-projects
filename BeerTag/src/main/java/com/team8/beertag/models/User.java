package com.team8.beertag.models;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "enabled")
    private Boolean enabled=false;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    @JoinTable(
            name = "users_beers_wishlist",
            joinColumns = @JoinColumn(name = "users_beers_user_id"),
            inverseJoinColumns = @JoinColumn(name = "users_beers_beer_id")
    )
    private List<Beer> wishList;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    @JoinTable(
            name = "users_beers_dranklist",
            joinColumns = @JoinColumn(name = "users_beers_dranklist_user_id"),
            inverseJoinColumns = @JoinColumn(name = "users_beers_dranklist_beer_id")
    )
    private List<Beer> drankList;

    //TODO profile pic
    @Column(name = "picture")
    private byte[] image;

    public User(){
        wishList = new ArrayList<>();
        drankList = new ArrayList<>();
    }

    public User(String username, String password, String firstName, String lastName, String email) {
        wishList = new ArrayList<>();
        drankList = new ArrayList<>();
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public User(String username, String email) {
        wishList = new ArrayList<>();
        drankList = new ArrayList<>();
        this.username = username;
        this.email = email;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String userName) {
        this.username = userName;
    }

    public int getId() {
        return id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {return this.password;}

    public String getEmail() {
        return this.email;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setPassword(String password) {this.password = password;}

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public List<Beer> getWishList() {
        return wishList;
    }

    public void setWishList(List<Beer> wishList) {
        this.wishList = wishList;
    }

    public List<Beer> getDrankList() {
        return drankList;
    }

    public void setDrankList(List<Beer> drankList) {
        this.drankList = drankList;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }


}
