package com.team8.beertag.models;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DisplayBeerDto {

    private int id;
    private String name;
    private double ABV;
    private String description;
    private Brewery brewery;
    private Style style;
    private Country originCountry;
    private List<Tag> tags;
    private double rating;
    private int creatorId;

    //TODO Add beer pic
    private String image;

    public DisplayBeerDto(){
        tags = new ArrayList<>();
    }

    public DisplayBeerDto(String name, double ABV, String description, Style style, Country originCountry, Brewery brewery, double rating) {
        tags = new ArrayList<>();

        setName(name);
        setABV(ABV);
        setDescription(description);
        setStyle(style);
        setOriginCountry(originCountry);
        setBrewery(brewery);
        setRating(rating);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Brewery getBrewery() {
        return brewery;
    }

    public Country getOriginCountry() {
        return originCountry;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public double getABV() {
        return ABV;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setABV(double ABV) {
        this.ABV = ABV;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setBrewery(Brewery brewery) {
        this.brewery = brewery;
    }

    public void setOriginCountry(Country originCountry) {
        this.originCountry = originCountry;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }
}
