package com.team8.beertag.services;

import com.team8.beertag.models.Style;
import com.team8.beertag.repositories.StyleRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class StyleServiceImplTests {

    @Mock
    StyleRepository mockRepository;

    @InjectMocks
    StyleServiceImpl service;

    @Test
    public void getStyleById_Should_ReturnStyle_When_StyleExists(){

        // Arrange
        Style style = Factory.mockStyle();

        Mockito.when(mockRepository.getById(ArgumentMatchers.anyInt()))
                .thenReturn(style);

        // Act

        Style returnedStyle = service.getById(23);

        // Assert
        Assert.assertEquals(returnedStyle.getName(),style.getName());
    }

    @Test
    public void getAllStyles_Should_ReturnAllStyles_WhenPrompted(){

        // Arrange
        List<Style> styleList = Factory.mockStyleList();

        Mockito.when(mockRepository.getAllStyles())
                .thenReturn(styleList);

        // Act
        List<Style> returnedStyleList = service.getAllStyles();

        // Assert
        Assert.assertEquals(styleList.get(0),returnedStyleList.get(0));
    }

    @Test
    public void deleteStyle_Should_Call_Repository_Once_When_Beer_isDeleted() {

        // Arrange
        Style style = Factory.mockStyle();

        //Act
        service.deleteStyle(1);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).deleteStyle(1);

    }

    @Test
    public void getPopularStyles_Should_Return(){

        //Arrange
        List<Style> list = Factory.mockStyleList();

        //Act
       List<Style> returned = service.getPopularStyles();

        //Assert

       Mockito.verify(mockRepository,Mockito.times(1)).getAllUsedStyles();
    }

    @Test
    public void createStyle_Should_ReturnStyle_When_StyleIsCreated(){

        // Arrange
        Style style = Factory.mockStyle();

        Mockito.when(mockRepository.createStyle(style))
                .thenReturn(style);

        // Act
        Style returnedCountry = service.createStyle(style);

        // Assert
        Assert.assertEquals(style.getName(),returnedCountry.getName());
    }


}
