package com.team8.beertag.services;


import com.team8.beertag.exceptions.DuplicateEntityException;
import com.team8.beertag.models.Beer;
import com.team8.beertag.models.DisplayBeerDto;
import com.team8.beertag.models.User;
import com.team8.beertag.repositories.BeerRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.exceptions.misusing.MissingMethodInvocationException;
import org.mockito.exceptions.misusing.UnnecessaryStubbingException;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class BeerServiceImplTests {

    @Mock
    BeerRepository mockRepository;

    @InjectMocks
    BeerServiceImpl service;

    @Test
    public void createBeer_Should_ReturnBeer_When_BeerIsCreated() {

        // Arrange
        Beer beer = Factory.mockBeer();

        Mockito.when(mockRepository.createBeer(beer)
        ).thenReturn(beer);

        // Act

        Beer returnedBeer = service.createBeer(beer);

        // Assert
        Assert.assertEquals(returnedBeer.getId(), beer.getId());
    }

    @Test
    public void getBeerById_Should_ReturnBeer_When_BeerExists() {

        // Arrange
        Beer beer = Factory.mockBeer();

        Mockito.when(mockRepository.getBeerById(ArgumentMatchers.anyInt()))
                .thenReturn(beer);

        // Act
        Beer returnedBeer = service.getBeerById(1);

        // Assert
        Assert.assertEquals(returnedBeer.getName(), beer.getName());
    }

    @Test
    public void getAllBeers_Should_ReturnAllBeers_When_Prompted() {

        // Arrange
        List<Beer> beerList = Factory.mockBeerList();
        Map<Integer, String> flagMap = new HashMap<>();

        Mockito.when(mockRepository.getAllBeers(flagMap))
                .thenReturn(beerList);

        // Act
        List<Beer> returnedBeerList = service.getAllBeers(flagMap);

        // Assert
        Assert.assertEquals(returnedBeerList.get(0), beerList.get(0));
    }

    @Test(expected = DuplicateEntityException.class)
    public void createBeer_Should_Throw_When_BeerAlreadyExists() {

        // Arrange
        Beer beer = Factory.mockBeer();

        Mockito.when(service.checkBeerExists(beer.getName()))
                .thenReturn(true);

        // Act
        service.createBeer(beer);
    }

    @Test
    public void updateBeer_Should_Return_UpdatedBeer_When_BeerIsUpdated() {

        // Arrange
        Beer beer = Factory.mockBeer();

        Mockito.when(mockRepository.updateBeer(beer))
                .thenReturn(beer);

        // Act
        Beer returnedBeer = service.updateBeer(beer);

        // Assert

        Assert.assertEquals(beer.getName(), returnedBeer.getName());
    }

    @Test
    public void deleteBeer_Should_Call_Repository_Once_When_Beer_isDeleted() {

        // Arrange
        Beer beer = Factory.mockBeer();

        //Act
        service.deleteBeer(1);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).deleteBeer(1);

    }

    @Test
    public void getNewestBeers_Should_Return_List_When_Called(){

        // Arrange
        List<Beer> beerList = Factory.mockBeerList();

        Mockito.when(mockRepository.getNewestBeers()).thenReturn(beerList);

        // Act

         List<Beer> newList = service.getNewestBeers();

        // Assert

        Assert.assertEquals(newList,beerList);

    }

    @Test
    public void getRandomBeerID_Should_Return_ID_When_Called(){

        // Arrange
        Beer beer = Factory.mockBeer();

        Mockito.when(mockRepository.getRandomBeerId()).thenReturn(1);

        // Act

        int returnedID = service.getRandomBeerId();

        // Assert

        Assert.assertEquals(returnedID,1);

    }

    @Test(expected = NullPointerException.class)
    public void getPages_Should_Return_Pages_When_Called(){

        // Arrange
        List<List<DisplayBeerDto>> pages = Factory.mockBeerPages();
        List<Beer> pages1 = Factory.mockBeerList();

        Mockito.when(mockRepository.getAllBeers(new HashMap<>())).thenReturn(pages1);
        // Act

        List<List<DisplayBeerDto>> reutnredList = service.getPages(new HashMap<>());

        // Assert

        Mockito.verify(mockRepository, Mockito.times(0)).getAllBeers(new HashMap<>());
    }

    @Test
    public void getSimilarBeers_Should_Return_Beers_When_Called(){

        //Arrange
        List<Beer> pages1 = Factory.mockBeerList();
        Beer beer = Factory.mockBeer();

        Mockito.when(mockRepository.getAllBeers()).thenReturn(pages1);

        // Act
        List<Beer> returned = service.getSimilarBeers(beer);

        //Assert

        Assert.assertNotEquals(returned,pages1);
    }


    @Test(expected = NullPointerException.class)
    public void getSuggestedBeers_Should_Return_Beers_When_Called(){

        //Arrange
        User user = Factory.mockUser();
        Beer beer = Factory.mockBeer();
        List<Beer> beerList = Factory.mockBeerList();

        Mockito.when(mockRepository.getAllBeers()).thenReturn(beerList);

        // Act
        List<Beer> returned = service.getSuggestedBeers(user);

        //Assert

        Assert.assertEquals(returned.get(0),beer);
    }



}
