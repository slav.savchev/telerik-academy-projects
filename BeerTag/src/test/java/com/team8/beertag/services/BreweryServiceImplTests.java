package com.team8.beertag.services;


import com.team8.beertag.exceptions.DuplicateEntityException;
import com.team8.beertag.models.Brewery;
import com.team8.beertag.repositories.BreweryRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class BreweryServiceImplTests {

    @Mock
    BreweryRepository mockRepository;

    @InjectMocks
    BreweryServiceImpl service;

    @Test
    public void createBrewery_Should_ReturnBrewery_When_BreweryIsCreated(){

        // Arrange
        Brewery brewery = Factory.mockBrewery();

        Mockito.when(mockRepository.createBrewery(brewery)
        ).thenReturn(brewery);

        // Act

        Brewery returnedBrewery = service.createBrewery(brewery );

        // Assert
        Assert.assertEquals(returnedBrewery.getName(),brewery.getName());
    }


    @Test
    public void getBreweryById_Should_ReturnBrewery_When_BreweryExists(){

        // Arrange
        Brewery brewery = Factory.mockBrewery();

        Mockito.when(mockRepository.getBreweryById(ArgumentMatchers.anyInt()))
                .thenReturn(brewery);

        // Act
        Brewery returnedBrewery = service.getBreweryById(1);

        // Assert
        Assert.assertEquals(returnedBrewery.getName(),brewery.getName());
    }

    @Test
    public void getAllBreweries_Should_ReturnAllBreweries_When_Prompted(){

        // Arrange
        List<Brewery> breweryList = Factory.mockBreweryList();

        Mockito.when(mockRepository.getAllBreweries())
                .thenReturn(breweryList);

        // Act
        List<Brewery> returnedBreweryList = service.getAllBreweries();

        // Assert
        Assert.assertEquals(returnedBreweryList.get(0),breweryList.get(0));
    }

    @Test(expected = DuplicateEntityException.class)
    public void createBrewery_Should_Throw_When_BreweryAlreadyExists(){

        // Arrange
        Brewery brewery = Factory.mockBrewery();

        Mockito.when(service.checkBreweryExists(brewery.getName()))
                .thenReturn(true);

        // Act
        service.createBrewery(brewery);
    }

    @Test
    public void updateBrewery_Should_Return_UpdatedBrewery_When_BreweryIsUpdated(){

        // Arrange
        Brewery brewery = Factory.mockBrewery();

        Mockito.when(mockRepository.updateBrewery(brewery))
                .thenReturn(brewery);

        // Act
        Brewery returnedBrewery = service.updateBrewery(brewery);

        // Assert

        Assert.assertEquals(brewery.getName(),returnedBrewery.getName());
    }

    @Test
    public void deleteBrewery_Should_Call_Repository_Once_When_Beer_isDeleted() {

        // Arrange
        Brewery brewery = Factory.mockBrewery();

        //Act
        service.deleteBrewery(1);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).deleteBrewery(1);

    }
}
