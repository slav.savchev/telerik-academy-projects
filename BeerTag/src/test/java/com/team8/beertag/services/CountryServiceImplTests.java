package com.team8.beertag.services;

import com.team8.beertag.exceptions.DuplicateEntityException;
import com.team8.beertag.models.Country;
import com.team8.beertag.repositories.CountryRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CountryServiceImplTests {

    @Mock
    CountryRepository mockRepository;

    @InjectMocks
    CountryServiceImpl service;


    @Test
    public void createCountry_Should_ReturnCountry_When_CountryIsCreated(){

        // Arrange
        Country country = Factory.mockCountry();

        Mockito.when(mockRepository.createCountry(country))
                .thenReturn(country);

        // Act
        Country returnedCountry = service.createCountry(country);

        // Assert
        Assert.assertEquals(country.getCountryName(),returnedCountry.getCountryName());
    }

    @Test
    public void getCountryById_Should_ReturnCountry_When_CountryExists(){

        // Arrange
        Country country = Factory.mockCountry();

        Mockito.when(mockRepository.getCountryByID(ArgumentMatchers.anyInt()))
                .thenReturn(country);

        // Act

        Country returnedCountry = service.getCountryByID(23);

        // Assert
        Assert.assertEquals(country.getCountryName(),returnedCountry.getCountryName());
    }

    @Test
    public void getAllCountries_Should_ReturnAllCountries_When_Prompted(){

        // Arrange
        List<Country> countryList = Factory.mockCountryList();

        Mockito.when(mockRepository.getAllCountries())
                .thenReturn(countryList);

        // Act
        List<Country> returnedCountryList = service.getAllCountries();

        // Assert
        Assert.assertEquals(returnedCountryList.get(0),countryList.get(0));
    }

    @Test(expected = DuplicateEntityException.class)
    public void createCountry_Should_Throw_When_CountryAlreadyExists(){

        // Arrange
        Country country = Factory.mockCountry();

        Mockito.when(service.checkCountryExists(country.getCountryName()))
                .thenReturn(true);

        // Act
        service.createCountry(country);
    }

    @Test
    public void updateCountry_Should_Return_UpdatedCountry_When_CountryIsUpdated(){

        // Arrange
        Country country = Factory.mockCountry();

        Mockito.when(mockRepository.updateCountry(country))
                .thenReturn(country);

        // Act
        Country returnedCountry = service.updateCountry(country);

        // Assert

        Assert.assertEquals(country.getCountryName(),returnedCountry.getCountryName());
    }


    @Test
    public void deleteCountry_Should_Call_Repository_Once_When_Beer_isDeleted() {

        // Arrange
        Country country = Factory.mockCountry();

        //Act
        service.deleteCountry(1);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).deleteCountry(1);

    }
}
