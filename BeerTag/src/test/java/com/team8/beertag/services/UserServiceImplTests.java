package com.team8.beertag.services;

import com.team8.beertag.exceptions.DuplicateEntityException;
import com.team8.beertag.models.Beer;
import com.team8.beertag.models.DisplayBeerDto;
import com.team8.beertag.models.User;
import com.team8.beertag.models.UserDto;
import com.team8.beertag.repositories.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @Mock
    UserRepository mockRepository;

    @InjectMocks
    UserServiceImpl service;

    @Test
    public void createUser_Should_ReturnUser_When_UserIsCreated(){

        // Arrange
        User user = Factory.mockUser();

        Mockito.when(mockRepository.createUser(user)
        ).thenReturn(user);

        // Act
        User returnedUser = service.createUser(user);

        // Assert
        Assert.assertEquals(returnedUser.getUsername(),user.getUsername());
    }

    @Test
    public void getUserById_Should_ReturnUser_When_UserExists(){

        // Arrange
        User user = Factory.mockUser();

        Mockito.when(mockRepository.getUserById(ArgumentMatchers.anyInt()))
                .thenReturn(user);

        // Act
        User returnedUser = service.getUserById(1);

        // Assert
        Assert.assertEquals(returnedUser.getUsername(),user.getUsername());
    }

    @Test
    public void getAllUsers_Should_ReturnAllUsers_When_Prompted(){

        // Arrange
        List<User> userList = Factory.mockUserList();

        Mockito.when(mockRepository.getAllUsers())
                .thenReturn(userList);

        // Act
        List<User> returnedUserList = service.getAllUsers();

        // Assert
        Assert.assertEquals(returnedUserList.get(0),userList.get(0));
    }

    @Test(expected = DuplicateEntityException.class)
    public void createUser_Should_Throw_When_UserAlreadyExists(){

        // Arrange
        User user = Factory.mockUser();

        Mockito.when(service.checkUserExists(user.getUsername()))
                .thenReturn(true);

        // Act
        service.createUser(user);
    }

    @Test
    public void updateUser_Should_Return_UpdatedUser_When_UserIsUpdated(){

        // Arrange
        User user = Factory.mockUser();

        Mockito.when(mockRepository.updateUser(user))
                .thenReturn(user);

        // Act
        User returnedUser = service.updateUser(user);

        // Assert
        Assert.assertEquals(returnedUser.getUsername(),user.getUsername());
    }

    @Test
    public void deleteUser_Should_Call_Repository_Once_When_Beer_isDeleted() {

        // Arrange
        User user = Factory.mockUser();

        //Act
        service.deleteUser(1);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).deleteUser(1);

    }

    @Test
    public void getWishListBeers_Should_Return_Beers_When_Called(){

        //Arrange
        List<Beer> list = Factory.mockBeerList();

        Mockito.when(mockRepository.getWishListBeers(1)).thenReturn(list);

        //Act
        List<Beer> returned = service.getWishListBeers(1);

        //Assert

        Assert.assertEquals(returned,list);

    }

    @Test
    public void getDrankListBeers_Should_Return_Beers_When_Called(){

        //Arrange
        List<Beer> list = Factory.mockBeerList();

        Mockito.when(mockRepository.getDrankListBeers(1)).thenReturn(list);

        //Act
        List<Beer> returned = service.getDrankListBeers(1);

        //Assert

        Assert.assertEquals(returned,list);

    }


    @Test
    public void getNextAvailableId_Should_Ruturn_ID_when_Called(){

        //Arrange
        int id =1;

        Mockito.when(mockRepository.getNextAvailableId()).thenReturn(1);

        //Act

        int idd = service.getNextAvailableId();

        //Assert
        Assert.assertEquals(id,idd);
    }

    @Test
    public void constructUser_Should_Return_User_When_Called(){

        //Arrange
        User user = Factory.mockUser();

        Mockito.when(mockRepository.constructUser(user)).thenReturn(user);

        //Act
        User user1 = service.constructUser(user);

        //Assert

        Assert.assertEquals(user,user1);
    }

    @Test
    public void deleteBeerFromWishList_Should_Call_Repository(){
        // Arrange
        User user = Factory.mockUser();

        //Act
        service.deleteBeerFromWishList(1,1);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).deleteBeerFromWishList(1,1);
    }

    @Test
    public void deleteBeerFromDrankList_Should_Call_Repository(){
        // Arrange
        User user = Factory.mockUser();

        //Act
        service.deleteBeerFromDrankList(1,1);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).deleteBeerFromDrankList(1,1);
    }


    @Test
    public void addBeerToWishList_Should_Return_List_When_Called(){

        //Arrange
        List<Beer> beerList = Factory.mockBeerList();
        Mockito.when(mockRepository.addBeerToWishList(1,1)).thenReturn(beerList);

        //Act
        List<Beer> beerList1 = service.addBeerToWishList(1,1);

        //Assert
        Assert.assertEquals(beerList,beerList1);
    }

    @Test
    public void addBeerToDrankList_Should_Return_List_When_Called(){

        //Arrange
        List<Beer> beerList = Factory.mockBeerList();
        Mockito.when(mockRepository.addBeerToDrankList(1,1)).thenReturn(beerList);

        //Act
        List<Beer> beerList1 = service.addBeerToDrankList(1,1);

        //Assert
        Assert.assertEquals(beerList,beerList1);
    }


}
