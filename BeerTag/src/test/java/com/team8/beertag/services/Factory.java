package com.team8.beertag.services;

import com.team8.beertag.models.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Factory {


    public static Beer mockBeer(){
        Beer beer =  new Beer("Zagorka",4.5,"Not very nice");
        beer.setId(0);
        beer.setStyle(new Style("Blonde",0));
        beer.setOriginCountry(new Country("Bulgaira",1));
        beer.setBrewery(new Brewery("Meantime"));
        List<Tag> tagList = new ArrayList<>();
        tagList.add(new Tag("Drinkable"));
        beer.setTags(tagList);
        return beer;
    }

    public static List<Beer> mockBeerList(){
        List<Beer> beerList = new ArrayList<>();

        Beer beer =  new Beer("Zagorka",4.5,"Not very nice");
        beer.setId(0);
        beer.setStyle(new Style("Blonde",0));
        beer.setOriginCountry(new Country("Bulgaira",1));
        beer.setBrewery(new Brewery("Meantime"));
        List<Tag> tagList = new ArrayList<>();
        tagList.add(new Tag("Drinkable"));
        beer.setTags(tagList);
        beerList.add(beer);

        beer =  new Beer("Kamenitsa",4.0,"Very nice");
        beer.setId(1);
        beer.setStyle(new Style("Dark",1));
        beer.setOriginCountry(new Country("Bulgaristan",2));
        beer.setBrewery(new Brewery("BG_Brew"));
        tagList = new ArrayList<>();
        tagList.add(new Tag("Drinkable"));
        beer.setTags(tagList);
        beerList.add(beer);

        return beerList;
    }

    public static Brewery mockBrewery(){
        return new Brewery("MockBrewery");
    }

    public static List<Brewery> mockBreweryList(){
        List<Brewery> mockBreweryList = new ArrayList<>();

        Brewery mockBrewery = new Brewery("MockBrew1");
        mockBrewery.setId(0);
        mockBreweryList.add(mockBrewery);

        mockBrewery = new Brewery("MockBrew2");
        mockBrewery.setId(1);
        mockBreweryList.add(mockBrewery);

        return mockBreweryList;
    }

    public static Country mockCountry(){
        Country country = new Country("MockLandia",1);
        return country;
    }

    public static List<Country> mockCountryList(){
        List<Country> mockCountryList = new ArrayList<>();

        Country country = new Country("MockLandia",1);
        mockCountryList.add(country);

        country = new Country("MockLandia2",2);
        mockCountryList.add(country);

        return mockCountryList;
    }

    public static Style mockStyle(){
        return new Style("MockStyle",0);
    }

    public static List<Style> mockStyleList(){
        List<Style> mockStyleList = new ArrayList<>();

        Style style = new Style("MockStyle",0);
        mockStyleList.add(style);

        style = new Style("MockStyle1",1);
        mockStyleList.add(style);

        return mockStyleList;
    }

    public static Tag mockTag(){
        return new Tag("MockTag");
    }

    public static List<Tag> mockTagSet(){
        List<Tag> mockTagList = new ArrayList<>();

        Tag tag = new Tag("MockTag");
        tag.setId(0);
        mockTagList.add(tag);

        tag = new Tag("MockTag2");
        tag.setId(1);
        mockTagList.add(tag);

        return mockTagList;
    }

    public static User mockUser(){
        return new User("Pesho","pesho.peshev@abv.bg");
    }

    public static List<User> mockUserList(){
        List<User> mockUserList = new ArrayList<>();

        User user = new User("Pesho","pesho.peshev@abv.bg");
        user.setId(0);
        mockUserList.add(user);

        user = new User("Gosho","gosho.goshev@abv.bg");
        user.setId(1);
        mockUserList.add(user);

        return mockUserList;
    }

    public static List<List<DisplayBeerDto>> mockBeerPages(){
        List<DisplayBeerDto> list1 = new ArrayList<>();
        List<DisplayBeerDto> list2 = new ArrayList<>();

        List<List<DisplayBeerDto>> listList = new ArrayList<>();
        listList.add(list1);
        listList.add(list2);

        return listList;
    }


    public static List<List<UserDto>> mockUserPages(){
        List<UserDto> list1 = new ArrayList<>();
        List<UserDto> list2 = new ArrayList<>();

        List<List<UserDto>> listList = new ArrayList<>();
        listList.add(list1);
        listList.add(list2);

        return listList;
    }

}
