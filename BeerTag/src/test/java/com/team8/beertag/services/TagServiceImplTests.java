package com.team8.beertag.services;

import com.team8.beertag.models.Tag;
import com.team8.beertag.repositories.TagRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Set;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTests {

    @Mock
    TagRepository mockRepository;

    @InjectMocks
    TagServiceImpl service;

    @Test
    public void createTag_Should_ReturnTag_When_TagIsCreated(){

        // Arrange
        Tag tag = Factory.mockTag();

        Mockito.when(mockRepository.createTag(tag))
                .thenReturn(tag);

        // Act
        Tag returnedTag = service.createTag(tag);

        // Assert
        Assert.assertEquals(tag.getName(),returnedTag.getName());
    }

    @Test
    public void getTagById_Should_ReturnTag_When_TagExists(){

        // Arrange
        Tag tag = Factory.mockTag();

        Mockito.when(mockRepository.getTagById(ArgumentMatchers.anyInt()))
                .thenReturn(tag);

        // Act
        Tag returnedTag = service.getTagById(23);

        // Assert
        Assert.assertEquals(tag.getName(),returnedTag.getName());
    }

    @Test
    public void getAllTags_Should_ReturnAllTags_When_Prompted(){

        // Arrange
        List<Tag> tagSet = Factory.mockTagSet();

        Mockito.when(mockRepository.getAllTags())
                .thenReturn(tagSet);

        // Act
        List<Tag> returnedSet = service.getAllTags();

        // Assert
        Assert.assertEquals(tagSet,returnedSet);
    }

    @Test
    public void updateTag_Should_UpdateTag_When_TagIsUpdated(){

        // Arrange
        Tag tag = Factory.mockTag();

        Mockito.when(mockRepository.updateTag(tag))
                .thenReturn(tag);

        // Act
        Tag returnedTag = service.updateTag(tag);

        // Assert
        Assert.assertEquals(returnedTag.getName(),tag.getName());
    }

    @Test
    public void deleteTag_Should_Call_Repository_Once_When_Beer_isDeleted() {

        // Arrange
        Tag tag = Factory.mockTag();

        //Act
        service.deleteTag(1);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).deleteTag(1);

    }

    @Test
    public void getPopularTags_Should_Call_Repo(){
        // Arrange
        Tag tag = Factory.mockTag();

        //Act
        service.getPopularTags();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getUsedTags();
    }


}
